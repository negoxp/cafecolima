
$(function() {
  

  $('#scorte').on('change', function() {
    $( "#sform" ).submit();
  })

  $('#genero').on('change', function() {
    //alert( this.value );
    if ($(this).val()=="h"){
    	$('.defaulh').show();
    	$('.defaulm').hide();	
    	$('#fd1').prop('checked', true);
    }
    if ($(this).val()=="m"){
    	$('.defaulh').hide();
    	$('.defaulm').show();
    	$('#fd7').prop('checked', true);
    }

    if ($(this).val()!="h" && $(this).val()!="m" ){
    	$('.defaulh').hide();
    	$('.defaulm').hide();	
    }
    
  });

  var outerContentWidth = $('#firstnode').width();
  var scrollPosition = outerContentWidth/2;
  $('#arbol').scrollLeft(scrollPosition-($( window ).width()/2)+100);

  $(".mymodal").click(function(){
  
		$("#parentId").val($(this).attr('parentId'));
		$("#hijo_posicion").val($(this).attr('hijoPosicion'));
    $("#userid").val($(this).attr('userid'));
               
    $('#myModal').appendTo("body").modal('show');
  });

  $(".showImg").click(function(){
    var imgpath = $(this).attr('imgpath');
    var downpath = $(this).attr('downpath');
    $("#my_image").attr("src",imgpath);
    $("#my_down").attr("href",downpath);
    $('#showImg').appendTo("body").modal('show');
  });


  //---------- Car Shopping -----



  $(".myCar").click(function(){  

    var productoId = $(this).attr('productoId');
    var productoNombre = $(this).attr('productoNombre');
    var productoCosto = $(this).attr('productoCosto');
    var productoCantidad = 1;

    $.ajax({
      url: url_general + '/tienda/agregar-carrito',
      type: "post",
      data: {"_token": $('meta[name="csrf-token"]').attr('content'), 'productoId':productoId, 'productoNombre': productoNombre, 'productoCosto': productoCosto, 'productoCantidad': productoCantidad },
      success: function(data){
        //alert(data); 
        $(".cartBody").html(data);
      }
    }); 

    $('#myCar').appendTo("body").modal('show');
  });

  $(".myCarmodal").click(function(){  

    $.ajax({
      url: url_general + '/tienda/mostrar-carrito',
      type: "post",
      data: {"_token": $('meta[name="csrf-token"]').attr('content')},
      success: function(data){
        $(".cartBody").html(data);
      }
    });

    $('#myCar').appendTo("body").modal('show');
  });
 
  $(document).on('click', '.eliminarCarrito', function () {  
    var productoId = $(this).attr('productoId');
    $.ajax({
      url: url_general + '/tienda/eliminar-carrito',
      type: "post",
      data: {"_token": $('meta[name="csrf-token"]').attr('content'), 'productoId':productoId },
      success: function(data){
        //alert(data); 
        $(".cartBody").html(data);
      }
    });

  });

  $(document).on('click', '.number-spinner button', function () {    
    var productoId = $(this).attr('productoId');

    var btn = $(this),
      oldValue = btn.closest('.number-spinner').find('input').val().trim(),
      newVal = 0;
    
    if (btn.attr('data-dir') == 'up') {
      newVal = parseInt(oldValue) + 1;
      $.ajax({
        url: url_general + '/tienda/actualizar-cantidad',
        type: "post",
        data: {"_token": $('meta[name="csrf-token"]').attr('content'), 'productoId':productoId, 'newVal': newVal},
        success: function(data){
          $(".cartBody").html(data);
        }
      }); 


    } else {
      if (oldValue > 1) {
        newVal = parseInt(oldValue) - 1;
        $.ajax({
          url: url_general + '/tienda/actualizar-cantidad',
          type: "post",
          data: {"_token": $('meta[name="csrf-token"]').attr('content'), 'productoId':productoId, 'newVal': newVal},
          success: function(data){
            $(".cartBody").html(data);
          }
        }); 
      } else {
        newVal = 1;
      }
    }
    btn.closest('.number-spinner').find('input').val(newVal);
    return false;
  });



  //---------- Car Shopping -----



  //$(".phone").mask("(999) 999-9999");
  //$(".patrocinadorMask").mask('#');

  $('.pagadoclass').bootstrapSwitch();
  $('.pagadoclass').on('switch-change', function(e, data) {
        //console.log($(this).attr('pedidoId'));
        //$(this).bootstrapSwitch('toggleDisabled');
        console.log(data.value);
        if (data.value){
          $(this).load(url_general + '/admin/aceptar-pago-efectivo/' + $(this).attr('pedidoId'), function(responseText, status) {
             if (status === 'success') {
                console.log("pago exitoso")
             } else {
                alert('NOT success');
             }
          });
        }else{
          $(this).load(url_general + '/admin/cancelar-pago-efectivo/' + $(this).attr('pedidoId'), function(responseText, status) {
             if (status === 'success') {
                console.log("pago exitoso")
             } else {
                alert('NOT success');
             }
          });
        }
  });

  $('.pagadoReciboclass').bootstrapSwitch();
  $('.pagadoReciboclass').on('switch-change', function(e, data) {
        //console.log($(this).attr('pedidoId'));
        //$(this).bootstrapSwitch('toggleDisabled');
        console.log(data.value);
        if (data.value){
          $(this).load(url_general + '/admin/aceptar-pago-recibo/' + $(this).attr('pedidoId'), function(responseText, status) {
             if (status === 'success') {
                console.log("pago exitoso")
             } else {
                alert('NOT success');
             }
          });
        }else{
          $(this).load(url_general + '/admin/cancelar-pago-recibo/' + $(this).attr('pedidoId'), function(responseText, status) {
             if (status === 'success') {
                console.log("pago exitoso")
             } else {
                alert('NOT success');
             }
          }); 
        }
  });

  $('.tpagadoReciboclass').bootstrapSwitch();
  $('.tpagadoReciboclass').on('switch-change', function(e, data) {
        console.log(data.value);
        if (data.value){
          $(this).load(url_general + '/admin/t-aceptar-pago-recibo/' + $(this).attr('pedidoId'), function(responseText, status) {
             if (status === 'success') {
                console.log("pago exitoso")
             } else {
                alert('NOT success');
             }
          });
        }else{
          $(this).load(url_general + '/admin/t-cancelar-pago-recibo/' + $(this).attr('pedidoId'), function(responseText, status) {
             if (status === 'success') {
                console.log("pago exitoso")
             } else {
                alert('NOT success');
             }
          }); 
        }
  });


  $('.entregadoclass').bootstrapSwitch();
  $('.entregadoclass').on('switch-change', function(e, data) {
        console.log(data.value);
        if (data.value){
          $(this).load(url_general + '/admin/aceptar-entregado/' + $(this).attr('pedidoId'), function(responseText, status) {
             if (status === 'success') {
                console.log("entrego exitoso")
             } else {
                alert('NOT success');
             }
          });
        }else{
          $(this).load(url_general + '/admin/cancelar-entregado/' + $(this).attr('pedidoId'), function(responseText, status) {
             if (status === 'success') {
                console.log("entrego exitoso")
             } else {
                alert('NOT success');
             }
          });
        }
  });

  $('.descuentoclass').bootstrapSwitch();
  $('.descuentoclass').on('switch-change', function(e, data) {
        console.log(data.value);
        if (data.value){
          $(this).load(url_general + '/admin/crear-descuento/' + $(this).attr('pedidoId'), function(responseText, status) {
             if (status === 'success') {
                console.log("entrego exitoso")
             } else {
                alert('NOT success');
             }
          });
        }else{
          $(this).load(url_general + '/admin/cancelar-descuento/' + $(this).attr('pedidoId'), function(responseText, status) {
             if (status === 'success') {
                console.log("entrego exitoso")
             } else {
                alert('NOT success');
             }
          });
        }
  });

  $("#verificar_usr").click(function(){
    var patrocinador_id = $("#patrocinador_id").val();
    //alert(patrocinador_id);
      $(this).load(url_general + '/proceso/verificar_usr/' + patrocinador_id, function(responseText, status) {
         //alert(responseText);
         
         if (responseText === 'Válido') {
            $("#patrocinador_id").removeClass( "btn-success" )
            $("#patrocinador_id").removeClass( "btn-danger" )
            $(this).removeClass( "btn-info" )
            $(this).removeClass( "btn-success" )
            $(this).removeClass( "btn-danger" )
            $(this).addClass( "btn-success" )
            $("#patrocinador_id").addClass( "btn-success" )
            //console.log("entrego exitoso")
         } else {
            $("#patrocinador_id").removeClass( "btn-success" )
            $("#patrocinador_id").removeClass( "btn-danger" )
            $(this).removeClass( "btn-info" )
            $(this).removeClass( "btn-danger" )
            $(this).removeClass( "btn-success" )
            $(this).addClass( "btn-danger" )
            $("#patrocinador_id").addClass( "btn-danger" )
            //alert('NOT success');
         }
         
      });
  });  

});


var TRange=null;
var outerContentWidth2=0;
var scrollPosition2=0;
function findString (str) {
 if (parseInt(navigator.appVersion)<4) return;
 var strFound;
 if (window.find) {

  // CODE FOR BROWSERS THAT SUPPORT window.find
  strFound=self.find(str);
  if (!strFound) {
   strFound=self.find(str,0,1);
   while (self.find(str,0,1)) continue;
  }

  outerContentWidth2 = strFound.width();
  scrollPosition2 = outerContentWidth2/2;
  $('#arbol').scrollLeft(scrollPosition2-($( window ).width()/2)+100);

 }
 else if (navigator.appName.indexOf("Microsoft")!=-1) {

  // EXPLORER-SPECIFIC CODE

  if (TRange!=null) {
   TRange.collapse(false);
   strFound=TRange.findText(str);
   if (strFound) TRange.select();
  }
  if (TRange==null || strFound==0) {
   TRange=self.document.body.createTextRange();
   strFound=TRange.findText(str);
   if (strFound) TRange.select();
  }
 }
 else if (navigator.appName=="Opera") {
  alert ("Opera browsers not supported, sorry...")
  return;
 }
 if (!strFound) alert ("String '"+str+"' not found!")
 return;
}