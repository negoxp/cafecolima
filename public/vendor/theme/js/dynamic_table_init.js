function fnFormatDetails ( oTable, nTr )
{
    var aData = oTable.fnGetData( nTr );
    var sOut = '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">';
    sOut += '<tr><td>Rendering engine:</td><td>'+aData[1]+' '+aData[4]+'</td></tr>';
    sOut += '<tr><td>Link to source:</td><td>Could provide a link here</td></tr>';
    sOut += '<tr><td>Extra info:</td><td>And any further details here (images etc)</td></tr>';
    sOut += '</table>';

    return sOut;
}

$(document).ready(function() {

    $('#dynamic-table').dataTable( {
        "aaSorting": [[ 2, "asc" ]],
        "bPaginate": false,
    } );

    $('#dynamic-table-pagados').dataTable( {
        "aaSorting": [[ 5, "desc" ]],
        "bPaginate": false,
    } );    


    $('#dynamic-table2').DataTable({
            "pageLength": 50,
            dom: 'Bfrtip',
            buttons: ['excel']
        });

    $('#dynamic-table3').DataTable({
            "pageLength": 50,
            "aaSorting": [[ 0, "desc" ]],
            dom: 'Bfrtip',
            "bPaginate": false,
            buttons: ['excel']
        });


} );