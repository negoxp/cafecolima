<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/unirme', 'OutController@unirme');
Route::post('/invite', 'OutController@invite');

Route::get('/blocked', function () {
    return view('blocked');
});

Route::auth(); 

Route::get('home', 'HomeController@index');

Route::group(['middleware' => ['auth']], function() {

	Route::get('/download/{pedido_id}', 'DownloadsController@download');
	Route::get('/tdownload/{pedido_id}', 'DownloadsController@tdownload');
	Route::get('/sendmails', 'DownloadsController@sendmails');
	
	Route::get('/pedidos/invoice/{pedido_id}', 'PedidoController@invoice');
	Route::get('/pedidos/invoice_print/{pedido_id}', 'PedidoController@invoice_print');
	Route::get('/tpedidos/invoice/{pedido_id}', 'TPedidoController@invoice');
	Route::get('/tpedidos/invoice_print/{pedido_id}', 'TPedidoController@invoice_print');
	Route::get('/tpedidos/{pedido_id}/editp', 'TPedidoController@editp');
	Route::PUT('/tpedidos/dupdate/{pedido_id}', 'TPedidoController@dupdate')->name('tpedidos.dupdate');
	
	Route::get('/proceso/aceptar_pago/{pedido_id}', 'ProcesosController@aceptar_pago');
	Route::get('/proceso/rechazar_pago/{pedido_id}', 'ProcesosController@rechazar_pago');
	Route::get('/proceso/t_rechazar_pago/{pedido_id}', 'ProcesosController@t_rechazar_pago');
	Route::get('/proceso/verificar_usr/{usr_id}', 'ProcesosController@verificar_usr');



	Route::get('/users/{user_id}/editme', 'UserController@editme');
	Route::PUT('/users/updateme/{user_id}', 'UserController@updateme'); 


	Route::resource('users','UserController');
	Route::resource('pedidos','PedidoController');
	Route::resource('tpedidos','TPedidoController');
	Route::resource('productos','ProductoController');

	Route::controllers([
    'home'             =>  'HomeController',
    'admin'             =>  'AdminController', 
    'tienda'             =>  'TiendaController', 
	]); 
 

});