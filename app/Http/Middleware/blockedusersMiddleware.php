<?php

namespace App\Http\Middleware;
use Auth;

use Closure;

class blockedusersMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user()->isactive) {
            return $next($request);
        }
        return redirect('/blocked'); 
        //throw new \Exception("Unauthorized");
        
        //return $next($request);
        //return redirect('/');   
    }
}
