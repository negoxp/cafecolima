<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Kris\LaravelFormBuilder\FormBuilder;
use App\User;
use App\Role;
use App\Producto;
use App\Pedido;
use App\PedidosDetalle;
use App\TPedido;
use App\TPedidoDetalle;
use App\Comisione;
use App\Http\Requests\UserFormRequest;
use App\Http\Requests;
use App\Config;
use MP;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;

use Password;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\PasswordBroker;
use Illuminate\Foundation\Auth\ResetsPasswords;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('blockedusers');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $me=\Auth::user();
        $mescorte=Config::getMescorte();
        
        if (\Auth::user()->hasRole('moderador')){
            return redirect()->action('AdminController@getPedidosEntregados');
            exit();
        }

        $condition="user_id='".$me->id."' and mespedido='".$mescorte."'"; 
        $pedido = Pedido::whereRaw($condition)->first();
        
        $me=\Auth::user();

        return view('dashboard/index')->with([
            'me'   =>  $me,
            'pedido' => $pedido,
            'success'   =>  "",
        ]);

        //return view('home');
    }

    public function getPedidos()
    {
        //echo \Auth::user()->name;
        //echo \Auth::user()->hasRole('admin');
        //exit();

        $preferenceData = [
            'items' => [
                [
                    'id' => 12,
                    'category_id' => 'phones',
                    'title' => 'iPhone 6 prueba',
                    'description' => 'iPhone 6 de 64gb nuevo',
                    'picture_url' => 'http://d243u7pon29hni.cloudfront.net/images/products/iphone-6-dorado-128-gb-red-4g-8-mpx-1256254%20(1)_m.png',
                    'quantity' => 1,
                    'currency_id' => 'MXN',
                    'unit_price' => 10
                ]
            ],
        ];

        $preference = MP::create_preference($preferenceData);
        //dd($preference);

        $me=\Auth::user();

        $condition="user_id='".$me->id."'";
        if (\Auth::user()->hasRole('admin') ){
            //$condition="1=1";
        }
        $pedidos = Pedido::whereRaw($condition)->get();
        $tpedidos = TPedido::whereRaw($condition)->get();


        $me=\Auth::user();
        $mescorte=Config::getMescorte();

        return view('dashboard/pedidos')->with([
            'me'   =>  $me,
            'pedidos'   =>  $pedidos,
            'tpedidos'   =>  $tpedidos,
            'mescorte'   =>  $mescorte,
            'preference'   =>  $preference,
            'success'   =>  "", 
        ]);
    }

    public function getSrchform()
    {
        return view('dashboard/srchform')->with([
            'success'   =>  "",
        ]);
    }


    

    public function getComisiones()
    {

        $me=\Auth::user();

        $comisiones = Comisione::whereRaw("user_id='".$me->id."'")->get();


        $me=\Auth::user();

        return view('dashboard/miscomisiones')->with([
            'me'   =>  $me,
            'comisiones'   =>  $comisiones,
            'success'   =>  "",
        ]);
    }

    public function getMisdirectos()
    {

        $me=\Auth::user();

        $condition="presentador_id='".$me->id."' and  isactive='1' and free_spot='0' and (status='new' or status='preactivo' or status='activo' or  status='mes_pasdo_no_pagado')"; 
        $directos = User::whereRaw($condition)->get();

        return view('dashboard/misdirectos')->with([
            'me'   =>  $me,
            'directos'   =>  $directos,
            'success'   =>  "",
        ]);

    }

    public function getMilista()
    {

        $me=\Auth::user();

        $condition="presentador_id='".$me->id."' and  isactive='1' and free_spot='0' and (status='new' or status='preactivo' or status='activo' or  status='mes_pasdo_no_pagado')"; 
        $directos = User::whereRaw($condition)->get();

        return view('dashboard/misdirectos')->with([
            'me'   =>  $me,
            'directos'   =>  $directos,
            'success'   =>  "",
        ]);

    }

    public function getImpresosVolantes()
    {

        $me=\Auth::user();

        return view('impresos/volantes')->with([
            'me'   =>  $me,
            'success'   =>  "",
        ]);

    }


    public function postInvite(Request $request){   

        $now = new \DateTime();
        $token = str_random(10);
        $me=\Auth::user();

        $remplaced=false;
        
        //Si vamos a remplazar primero vamos a borrar
        if (is_numeric($request->get('userid'))){
            $userRemplaced = User::find($request->get('userid'));
            
            if ($userRemplaced->free_spot==1){
                $userRemplaced->email_backup=$userRemplaced->email;
                $userRemplaced->save();
                $userRemplaced->email="eliminado.".$userRemplaced->id."@cafecolima.com";
                $userRemplaced->isactive=false;
                $userRemplaced->padre_id=null;
                $userRemplaced->presentador_id=null;
                $userRemplaced->save();
                $remplaced=true;
            }

            //dd("aquivoy".$remplaced);

        }

        //first check if user exist 
        $condition="email='".$request->get('email')."'";
        $userexist = User::whereRaw($condition)->limit(1)->get(); 
        if ($me->getInvitacionesPendientes()<30){
                if (count($userexist)==0){
                    $user = new User([
                        'name'              =>  shortname($request->get('nombres'), $request->get('apellidos')),
                        'email'             =>  $request->get('email'),
                        'celular'             =>  $request->get('celular'),
                        'password'          =>  bcrypt("cafecolima2017")

                    ]);
                    //'password'          =>  bcrypt($now->getTimestamp())
                    $user->tipo="socio";
                    $user->status="nuevo"; 
                    $user->numero_membresia=md5( $now->getTimestamp());
                    $user->nombre=$request->get('nombres'); 
                    $user->celular=$request->get('celular');
                    $user->apellidos=$request->get('apellidos');
                    $user->padre_id=$request->get('parentId');
                    $user->registro_id=\Auth::user()->id;

                    if (User::find($request->get('patrocinador_id'))){
                      $user->presentador_id=$request->get('patrocinador_id');
                    }else{
                      $user->presentador_id=\Auth::user()->id;
                    }
                    

                    $user->genero=$request->get('genero');
                    $user->foto_default=$request->get('foto_default');
                    $user->hijo_posicion=$request->get('hijo_posicion');
                    $user->can_edit=1;
                    $user->remember_token = $token;

                    //$user->roles()->getResults()
                    $user->save();

                    //Agregar pedido de Inicio
                    $mescorte=Config::getMescorte();
                    $producto_inicio = Producto::find(3);
                    $pedido = new Pedido();
                    $pedido->user_id = $user->id;
                    $pedido->sub_total = $producto_inicio->costo_neto/1.16;
                    $pedido->iva = $producto_inicio->costo_neto-($producto_inicio->costo_neto/1.16);
                    $pedido->total = $producto_inicio->costo_neto;
                    $pedido->status = 'pendiente';
                    $pedido->mespedido = $mescorte;
                    $user->ultimo_pedido=$pedido->created_at;
                    $user->status="new";
                    $pedido->save();

                    $pedidoDetalle = new PedidosDetalle();
                    $pedidoDetalle->pedido_id = $pedido->id;
                    $pedidoDetalle->producto_id = $producto_inicio->id;
                    $pedidoDetalle->cantidad = 1;
                    $pedidoDetalle->costo_unitario = $producto_inicio->costo_neto;
                    $pedidoDetalle->total = $producto_inicio->costo_neto;
                    $pedidoDetalle->save();
                    //para actualizar el total
                    $pedido->save();


                    $user->roles()->sync([]);
                    $user->roles()->attach(3);

                    $user->save();

                    if ($remplaced){
                        $usuarios = User::whereRaw("padre_id='".$userRemplaced->id."'")->get();
                        foreach($usuarios as $s){
                            $s->padre_id=$user->id;
                            $s->save();
                        } 
                    }

                    //deshabilitamos para no mandar correos pro un inicio. 
                    
                    $response = Password::sendResetLink(['email' => $user->email], function($message)
                    {
                        $message->subject('Bienvenido a Café Sabor a Colima');
                    });

                    return redirect()->action('HomeController@index')->with('message','El registro ha sido exitoso, hemos enviado una invitación al usuario.');
                }else{
                    return redirect()->action('HomeController@index')->with('message-danger','El usuario ya esta registrado previamente.');
                }
        }else{
            return redirect()->action('HomeController@index')->with('message-warning','Ya no tiene invitaciones, es necesario que alguno de tus invitados haga el pago para seguir invitando.');
        }

    }
}

function shortname($names, $last)
{
    $nombres = explode(" ", $names);
    $apellidos = explode(" ", $last);
    return $nombres[0]." ".$apellidos[0];
}
