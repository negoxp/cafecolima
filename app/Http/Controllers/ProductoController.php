<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

//use Illuminate\Http\Request;
use Kris\LaravelFormBuilder\FormBuilder;
use App\User;
use App\Role;
use App\Producto;
use App\Pedido;
use App\PedidosDetalle;
use App\Http\Requests\UserFormRequest;
use App\Http\Requests;
use App\Config;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;

use Password;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\PasswordBroker;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Request;

class ProductoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('blockedusers');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $me=\Auth::user();
      $mescorte=Config::getMescorte();
      
      $condition="id not in (1,2,3,4,5)"; 
      $productos = Producto::whereRaw($condition)->orderBy('id', 'asc')->get(); 

      return view('productos/index')->with([
          'me'   =>  $me,
          'productos' => $productos,
          'success'   =>  "", 
      ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    
         return view('productos/new')->with([
            'success'   =>  "",
        ]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $me=\Auth::user();
        $rules = array(
            'nombre'       => 'required',
            'costo_neto'       => 'required',
            //'puntos'        => 'required',
        );
        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            //return Redirect::to('productos/' . $id . '/edit')->withErrors($validator);
            return redirect()->action('ProductoController@index')->with('message','Se editó correctamente el producto.');
        } else {
            // store
            $file = Request::file('foto');
            
            $producto = new Producto();
            $producto->created_at = date('Y-m-d H:i:s');
            $producto->updated_at = date('Y-m-d H:i:s');

            if ($file){

                //$file = Input::file('comprobante');
                $path = '/uploads/productos';
                $photoname = $producto->id.'_'.date('Ymd').'_'.uniqid().'.'. $file->guessClientExtension();

                $file->move(public_path() . $path, $photoname);
                $producto->foto = $path. '/' . $photoname;
                //save referencia 
                
            } 
            $producto->nombre=Request::get('nombre');
            $producto->costo_neto=Request::get('costo_neto');
            $producto->puntos=Request::get('puntos');
            $producto->is_in_tienda=Request::get('is_in_tienda');
            $producto->save();
            // redirect
            return redirect()->action('ProductoController@index')->with('message','Se editó correctamente el producto.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

      $producto = Producto::find($id);
      

      return view('productos/edit')->with([
            'producto'   =>  $producto,
            'success'   =>  "",
        ]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $me=\Auth::user();
      	$rules = array(
            'nombre'       => 'required',
            'costo_neto'       => 'required',
        );
        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            //return Redirect::to('productos/' . $id . '/edit')->withErrors($validator);
            return redirect()->action('ProductoController@index')->with('message','Se editó correctamente el producto.');
        } else {
            // store
        	$file = Request::file('foto');
            
            $producto = Producto::find($id);
            $producto->updated_at = date('Y-m-d H:i:s');

            if ($file){

                //$file = Input::file('comprobante');
                $path = '/uploads/productos';
                $photoname = $producto->id.'_'.date('Ymd').'_'.uniqid().'.'. $file->guessClientExtension();

                $file->move(public_path() . $path, $photoname);
                $producto->foto = $path. '/' . $photoname;
                //save referencia 
                
			} 
            $producto->nombre=Request::get('nombre');
            $producto->costo_neto=Request::get('costo_neto');
            $producto->puntos=Request::get('puntos');
            $producto->is_in_tienda=Request::get('is_in_tienda');
            $producto->save();
            // redirect
            return redirect()->action('ProductoController@index')->with('message','Se editó correctamente el producto.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
