<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Kris\LaravelFormBuilder\FormBuilder;
use App\User;
use App\Role;
use App\Producto;
use App\Pedido;
use App\PedidosDetalle;
use App\Comisione;
use App\Http\Requests\UserFormRequest;
use App\Http\Requests;
use App\Config;
use App\TPedido;
use App\TPedidoDetalle;
use Cart;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;

use Password;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\PasswordBroker;
use Illuminate\Foundation\Auth\ResetsPasswords;


class TiendaController extends Controller
{
   /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('blockedusers');
    }

    public function getIndex()
    {   
        $me=\Auth::user();
        $mescorte=Config::getMescorte();
        
        /*
        if (\Auth::user()->hasRole('moderador')){
            return redirect()->action('AdminController@getPedidosEntregados');
            exit();
        }

        $condition="user_id='".$me->id."' and mespedido='".$mescorte."'"; 
        $pedido = Pedido::whereRaw($condition)->first();
         
        $me=\Auth::user();
        */
        $condition="is_in_tienda=true"; 
        $productos = Producto::whereRaw($condition)->orderBy('id', 'asc')->get(); 

        return view('tienda/index')->with([
            'me'   =>  $me,
            'productos' => $productos,
            'success'   =>  "", 
        ]);

        //return view('home');
    }
    public function getCheckout()
    {   
        $me=\Auth::user();
        $mescorte=Config::getMescorte();

        $condition="is_in_tienda=true"; 
        $productos = Producto::whereRaw($condition)->orderBy('id', 'asc')->get(); 

        return view('tienda/checkout')->with([
            'me'   =>  $me,
            'productos' => $productos,
            'cart'   =>  Cart::content(),
            'subtotal' => Cart::subtotal(),
            'success'   =>  "", 
        ]);

        //return view('home');
    }

    public function postComprar(Request $request)
    {   
        $me=\Auth::user();
        $mescorte=Config::getMescorte();
        $user_id=$me->id;

        if (\Auth::user()->hasRole('admin') ){
            if ($u = User::find($request->get('user_id'))){
               $user_id=$u->id; 
            }
        }
    

        $pedido = new TPedido();
        $pedido->user_id = $user_id;
        $pedido->total = str_replace(',', '', Cart::subtotal());
        $pedido->status = 'pendiente';
        $pedido->mespedido = $mescorte;
        $pedido->comision = str_replace(',', '', Cart::subtotal())*0.10;
        $pedido->presentador_id = $me->presentador_id ;
        $pedido->pagado = false ;
        $pedido->entregado = false;
        $pedido->save();
        $tpuntos=0; 
        foreach(Cart::content() as $c){
            $producto = Producto::find($c->id);
            if(!$pedidoDetalle = TPedidoDetalle::where('t_pedido_id','=',$pedido->id)->where('producto_id','=',$producto->id)->first()){
                $pedidoDetalle = new TPedidoDetalle();
                $pedidoDetalle->t_pedido_id = $pedido->id;
                $pedidoDetalle->producto_id = $producto->id;
            }

            $pedidoDetalle->cantidad = $c->qty;
            $pedidoDetalle->costo_unitario = $producto->costo_neto;
            $pedidoDetalle->total = $producto->costo_neto*$c->qty;
            $pedidoDetalle->puntos = $producto->puntos*$c->qty;
            $tpuntos += $producto->puntos*$c->qty;
            $pedidoDetalle->save();
        }
        
        $pedido->puntos = $tpuntos;
        $pedido->save();

        Cart::destroy();
        return redirect('home/pedidos')->with('success','Tu pedido se realizó correctamente');

        //return \Redirect::route('home.pedidos')->with('success','Tu pedido se realizó correctamente');

    }


    public function postAgregarCarrito(Request $request){  
        $productoId = $request->get('productoId');
        $productoNombre = $request->get('productoNombre');
        $productoCosto = $request->get('productoCosto');
        $producto = Producto::find($productoId);

        //Cart::destroy();
        Cart::add($productoId, $producto->nombre, 1, $producto->costo_neto)->associate('App\Producto');
        
        return view('tienda/varcarrito')->with([
            'cart'   =>  Cart::content(),
            'subtotal' => Cart::subtotal(),
            'success'   =>  "", 
        ]);
    }

    public function postMostrarCarrito(Request $request){  

        return view('tienda/varcarrito')->with([
            'cart'   =>  Cart::content(),
            'subtotal' => Cart::subtotal(),
            'success'   =>  "", 
        ]);
    }

    public function postActualizarCantidad(Request $request){  

        $productoId = $request->get('productoId');
        $newVal = $request->get('newVal');
        $producto = Producto::find($productoId);

        foreach(Cart::content() as $c){
            if ($c->id==$productoId){
                //Cart::remove($c->rowId)->associate('App\Producto');
                //Cart::add($productoId, $producto->nombre, $newVal, $producto->costo_neto)->associate('App\Producto');
                Cart::update($c->rowId, $newVal)->associate('App\Producto');
            }
        }
        //dd(Cart::content());
        
        return view('tienda/varcarrito')->with([
            'cart'   =>  Cart::content(),
            'subtotal' => Cart::subtotal(),
            'success'   =>  "", 
        ]);
        
    }

    public function postEliminarCarrito(Request $request){  

        $productoId = $request->get('productoId');
        $producto = Producto::find($productoId);

        foreach(Cart::content() as $c){
            if ($c->id==$productoId){
                Cart::remove($c->rowId);
                //Cart::add($productoId, $producto->nombre, $newVal, $producto->costo_neto)->associate('App\Producto');
                //Cart::update($c->rowId, $newVal)->associate('App\Producto');
            }
        }
        //dd(Cart::content());
        
        return view('tienda/varcarrito')->with([
            'cart'   =>  Cart::content(),
            'subtotal' => Cart::subtotal(),
            'success'   =>  "", 
        ]);
        
    }


}
