<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Kris\LaravelFormBuilder\FormBuilder;
use App\User;
use App\Role;
use App\Producto;
use App\Pedido;
use App\PedidosDetalle;
use App\TPedido;
use App\TPedidosDetalle;
use App\Http\Requests\UserFormRequest;
use App\Http\Requests;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;

use Password;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\PasswordBroker;
use Illuminate\Foundation\Auth\ResetsPasswords;

class ProcesosController extends Controller
{
  public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('blockedusers');
    }


   public function aceptar_pago($pedido_id) {

   	if(! \Auth::user()->hasRole('admin') ){
      return redirect()->action('HomeController@pedidos')->with('message-danger','El usuario no tiene privilegios');
    }

   	$pedido = Pedido::find($pedido_id);
   	$pedido->status = 'aceptado';
   	$pedido->pagado = true;
   	$pedido->save();

    return redirect()->action('HomeController@getPedidos')->with('message-success','Se ha aceptado el pago.');
  }

  public function rechazar_pago($pedido_id) {

    if(! \Auth::user()->hasRole('admin') ){
      return redirect()->action('HomeController@pedidos')->with('message-danger','El usuario no tiene privilegios');
    }

    $pedido = Pedido::find($pedido_id);
    $pedido->status = 'rechazado';
    $pedido->pagado = false;
    $pedido->save();

    return redirect()->action('AdminController@getValidarPedidosRecibo')->with('message-warning','Se ha RECHAZADO el pago.');

  }  

  public function t_rechazar_pago($pedido_id) {

    if(! \Auth::user()->hasRole('admin') ){
      return redirect()->action('HomeController@pedidos')->with('message-danger','El usuario no tiene privilegios');
    }

    $pedido = TPedido::find($pedido_id);
    $pedido->status = 'rechazado';
    $pedido->pagado = false;
    $pedido->save();

    return redirect()->action('AdminController@getValidarPedidosCarrito')->with('message-warning','Se ha RECHAZADO el pago.');

  }  


  public function verificar_usr($user_id) {

    $me=\Auth::user();

    if ($usr = User::find($user_id)){
      //return $usr->nombre;
      return "Válido";
    }else{
      return "No Válido";
    }


  } 


}
