<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Kris\LaravelFormBuilder\FormBuilder;
use App\User;
use App\Role;
use App\Producto;
use App\Pedido;
use App\TPedido;
use App\Corte;
use App\PedidosDetalle;
use App\TPedidosDetalle;
use App\Config;
use App\Comisione;
use App\Http\Requests\UserFormRequest;
use App\Http\Requests;
use DB;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;

use Password; 
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\PasswordBroker;
use Illuminate\Foundation\Auth\ResetsPasswords;


class AdminController extends Controller
{
  public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('blockedusers'); 
        /*
        $me=\Auth::user();

        if ($me->hasRole('admin') or $me->hasRole('moderador') ){
            $condition="1=1";
        }else{
          return redirect()->action('HomeController@index')->with('message-danger','Acceso no autorizado');
        }
        */
    }
  public function getViewusr(Request $request)
    {
        $me=User::find($request->get('usrid'));
        $mescorte=Config::getMescorte();

        if (!\Auth::user()->hasRole('admin') ){
            exit();
        } 

        $condition="user_id='".$me->id."' and mespedido='".$mescorte."'"; 
        $pedido = Pedido::whereRaw($condition)->first();

        return view('admin/viewusr')->with([
            'me'   =>  $me,
            'pedido' => $pedido,
            'success'   =>  "",
        ]);
    }


  public function getComisiones () {
  	$me=\Auth::user();

    //dd(Config::getMescorte());

    $condition="user_id='".$me->id."'";
    if (\Auth::user()->hasRole('admin') ){
        $condition="1=1";
    }

    //$condition="pedido_mes_pagado=1";
    $condition="isactive=1";

    $usuarios = User::whereRaw($condition)->get();

    return view('admin/comisiones')->with([
        'usuarios'   =>  $usuarios,
        'success'   =>  "",
    ]);

  } 

  public function getComisionespasadas () {
    $me=\Auth::user();

    //dd(Config::getMescorte());

    //$condition="user_id='".$me->id."'";

    //$condition="pedido_mes_pagado=1";
    $corte = Corte::orderBy('id', 'desc')->first(); 

    $condition="mespedido='".$corte->mescorte."' and true"; //comision_pagar>0

    $comisiones = Comisione::whereRaw($condition)->get();

    return view('admin/comisionespasadas')->with([
        'comisiones'   =>  $comisiones,
        'success'   =>  "",
    ]);

  }

  public function getTodosusuarios () {
    $me=\Auth::user();

    //$condition="mespedido='201701' and comision_pagar>0";
    //$usuariosPagados = User::whereRaw("pedido_mes_pagado='1' and isactive='1' and free_spot='0' ")->get();
    $condition="isactive='1' and free_spot='0'";
    $usuarios = User::whereRaw($condition)->get();

    return view('admin/todosusuarios')->with([
        'usuarios'   =>  $usuarios,
        'success'   =>  "",
    ]);

  }

  public function getBasededatos () {
    $me=\Auth::user();

    //$condition="mespedido='201701' and comision_pagar>0";
    $condition="1=1";
    $usuarios = User::whereRaw($condition)->get();

    return view('admin/basededatos')->with([
        'usuarios'   =>  $usuarios,
        'success'   =>  "",
    ]);

  }

  public function getNuevos () {
    $me=\Auth::user();

    //$condition="mespedido='201701' and comision_pagar>0";
    //$pedidos = Pedido::whereRaw($condition)->orderBy('autorizacion_fecha', 'desc')->get();
    $condition="status='new' or status='preactivo'"; 
    $usuarios = User::whereRaw($condition)->orderBy('id', 'DESC')->get();

    return view('admin/nuevos')->with([
        'usuarios'   =>  $usuarios,
        'success'   =>  "",
    ]);

  }

  public function getHacercorte () {
    ini_set('max_execution_time',600);
    $me=\Auth::user();
    if (\Auth::user()->hasRole('admin') ){
        

        //dd(Config::getMescorte());
        
        $condition="1=1";
        $usuarios = User::whereRaw("isactive='1'")->get();
        $usuariosPagados = User::whereRaw("pedido_mes_pagado='1' and isactive='1' and free_spot='0' and directos>=2 ")->get();

        $mescorte=Config::getMescorte();
        //calcular nuevo mes corte
        $nextmescorte = date("Ym", strtotime("+1 months", strtotime(date("Y-m-d"))));
        if (!is_numeric($mescorte)) { dd("Error mes corte no valido"); }

        //Crear registro de recorte en CORTE
        if(!$corte = Corte::where('mescorte','=',$mescorte)->first()){
            $corte = new Corte();
        }

        $corte->mescorte=$mescorte;
        $corte->red_total=count($usuarios);
        $corte->red_pagados=count($usuariosPagados);
        $corte->red_sin_pagar=count($usuarios)-count($usuariosPagados);
        $corte->save();

        //Cancelar Pedido no pagados
        $condition="1=1";
        $pedidos = Pedido::whereRaw($condition)->get();
        foreach($pedidos as $p){
            $p->vigente=false;
            $p->save();
        }

        //Cacluar las comisiones y guardar en COMISIONES
        $tPedidoCantidad=0;
        $tPago=0;
        $tRed=0;
        $tRedSpagar=0;
        $tRedPagados=0;
        $tComision=0;
        $tProxPedido=0;
        $tComisionRetenida=0;
        $tPagarComision=0;
        foreach($usuarios as $s){
            $pedidoCantidad=$s->getTotalPedidoMes();
            $pago=$s->pedido_mes_pagado ? $s->getTotalPedidoMes() : 0;
            $red=$s->getHijos();
            $redPagados=$s->getHijosPagados();
            $redSpagar=$s->getHijos() - $redPagados; 
            
            $comision=$s->getComision();
            $bono=$s->pedido_mes_pagado ? $s->getBono() : 0;
            $comision_tienda=$s->pedido_mes_pagado ? $s->getComisionTienda() : 0;
            
            $comision_total=$comision+$bono+$comision_tienda;

            //chekar si el mes pasado tuvieron descuento 
            // if $s->solo_red=1;

            $costo_siguiente_pedido=350;

            if ($s->solo_red==1){
                $costo_siguiente_pedido=$costo_siguiente_pedido-75; 
            } 

            $proxPedido=$comision_total > $costo_siguiente_pedido ? 0 : ($costo_siguiente_pedido-$comision_total);
            $comisionRetenida= $comision_total >= $costo_siguiente_pedido ? $costo_siguiente_pedido  : $comision_total ;
            $pagarComision= ($comision_total-$costo_siguiente_pedido) > 0 ? ($comision_total-$costo_siguiente_pedido)  : 0 ;

            //Crear registro individual por usuario
            if(!$corteUsr = Comisione::where('mespedido','=',$mescorte)->where('user_id','=',$s->id)->first()){
                $corteUsr = new Comisione();
                $corteUsr->mespedido=$mescorte;
                $corteUsr->user_id=$s->id;
                $corteUsr->save();
            }

            //Validar doblemente que el pedido este pagado con la tabla pedidos
            $corteUsr->pedido_id=$s->ultimo_pedido_id;
            $pedido = Pedido::find($s->ultimo_pedido_id);
            
            if (count($pedido)!=0){
                if (!$pedido->pagado){$comision=0;}
                $corteUsr->pagado=$pedido->pagado;
                $corteUsr->pago_pedido=$pedido->total;
            }else{
                $comision=0;
                $corteUsr->pagado=false;
                $corteUsr->pago_pedido=0;
            }
            
            $corteUsr->red_total=$red;
            $corteUsr->red_pagados=$redPagados;
            $corteUsr->red_sin_pagar=$redSpagar;
            $corteUsr->red_comisiones_total=$s->getComision(true);
            $corteUsr->comision_total=$comision_total;
            $corteUsr->bono=$bono;
            $corteUsr->descuento_proximo_pedidos=$comisionRetenida;
            $corteUsr->comision_pagar=$pagarComision;
            $corteUsr->directos=$s->directos;
            $corteUsr->comision_tienda=$comision_tienda;
            $corteUsr->save();

            $tPedidoCantidad+=$pedidoCantidad;
            $tPago+=$pago;
            $tRed+=$red;
            $tRedPagados+=$redPagados;
            $tRedSpagar+=$redSpagar;
            $tComision+=$comision;
            $tProxPedido+=$proxPedido;
            $tComisionRetenida+=$comisionRetenida;
            $tPagarComision+=$pagarComision;

            //Generar Nuevos Pedidos
            if(!$pedido = Pedido::where('mespedido','=',$nextmescorte)->where('user_id','=',$s->id)->first()){
                //Agregar pedido de Inicio

                $producto_inicio = Producto::find(2);
                $pedido = new Pedido();
                $pedido->save();
                $pedido->user_id = $s->id;
                $pedido->sub_total = $proxPedido/1.16;
                $pedido->iva = $proxPedido-($proxPedido/1.16);
                $pedido->total = $proxPedido;
                

                $pedido->status = 'pendiente';
                $pedido->mespedido = $nextmescorte;
                
                $pedido->save();

                //Reiniciar table de Users
                $s->ultimo_pedido=$pedido->created_at;
                $status_pago=$s->pedido_mes_pagado ? "activo" : "mes_pasdo_no_pagado";
                $s->status=$status_pago;
                $s->save(); 

                if(!$pedidoDetalle = PedidosDetalle::where('pedido_id','=',$pedido->id)->where('producto_id','=',$producto_inicio->id)->first()){
                    $pedidoDetalle = new PedidosDetalle();
                    $pedidoDetalle->pedido_id = $pedido->id;
                    $pedidoDetalle->producto_id = $producto_inicio->id;
                }

                $pedidoDetalle->cantidad = 1;
                $pedidoDetalle->costo_unitario = $producto_inicio->costo_neto;
                $pedidoDetalle->total = $producto_inicio->costo_neto;
                $pedidoDetalle->save();

                //si pago con comision aplicar el descuento
                if ($proxPedido<$costo_siguiente_pedido){
                    $producto_descuento = Producto::find(1);
                    
                    if(!$pedidoDetalle = PedidosDetalle::where('pedido_id','=',$pedido->id)->where('producto_id','=',$producto_descuento->id)->first()){
                        $pedidoDetalle = new PedidosDetalle();
                        $pedidoDetalle->pedido_id = $pedido->id;
                        $pedidoDetalle->producto_id = $producto_inicio->id;
                    }
                    $pedidoDetalle->cantidad = 1;
                    $pedidoDetalle->costo_unitario = $comisionRetenida*-1;
                    $pedidoDetalle->total = $comisionRetenida*-1;
                    $pedidoDetalle->save();                    
                }

                //aplicar descuento solo red
                if ($s->solo_red==1){
                  $producto_inicio = Producto::find(5);
                  $pedidoDetalle = new PedidosDetalle();
                  $pedidoDetalle->pedido_id = $pedido->id;
                  $pedidoDetalle->producto_id = $producto_inicio->id;
                  $pedidoDetalle->cantidad = 1;
                  $pedidoDetalle->costo_unitario = $producto_inicio->costo_neto;
                  $pedidoDetalle->total = $producto_inicio->costo_neto;
                  $pedidoDetalle->save();
                } 

                $pedido->save();



            }
        }


        $corte->red_comisiones_total=$tPagarComision;
        $corte->red_comisiones_retenida=$tComisionRetenida;
        $corte->ingresos_pedidos=$tPago;
        $corte->save();

        //Cambiar Variable General del Programa 
        $confifmes = Config::where('configuracion','=','mescorte')->first();
        $confifmes->valor=$nextmescorte;
        $confifmes->save();

        //actualizar los valores en la base de datos con los nuevos status
        //validar si ya se pago el total del pedido
        $usuarios = User::whereRaw("isactive='1'")->get();
        foreach (User::all() as $u) {
            //reiniciar a todos los usuarios no pagado
            $u->pedido_mes_pagado=false;
            $u->ultimo_pedido_id=null;
            $u->save();
            if($pedido = Pedido::where('mespedido','=',$nextmescorte)->where('user_id','=',$u->id)->first()){
                if ($pedido->total==0){
                    $pedido->status = 'aceptado';
                    $pedido->pagado = true;
                    $pedido->autorizacion_user_id = 254; 
                    $pedido->autorizacion_fecha = date('Y-m-d H:i:s'); 
                    $pedido->autorizacion_tipo = "Sistema"; 
                    $pedido->save();
                }
            }
        }

        


        echo "FIN del corte";
        exit();

        return view('admin/corte')->with([
            'usuarios'   =>  $usuarios,
            'success'   =>  "",
        ]);
        

    }

    

  }


  public function getValidarPedidosEfectivo()
    {

        $me=\Auth::user();

        $mescorte=Config::getMescorte();

        $condition="user_id='".$me->id."'";
        if (\Auth::user()->hasRole('admin') ){
            $condition="1=1";
        } 
        $condition="status='pendiente' and entregado=0 and mespedido='".$mescorte."'";
        $pedidos = Pedido::whereRaw($condition)->get();


        $me=\Auth::user();

        return view('admin/validarPedidos')->with([
            'me'   =>  $me,
            'pedidos'   =>  $pedidos,
            'success'   =>  "",
        ]);
    }

    public function getValidarPedidosRecibo()
    {

        $me=\Auth::user();
        $mescorte=Config::getMescorte();

        $condition="user_id='".$me->id."'";
        if (\Auth::user()->hasRole('admin') ){
            $condition="1=1";
        }
        $condition="(status='validacion' or status='rechazado') and entregado=0 and mespedido='".$mescorte."'";
        $pedidos = Pedido::whereRaw($condition)->get();


        $me=\Auth::user();

        return view('admin/validarPedidosRecibo')->with([
            'me'   =>  $me,
            'pedidos'   =>  $pedidos,
            'success'   =>  "",
        ]);
    }

    public function getValidarPedidosCarrito()
    {

        $me=\Auth::user();
        $mescorte=Config::getMescorte();

        if (\Auth::user()->hasRole('admin') ){
            $condition="1=1";
        }
        $condition="mespedido='".$mescorte."'";
        $pedidos = TPedido::whereRaw($condition)->get();

        //fix presentador id 
        foreach($pedidos as $p){
            $u = User::find($p->user_id);
            $p->presentador_id=$u->presentador_id;
            $p->save();
        }


        $me=\Auth::user();

        return view('admin/validarPedidosCarrito')->with([
            'me'   =>  $me,
            'pedidos'   =>  $pedidos,
            'success'   =>  "",
        ]);
    }

  public function getPedidosPagados(Request $request)
    {

        $me=\Auth::user();
        $mescorte=Config::getMescorte(); 

        $condition="user_id='".$me->id."'";
        if (\Auth::user()->hasRole('admin') ){
            $condition="1=1";
        }

        if ( $request->get('corte')!=""){
            $condition="pagado='1' and mespedido='".$request->get('corte')."'";
            $scorte=$request->get('corte');
        }else{
            $condition="pagado='1' and mespedido='".$mescorte."'";
            $scorte=$mescorte;
        }




        //$condition="pagado='1' and mespedido='".$mescorte."'";
        $pedidos = Pedido::whereRaw($condition)->orderBy('autorizacion_fecha', 'desc')->get();
        $cortes = Corte::orderBy('id', 'desc')->get(); 

        $me=\Auth::user();

        return view('admin/pedidosPagados')->with([
            'me'   =>  $me,
            'pedidos'   =>  $pedidos,
            'mescorte'   =>  $mescorte,
            'scorte'   =>  $scorte,
            'cortes'   =>  $cortes,
            'success'   =>  "",
        ]);
    }

    public function getPedidosEntregados(Request $request)
    {

        $me=\Auth::user();
        $mescorte=Config::getMescorte();

        if ( $request->get('corte')!=""){
            $condition="pagado='1' and mespedido='".$request->get('corte')."'";
            $scorte=$request->get('corte');
        }else{
            $condition="pagado='1' and mespedido='".$mescorte."'";
            $scorte=$mescorte;
        }

        
        $pedidos = Pedido::whereRaw($condition)->get(); 
        $cortes = Corte::orderBy('id', 'desc')->get(); 


        return view('admin/pedidosEntregados')->with([
            'me'   =>  $me,
            'pedidos'   =>  $pedidos,
            'cortes'   =>  $cortes,
            'mescorte'   =>  $mescorte,
            'scorte'   =>  $scorte,
            'success'   =>  "",
        ]);
    }

  public function getAceptarPagoEfectivo($pedido_id) {

    $me=\Auth::user();

    $pedido = Pedido::find($pedido_id);
    $pedido->status = 'aceptado';
    $pedido->pagado = true;
    $pedido->autorizacion_user_id = $me->id; 
    $pedido->autorizacion_fecha = date('Y-m-d H:i:s'); 
    $pedido->autorizacion_tipo = "Efectivo"; 

    $pedido->save();
    
    return "success";
  }

  public function getCancelarPagoEfectivo($pedido_id) {
    $me=\Auth::user();

    $pedido = Pedido::find($pedido_id);
    $pedido->status = 'pendiente';
    $pedido->pagado = false;
    $pedido->autorizacion_user_id = $me->id; 
    $pedido->autorizacion_fecha = date('Y-m-d H:i:s'); 
    $pedido->autorizacion_tipo = "Cancelado"; 
    if ($pedido->comprobante!=""){$pedido->status = "validacion";}
    $pedido->save();
    
    return "success";
  }

  public function getAceptarPagoRecibo($pedido_id) {

    $me=\Auth::user();

    $pedido = Pedido::find($pedido_id);
    $pedido->status = 'aceptado';
    $pedido->pagado = true;
    $pedido->autorizacion_user_id = $me->id; 
    $pedido->autorizacion_fecha = date('Y-m-d H:i:s'); 
    $pedido->autorizacion_tipo = "recibo"; 

    $pedido->save();
    
    return "success";
  }

  public function getCancelarPagoRecibo($pedido_id) {
    $me=\Auth::user();

    $pedido = Pedido::find($pedido_id);
    $pedido->status = 'validacion';
    $pedido->pagado = false;
    $pedido->autorizacion_user_id = $me->id; 
    $pedido->autorizacion_fecha = date('Y-m-d H:i:s'); 
    $pedido->autorizacion_tipo = "Cancelado"; 
    $pedido->save();
    
    return "success";
  }

  //------- TIENDA CARRITO --------

  public function getTAceptarPagoRecibo($pedido_id) {

    $me=\Auth::user();

    $pedido = TPedido::find($pedido_id);
    $pedido->status = 'aceptado';
    $pedido->pagado = true;
    $pedido->autorizacion_user_id = $me->id; 
    $pedido->autorizacion_fecha = date('Y-m-d H:i:s'); 
    $pedido->autorizacion_tipo = "recibo"; 

    $pedido->save();
    
    return "success";
  }

  public function getTCancelarPagoRecibo($pedido_id) {
    $me=\Auth::user();

    $pedido = TPedido::find($pedido_id);
    $pedido->status = 'validacion';
    $pedido->pagado = false;
    $pedido->autorizacion_user_id = $me->id; 
    $pedido->autorizacion_fecha = date('Y-m-d H:i:s'); 
    $pedido->autorizacion_tipo = "Cancelado"; 
    $pedido->save();
    
    return "success";
  }

  //------- TIENDA CARRITO --------


  public function getAceptarEntregado($pedido_id) {

    $me=\Auth::user();

    $pedido = Pedido::find($pedido_id);
    
    if ($pedido->pagado){
        $pedido->status = 'entregado';
        $pedido->entregado = true;
        $pedido->save();
        return "success";
    }else{
        return "No se puede entregar porque no esta pagado.";
    }
  }

  public function getCancelarEntregado($pedido_id) {
    $me=\Auth::user();

    $pedido = Pedido::find($pedido_id);
    $pedido->status = 'pendiente';
    $pedido->entregado = false;
    $pedido->save();
    
    return "success";
  }


  public function getCrearDescuento($pedido_id) {
 
    $pedido = Pedido::find($pedido_id);
    
    //agregar el descuento
      $ban=0;
      $recompra=0;
      $kitinicio=0;
      foreach (PedidosDetalle::where('pedido_id','=',$pedido->id)->get() as $pd) {
        if ($pd->producto_id==4 or $pd->producto_id==5){
          $ban=1;
        }
        if ($pd->producto_id==2 ){
          $recompra=1;
        }
        if ($pd->producto_id==3 ){
          $kitinicio=1;
        }

      }
      if ($ban==0 ){
        //identificar si es el kit de inicio o el de re compra
        if ($kitinicio==1){
          $producto_inicio = Producto::find(4);
          $pedidoDetalle = new PedidosDetalle();
          $pedidoDetalle->pedido_id = $pedido->id;
          $pedidoDetalle->producto_id = $producto_inicio->id;
          $pedidoDetalle->cantidad = 1;
          $pedidoDetalle->costo_unitario = $producto_inicio->costo_neto;
          $pedidoDetalle->total = $producto_inicio->costo_neto;
          $pedidoDetalle->save();
        }
        if ($recompra==1){
          $producto_inicio = Producto::find(5);
          $pedidoDetalle = new PedidosDetalle();
          $pedidoDetalle->pedido_id = $pedido->id;
          $pedidoDetalle->producto_id = $producto_inicio->id;
          $pedidoDetalle->cantidad = 1;
          $pedidoDetalle->costo_unitario = $producto_inicio->costo_neto;
          $pedidoDetalle->total = $producto_inicio->costo_neto;
          $pedidoDetalle->save();
        } 
      }

    $pedido->save();
    return "success";

  }

  public function getCancelarDescuento($pedido_id) {
    $me=\Auth::user();

    $pedido = Pedido::find($pedido_id);
    //remover todos los descuentos
    foreach (PedidosDetalle::where('pedido_id','=',$pedido->id)->get() as $pd) {
        PedidosDetalle::where('id','=',$pd->id)->where('producto_id','=',4)->delete();
        PedidosDetalle::where('id','=',$pd->id)->where('producto_id','=',5)->delete();
    }
    $pedido->save();
    return "success";
  }

}
