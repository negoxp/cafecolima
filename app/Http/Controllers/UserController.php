<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

//use Illuminate\Http\Request;
use Kris\LaravelFormBuilder\FormBuilder;
use App\User;
use App\Role;
use App\Pedido;
use App\Http\Requests\UserFormRequest;
use App\Http\Requests;
use App\Config;


use Password;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\PasswordBroker;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Request;

class UserController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
        $this->middleware('blockedusers');
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
        //$users = User::with('roles')->get();
        $users = User::all();
        //return view('home');
        return view('users.index',compact("users"));
	}

		/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create(FormBuilder $formBuilder)
	{
        $form = $formBuilder->create('App\Forms\UserForm',[
            'method'    =>  'POST',
            'url'       =>  route('users.store')
        ]);

        //dd(Role::all()->lists('display_name', 'id'));
		//
        return view('users.form', ['form'=>$form,'title'=>'New User']);

	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(UserFormRequest $request)
	{
		//
        $user = new User([
            'nombre'      =>  $request->get('nombre'),
            'apellidos'      =>  $request->get('apellidos'),
            'celular'      =>  $request->get('celular'),
            'email'     =>  $request->get('email'),
            'password'  =>  bcrypt($request->get('password')),
            //'restaurant_id'     =>  $request->get('restaurant_id',null),
            //'district_id'     =>  $request->get('district_id'.null)

        ]);

        //$user->roles()->getResults()
        $user->save();
        $user->roles()->sync([]);
        $user->roles()->attach($request->get('role_id'));
        return \Redirect::route('users.index')->with('success','User registered successfully');
	}

	 /**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id, FormBuilder $formBuilder)
	{

		//
        $user = User::findOrFail($id);
        $form = $formBuilder->create('App\Forms\UserForm',[
            'model'     =>  $user,
            'method'    =>  'PUT',
            'url'       =>  route('users.update',['id'=>$user->id])
        ]);

        return view('users.form', ['form'=>$form,'title'=>'Editar Usuario','id'=>$user->id]);

	}

	public function editme($id, FormBuilder $formBuilder)
	{

				$me=\Auth::user();

        $user = User::findOrFail($id);
        $form = $formBuilder->create('App\Forms\UserEditme',[
            'model'     =>  $user,
            'method'    =>  'PUT',
            'url'       =>  route('users.update',['id'=>$user->id]) 
        ]);

        return view('users.editme', ['form'=>$form,'title'=>'Edit User','id'=>$user->id, 'me'=>$me]);

	}

		/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, UserFormRequest $request)
	{
        $me=\Auth::user();

        if ($me->id==$id){
        	//YO MISMO ME ESTOY ACTUALIZANDO 
        	$user = User::find($id);

        	//Guardar Foto
        	$file = Request::file('foto');

					if ($file){

					    $path = '/uploads/perfil/'.$me->id;
					    $photoname = date('Ymd').'_'.$user->id.'_'.uniqid().'.'. $file->guessClientExtension();

					    //$file->move(public_path() . $path, $photoname);
					    $path = public_path('/uploads/perfil/' . $photoname);
					    Image::make($file->getRealPath())->resize(300, 300)->save($path);

					    $user->foto = $photoname;
							$user->save();
					}

					if ($request->get('nombre')!=""){
						$user->nombre = $request->get('nombre');
					}
					if ($request->get('apellidos')!=""){
						$user->apellidos = $request->get('apellidos');
					}

					$user->email = $user->email;
					$user->fecha_nac = $request->get('fecha_nac');
					$user->telefono = $request->get('telefono');
					$user->celular = $request->get('celular');
					$user->direccion = $request->get('direccion');
					$user->colonia = $request->get('colonia');
					$user->ciudad = $request->get('ciudad');
					$user->estado = $request->get('estado');
					$user->cp = $request->get('cp');
					$user->solo_red = $request->get('solo_red');
					$user->nombre_beneficiario = $request->get('nombre_beneficiario');

					if ($request->get('banco_nombre')!=""){
						$user->banco_nombre = $request->get('banco_nombre');
						$user->banco_titular = $request->get('banco_titular');
						$user->banco_cuenta = $request->get('banco_cuenta');
						$user->banco_clabe = $request->get('banco_clabe');
					}
					$user->can_edit = false;
					
					

	        if($request->get('password'))
            $user->password  = bcrypt($request->get('password'));
	        $user->save(); 

        	return redirect()->action('UserController@editme', array('user_id' => $id))->with('message-success','Se actualizó correctamente');
        }else{
        	//TEWNGO QUE SER ADMIN PARA MODIFICAR A OTRO
	        if (\Auth::user()->hasRole('admin') ){
	        	$user = User::find($id);
		        $user->update([
		            'nombre'      =>  $request->get('nombre'),
		            'apellidos'      =>  $request->get('apellidos'),
		            'celular'      =>  $request->get('celular'),
		            'email'     =>  $request->get('email'),
		        ]);
		        if($request->get('password'))
	            $user->password  = bcrypt($request->get('password'));
		        
	          $user->nombre = $request->get('nombre');
						$user->apellidos = $request->get('apellidos');
						$user->celular = $request->get('celular');
						$user->fundador = $request->get('fundador');
						$user->solo_red = $request->get('solo_red');
						$user->isactive = $request->get('isactive'); 
						$user->can_edit = $request->get('can_edit');
						$user->free_spot = $request->get('free_spot');
						$user->presentador_id = $request->get('presentador_id');
		        $user->save();

		        $user->roles()->sync([]);
		        $user->roles()->attach($request->get('role_id'));

		        //Check si tiene descuento para actualizar solo el mes actual 
	          $mespedido=Config::getMescorte();
	          if($pedido = Pedido::where('mespedido','=',$mespedido)->where('user_id','=',$user->id)->first()){
	              $pedido->checkDescuento();
	          }

		        return \Redirect::route('users.index')->with('success','User updated successfully');
	        }
        }

        

        

        
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
        User::destroy($id);
        return \Redirect::route('users.index')->with('success','User deleted successfully');
	}



}
