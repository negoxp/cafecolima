<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Mail;

class OutController extends Controller
{
  public function unirme(Request $request)
    {
    	return view('out.unirme');
    }

  public function invite(Request $request)
    {
    	
    	Mail::send('email.unirme', ['request'=>$request ], function ($m){
                $m->to(["negoxp@gmail.com", "delrinconelizondo@gmail.com", "juanpablo_80@hotmail.com"], env('MAIL_NAME_SEND'))->subject('Cafe Colima :: Unirme a la RED');
            });
    	return view('out.gracias');

    }
}
