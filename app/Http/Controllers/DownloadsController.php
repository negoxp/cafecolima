<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Kris\LaravelFormBuilder\FormBuilder;
use App\User;
use App\Role;
use App\Producto;
use App\Pedido;
use App\TPedido;
use App\PedidosDetalle;
use App\Http\Requests\UserFormRequest;
use App\Http\Requests;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;

use Password;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\PasswordBroker;
use Illuminate\Foundation\Auth\ResetsPasswords;



class DownloadsController extends Controller
{	

	public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('blockedusers');
    }


   public function download($pedido_id) {

   	$me=\Auth::user();

   	$pedido = Pedido::find($pedido_id);

    $file_path = public_path($pedido->comprobante);
    return response()->download($file_path);
  }

  public function tdownload($pedido_id) {

    $me=\Auth::user();

    $pedido = TPedido::find($pedido_id);

    $file_path = public_path($pedido->comprobante);
    return response()->download($file_path);
  }

  public function sendmails(){
    /*
    foreach (User::all() as $u) {
            
            $response = Password::sendResetLink(['email' => $u->email], function($message)
            {
                $message->subject('Bienvenido a Café Sabor a Colima');
            });
            
            //$this->info("Enviado ".$u->email);
            echo "Enviado ".$u->email."</br>";
            //sleep(1);

        }
    */
  }
}
