<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

//use Illuminate\Http\Request;
use Kris\LaravelFormBuilder\FormBuilder;
use App\User;
use App\Role;
use App\Producto;
use App\Pedido;
use App\PedidosDetalle;
use App\Http\Requests\UserFormRequest;
use App\Http\Requests;
use App\Config;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;

use Password;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\PasswordBroker;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Request;

class PedidoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('blockedusers');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

      $pedido = Pedido::find($id);
      

      return view('pedidos/edit')->with([
            'pedido'   =>  $pedido,
            'success'   =>  "",
        ]);

    }

    public function invoice($id)
    {

      $pedido = Pedido::find($id);
      $pedidoDetalles =PedidosDetalle::where('pedido_id','=',$pedido->id)->get();
      $cliente = User::find($pedido->user_id);

      return view('pedidos/invoice')->with([
            'pedido'   =>  $pedido,
            'pedidoDetalles'=>  $pedidoDetalles, 
            'cliente'   =>  $cliente,
            'success'   =>  "",
        ]);

    }

    public function invoice_print($id)
    {

      $pedido = Pedido::find($id);
      $pedidoDetalles =PedidosDetalle::where('pedido_id','=',$pedido->id)->get();
      $cliente = User::find($pedido->user_id);

      return view('pedidos/invoice_print')->with([
            'pedido'   =>  $pedido,
            'pedidoDetalles'=>  $pedidoDetalles, 
            'cliente'   =>  $cliente,
            'success'   =>  "",
        ]); 

    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $me=\Auth::user();

      	$rules = array(
            'comprobante'       => 'required',
            'referencia'       => 'required',
        );
        $validator = Validator::make(Input::all(), $rules);

        

        // process the login
        if ($validator->fails()) {
            return Redirect::to('pedidos/' . $id . '/edit')->withErrors($validator);
        } else {
            // store
        	$file = Request::file('comprobante');

            if ($file){
                $pedido = Pedido::find($id);
                $pedido->fecha_pago = date('Y-m-d H:i:s');
                $pedido->status = 'validacion';

                //$file = Input::file('comprobante');
                $path = '/uploads/comprobantes/'.$me->id;
                $photoname = date('Ymd').'_'.$pedido->id.'_'.uniqid().'.'. $file->guessClientExtension();

                $file->move(public_path() . $path, $photoname);
                $pedido->comprobante = $path. '/' . $photoname;
                //save referencia 
                $pedido->referencia=Request::get('referencia');
                $pedido->save();
			} 


            

            // redirect
            return redirect()->action('HomeController@getPedidos')->with('message','Se subiro correctamente el comprobante de pago.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
