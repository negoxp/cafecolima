<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\User;
use App\Producto;
use App\Pedido;
use App\PedidosDetalle;


class Config extends Model
{
  public static function getCompraInicial() 
  {
    $data = DB::table('configs')->where('configuracion','=','compra_inicial')->first();
    return $data->valor;
  }

  public static function getMinimoCompra() 
  {
    $data = DB::table('configs')->where('configuracion','=','minimo_compra')->first();
    return $data->valor;
  }
  public static function getMaximoNivelComision() 
  {
    $data = DB::table('configs')->where('configuracion','=','maximo_nivel_comision')->first();
    return $data->valor;
  }
  public static function getMaximoHijos() 
  {
    $data = DB::table('configs')->where('configuracion','=','maximo_hijos')->first();
    return $data->valor;
  }
  public static function getCaducidadSocio()  
  {
    $data = DB::table('configs')->where('configuracion','=','caducidad_socio')->first();
    return $data->valor;
  }
  public static function getComisionPar() 
  {
    $data = DB::table('configs')->where('configuracion','=','comision_par')->first();
    return $data->valor;
  }
  public static function getDiaCorte() 
  {
    $data = DB::table('configs')->where('configuracion','=','dia_corte')->first();
    return $data->valor;
  }
  public static function getMescorte() 
  {
    $data = DB::table('configs')->where('configuracion','=','mescorte')->first();
    return $data->valor;
  }
  public static function getDiasLimitePago() 
  {
    $data = DB::table('configs')->where('configuracion','=','dias_limite_pago')->first();
    return $data->valor;
  }
  public static function getInvitados() 
  {
    $data = DB::table('configs')->where('configuracion','=','invitados')->first();
    return $data->valor;
  }


}
