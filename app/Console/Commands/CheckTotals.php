<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use App\Role;
use App\Producto;
use App\Pedido;
use App\PedidosDetalle;
use App\TPedido;
use App\TPedidosDetalle;
use App\Comisione;
use DB;

class CheckTotals extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cafecolima:checktotals';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Verificar el Total de cada pedido';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info("Verificar el Total de cada pedido");
        $mespedido="201709";
        /*
        //Chekar el total de puntos por pedido
        foreach (TPedido::all() as $tp) {
            $tp->save();
        }
        */
        
        
        foreach (Comisione::where('mespedido','=','201705')->where('pagado','=','0')->get() as $c) {
            $u=User::find($c->user_id);
            $alguna_vez_pagado= $u->alguna_vez_pagado();

            if (!$alguna_vez_pagado){
                $p = Pedido::where('mespedido','=','201706')->where('user_id','=',$c->user_id)->first();
                $pd = PedidosDetalle::where('pedido_id','=',$p->id)->first();
                $pd->producto_id=3;
                $pd->costo_unitario=500;
                $pd->total=500;
                if ($u->solo_red==1){
                  $producto_inicio = Producto::find(4);
                  $pedidoDetalle = new PedidosDetalle();
                  $pedidoDetalle->pedido_id = $p->id;
                  $pedidoDetalle->producto_id = $producto_inicio->id;
                  $pedidoDetalle->cantidad = 1;
                  $pedidoDetalle->costo_unitario = $producto_inicio->costo_neto;
                  $pedidoDetalle->total = $producto_inicio->costo_neto;
                  $pedidoDetalle->save();
                }
                
                $pd->save();
                $p->save(); 
            }
        }
        
        foreach (Pedido::all() as $p) {
            $p->save();
        }
        

    }
}
