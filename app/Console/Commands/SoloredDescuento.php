<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use App\Role;
use App\Producto;
use App\Pedido;
use App\PedidosDetalle;
use DB;

class SoloredDescuento extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cafecolima:soloredDescuento';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info("Begin Solo Red Descuentos task");

        foreach (User::where('solo_red','=',true)->get() as $u) {

            //Buscar el Pedido 
            $mespedido = date('Ym');
            if($pedido = Pedido::where('mespedido','=',$mespedido)->where('user_id','=',$u->id)->first()){
                $pedido->checkDescuento();
            }
        }

        $this->info("End Solo Red Descuentos task");


    }
}
