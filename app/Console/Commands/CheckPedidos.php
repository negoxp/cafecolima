<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use App\Role;
use App\Producto;
use App\Pedido;
use App\PedidosDetalle;
use DB;

use Password;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\PasswordBroker;
use Illuminate\Foundation\Auth\ResetsPasswords;

class CheckPedidos extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cafecolima:checkpedidos';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Verificar que todos los usaurios tengan co relacion a un pedido';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info("Begin checkpedidos task");
        $mespedido="201704";

        foreach (User::all() as $u) {
            
            /*
            $u->red_integrantes = $u->getHijos();
            $u->red_integrantes_pagados = $u->getHijosPagados();
            $u->save();


            if(!$pedido = Pedido::where('mespedido','=',$mespedido)->where('user_id','=',$u->id)->first()){
                //Agregar pedido de Inicio
                $producto_inicio = Producto::find(3);
                $pedido = new Pedido();
                $pedido->user_id = $u->id;
                $pedido->sub_total = $producto_inicio->costo_neto/1.16;
                $pedido->iva = $producto_inicio->costo_neto-($producto_inicio->costo_neto/1.16);
                $pedido->total = $producto_inicio->costo_neto;
                $pedido->status = 'pendiente';
                $pedido->mespedido = $mespedido;
                $u->ultimo_pedido=$pedido->created_at;
                $u->status="new";
                $pedido->save();

                $pedidoDetalle = new PedidosDetalle();
                $pedidoDetalle->pedido_id = $pedido->id;
                $pedidoDetalle->producto_id = $producto_inicio->id;
                $pedidoDetalle->cantidad = 1;
                $pedidoDetalle->costo_unitario = $producto_inicio->costo_neto;
                $pedidoDetalle->total = $producto_inicio->costo_neto;
                $pedidoDetalle->save();
                $pedido->save();
            }
            */
            
            /*
            $response = Password::sendResetLink(['email' => $u->email], function($message)
            {
                $message->subject('Bienvenido a Café Sabor a Colima');
            });
            $this->info("Enviado ".$u->email);
            sleep(10);
            */


            //------- BONO -------

            //CORTE DE INGRESO
            //Primer pago aceptado
            //$mescorte=Config::getMescorte();
            $condition="user_id='".$u->id."' and pagado=1"; 
            $pedido = Pedido::whereRaw($condition)->orderBy('id', 'asc')->first();
            if (count($pedido)>0){
                $u->corte_ingreso=$pedido->mespedido;
                $u->save();
            }

            //FECHA DE ULTIMO PAGO ACEPTADO  
            $condition="user_id='".$u->id."' and pagado=1"; 
            $pedido = Pedido::whereRaw($condition)->orderBy('id', 'desc')->first();
            if (count($pedido)>0){
                $u->fecha_ultimo_pago_aceptado=$pedido->autorizacion_fecha;
                $u->save();
            }

            //------- END BONO -------

            //------- PARA MANTENER EL DESCUENTO  -------
            $p=$u->get_ultimo_pedido();
            if ( $p and $p->hasDescuento() ){
                $u->solo_Red=1;
                $u->save();
            }
            //------- END PARA MANTENER EL DESCUENTO  -------

            //------- CALCULOS DE DIRECTOS --------
            $condition="presentador_id='".$u->id."' and free_spot=0"; 
            $directos = User::whereRaw($condition)->count();
            $u->directos=$directos;
            $u->save();
            //------- END CALCULOS DE DIRECTOS --------

        }

        $this->info("End checkpedidos task");
    }
}
