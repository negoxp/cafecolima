<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use App\Role;
use App\Producto;
use App\Pedido;
use App\PedidosDetalle;
use App\Comisione;
use DB;

class CreateFreeSpot extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cafecolima:createfreespots';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Crea o libera los espacios de usarios no pagados';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info("Crea o libera los espacios de usarios no pagados");

        
        foreach (User::whereRaw("(status='mes_pasdo_no_pagado' or status='new' or status='preactivo') and isactive='1' and free_spot='0' ")->get() as $u) {
            //$this->info($u->id." ".$u->nombrecompleto()); 
            if ($u->createfreespots()){
               $this->info($u->id." ".$u->nombrecompleto()); 
            } 
            
        }
        
        /*
        //PARA VERIFICAR SI ALGUNA VEZ HAN PAGADO 
        foreach (User::whereRaw("status!='new' and isactive='1' and free_spot='0' ")->get() as $u) {
            $alguna_vez_pagado= $u->alguna_vez_pagado();
            if ($alguna_vez_pagado){
                $p = Pedido::where('mespedido','=','201703')->where('user_id','=',$u->id)->first();
                if ( $pd = PedidosDetalle::whereRaw('pedido_id='.$p->id.' and producto_id=3')->first() ){
                    $pd->producto_id=2;
                    $pd->costo_unitario=350;
                    $pd->total=350;
                    $pd->save();
                    $p->save(); 
                    $this->info("usr: ".$u->id);
                }
                
            }
        }

        */

        $this->info("End createfreespots");


    }
}
