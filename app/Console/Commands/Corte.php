<?php


namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use App\Role;
use App\Producto;
use App\Pedido;
use App\PedidosDetalle;
use App\Config;
use DB;

use Password;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\PasswordBroker;
use Illuminate\Foundation\Auth\ResetsPasswords;

class Corte extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cafecolima:corte';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Los procesos que se deben de correr antes del corte';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info("Begin corte task");
        $mespedido=Config::getMescorte();

        foreach (User::all() as $u) {

            //------- BONO -------
                //CORTE DE INGRESO
                $condition="user_id='".$u->id."' and pagado=1"; 
                $pedido = Pedido::whereRaw($condition)->orderBy('id', 'asc')->first();
                if (count($pedido)>0){
                    $u->corte_ingreso=$pedido->mespedido;
                    $u->save();
                }

                //FECHA DE ULTIMO PAGO ACEPTADO  
                $condition="user_id='".$u->id."' and pagado=1"; 
                $pedido = Pedido::whereRaw($condition)->orderBy('id', 'desc')->first();
                if (count($pedido)>0){
                    $u->fecha_ultimo_pago_aceptado=$pedido->autorizacion_fecha;
                    $u->save();
                }
            //------- END BONO -------

            //------- PARA MANTENER EL DESCUENTO  -------
                $p=$u->get_ultimo_pedido();
                if ( $p and $p->hasDescuento() ){
                    $u->solo_Red=1;
                    $u->save();
                }
            //------- END PARA MANTENER EL DESCUENTO  -------

            //------- CALCULOS DE DIRECTOS --------
                //patch aovid directos validation
                $uids_str  = "7,1249,1291,1273,1256,1257,1276,1145,1258,1259,1185,1260,1262,1186,1146,1132,1292,1354,1277,1267,1279,1188,1147,1293,1294,1189,1295,1296,1190,1148,1133,1127,1297,1298,1191,1299,1300,1192,1149,1301,1302,1278,1349,1304,1194,1153,1134,1305,1306,1195,1307,1308,1280,1157,1309,1310,1197,1355,1316,1198,1158,1135,1128,188,1317,1318,1199,1323,1324,1200,1159,1325,1326,1201,1327,1330,1202,1160,1136,1331,1335,1203,1336,1337,1204,1271,1338,1339,1205,1523,1340,1343,1206,1162,1137,1129,1509,1207,1208,1163,1209,1210,1164,1140,1281,1212,1165,1213,1214,1166,1139,1130,292,162,1282,1283,1167,1217,1284,1168,1085,1219,1220,1169,1221,1222,1170,1086,1008,1527,1223,1224,1272,1225,1357,1358,1226,1172,1009,1518,1227,1228,1173,1229,1230,1174,1004,1003,1002,1231,1232,1175,1233,1285,1176,1141,1530,1236,1286,1177,1287,1288,1274,1142,1131,1289,1290,1179,1242,1243,1275,1143,1245,1246,1181,1247,1511,1356,1248,1182,1144,307,293,171,161";
                $uids = explode(",", $uids_str);
                $condition="presentador_id='".$u->id."' and free_spot=0"; 
                $directos = User::whereRaw($condition)->count();
                
                if (in_array($u->id, $uids)) {
                    $u->directos=99;
                }else{
                    $u->directos=$directos;
                }
                $u->save();
            //------- END CALCULOS DE DIRECTOS --------

        }

        $this->info("End corte task");
    }
}
