<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TPedidoDetalle extends Model
{
  public function producto()
    {
        return $this->belongsTo('App\Producto');
    }
}
