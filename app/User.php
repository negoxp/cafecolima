<?php

namespace App;

//use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Auth\Authenticatable;
use Illuminate\Notifications\Notification;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use Illuminate\Support\Facades\Auth;
use DB;
use App\User;
use App\Producto;
use App\Pedido;
use App\PedidosDetalle;
use App\Config;
use App\Comisione;
use App\TPedido;
use App\TPedidoDetalle;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract 
{

    use Authenticatable, CanResetPassword, EntrustUserTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'users';

    protected $fillable = [
        'name', 'email', 'password',
    ];

    public function pedidos(){ 
      return $this->hasMany('App\Pedido');
    }

    public function tpedidos(){ 
      return $this->hasMany('App\TPedido');
    }

    public function comisiones() 
    {
        return $this->belongsToMany('App\Comisione');
    }
 
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }

     public function save(array $options = [])
       {
          // before save code 
          //$this->telefono='3121084130';

          //despues de haber creado crear su primer pedido 
          parent::save();
          
          //dd($this->status);
          /*
          if ($this->status=="nuevo"){
            $producto_inicio = Producto::find(3);

            $pedido = new Pedido();
            $pedido->user_id = $this->id;
            $pedido->sub_total = $producto_inicio->costo_neto/1.16;
            $pedido->iva = $producto_inicio->costo_neto-($producto_inicio->costo_neto/1.16);
            $pedido->total = $producto_inicio->costo_neto;
            //pendiente validacion aceptado rechazado
            $pedido->status = 'pendiente';
            $pedido->mespedido = date('Ym');
            

            $this->ultimo_pedido=$pedido->created_at;
            $this->status="new";
            //parent::save();
            $pedido->save();

            dd($pedido);

            echo "despues de salvado";
            exit(); 

            $pedidoDetalle = new PedidosDetalle();
            $pedidoDetalle->pedido_id = $pedido->id;
            $pedidoDetalle->producto_id = $producto_inicio->id;
            $pedidoDetalle->cantidad = 1;
            $pedidoDetalle->costo_unitario = $producto_inicio->costo_neto;
            $pedidoDetalle->total = $producto_inicio->costo_neto;
            //$pedidoDetalle->save();
          }
          */

          //parent::save();

          //exit();
          // after save code
       }
    public function shortname() 
    {
        if (!$this->isactive){
          return "##** BLOQUEADO **##";
        }
        $nombres = explode(" ", $this->nombre);
        $apellidos = explode(" ", $this->apellidos);
        return $nombres[0]." ".$apellidos[0];
    }

    public function getNameid($id) 
    {
      if ($u = User::find($id)){
        return $u->nombre." ".$u->apellidos;
      }
    }

    public function cantidad_ultimo_pedido() 
    {
        
        $mescorte=Config::getMescorte();
        $condition="user_id='".$this->id."' and mespedido='".$mescorte."'"; 
        $pedido = Pedido::whereRaw($condition)->first();
        if (count($pedido)>0){
          return $pedido->total;
        }
        return "NA";
    }

    public function status_corte_pasado() 
    {
        $mescorte=Config::getMescorte();
        $condition="user_id='".$this->id."' and mespedido='".$mescorte."'"; 
        $pedido = Pedido::whereRaw($condition)->first();
        if (count($pedido)>0){
          return $pedido->pagado ?  "Si" : "No";
        }
        return "NA";
    }

    public function get_ultimo_pedido() 
    {
        $mescorte=Config::getMescorte();
        $condition="user_id='".$this->id."' and mespedido='".$mescorte."'"; 
        if ($pedido = Pedido::whereRaw($condition)->first()){
          return $pedido;
        }else{
          $producto_inicio = Producto::find(2);
          $pedido = new Pedido();
          $pedido->save();
          $pedido->user_id = $this->id;
          $pedido->status = 'pendiente';
          $pedido->mespedido = $mescorte;
          $pedido->save();

          if(!$pedidoDetalle = PedidosDetalle::where('pedido_id','=',$pedido->id)->where('producto_id','=',$producto_inicio->id)->first()){
              $pedidoDetalle = new PedidosDetalle();
              $pedidoDetalle->pedido_id = $pedido->id;
              $pedidoDetalle->producto_id = $producto_inicio->id;
          }
          $pedidoDetalle->cantidad = 1;
          $pedidoDetalle->costo_unitario = $producto_inicio->costo_neto;
          $pedidoDetalle->total = $producto_inicio->costo_neto;
          $pedidoDetalle->save();

          $pedido->save();
          return $pedido;
        } 

        


    }

    public function alguna_vez_pagado() 
    {
      $pagado=false;
      foreach (Pedido::whereRaw("user_id='".$this->id."' and pagado=1")->get() as $p) {
        $pagado=true;
      }
      return $pagado;
    }

    public function pago_ultimos_cortes() 
    { 
      //si es usuario nuevo
      if ( $tc=Comisione::whereRaw("user_id='".$this->id."'")->count()<1 ){
        //dd(Comisione::whereRaw("user_id='".$this->id."'")->count());
        return true;
      }

      foreach (Comisione::whereRaw("user_id='".$this->id."'")->take(2)->orderBy('id', 'desc')->get() as $c) {
        if ($c->pagado==1){
          return true;
        }
      }
      return false;
    }


    public function pago_ultimos_status_color() 
    { 
      
      //si es usuario nuevo
      if ( $tc=Comisione::whereRaw("user_id='".$this->id."'")->count()<1 ){
        //amarillo
        return "tree-pic-nopaid_yellow ";
      }
      //si es usuario tiene un mes
      if ( $tc=Comisione::whereRaw("user_id='".$this->id."'")->count()==1 ){
        $c = Comisione::whereRaw("user_id='".$this->id."'")->first();
        //amarillo
        if ($c->pagado==1){
          return "tree-pic-nopaid_yellow ";
        }else{
          return "tree-pic-nopaid_red ";
        }
      }

      $i=1;
      foreach (Comisione::whereRaw("user_id='".$this->id."' ")->take(2)->orderBy('id', 'desc')->get() as $c) {
        if ($c->pagado==1 and $i==1){
          //amarillo
          return "tree-pic-nopaid_yellow ";
        }
        if ($c->pagado==1 and $i==2){
          //rojo
          return "tree-pic-nopaid_red ";
        }
        $i++;
      }
      return "tree-pic-nopaid_red ";
    }

    public function createfreespots(){
      $pedido=$this->get_ultimo_pedido();
      $alguna_vez_pagado=$this->alguna_vez_pagado();
      $pedido->hasDescuento() ? $costo_pkt_inico=350.00 : $costo_pkt_inico=350.00 ; 

      if ($pedido->pagado==0 and $pedido->total==$costo_pkt_inico and !$alguna_vez_pagado){
        if (date("Y-m-d H:i:s", strtotime("+8 days", strtotime($this->created_at))) < date("Y-m-d H:i:s")){
          $this->email_backup=$this->email;
          $this->isactive=0;
          $this->free_spot=1;
          $this->save();
          return true;
        } 

      } 

      //si no ha pagado en los ultimos 2 meses
      if (!$this->pago_ultimos_cortes()){
        $this->email_backup=$this->email;
        $this->isactive=0;
        $this->free_spot=1;
        $this->save();
        return true;
      }


      return false;

    }

    

    public function nombrecompleto() 
    {
        return $this->nombre." ".$this->apellidos;
    }

    /*
    //FUNCION CADUCADA
    public function getHijos($person=null, $nivel=-1){
        $person ? $me=$person : $me=$this;

        $userid=$me->id;

          $nivel++;
          $condition="padre_id='$userid'";
          $sons = User::whereRaw($condition)->orderBy('hijo_posicion', 'asc')->limit(2)->get();
          
          if (count($sons)==0){
            return $nivel;
          }else{
            foreach($sons as $s){
              $nivel_aux=$s->getHijos($s, $nivel);
              if ($nivel_aux>$nivel) {$nivel=$nivel_aux;}
            }
            return $nivel;
          }

    }
    */

    public function getHijosPagadosPercent($person=null){
      $person ? $me=$person : $me=$this;
      return $me->getHijos()>0 ? number_format((float)$me->getHijosPagados()/$me->getHijos()*100,2) : "0" ;
    }
    public function getHijosPagados($person=null, $pagados=0, $topparent=null, $nivel=0){
        $person ? $me=$person : $me=$this;
        $maximo_nivel_comision=Config::getMaximoNivelComision();

        if (!$person){ 
          $topparent=$this->id; 
        }else{
          $nivel++;
        } 

        if ($nivel<$maximo_nivel_comision){
          $userid=$me->id;
          $condition="padre_id='$userid'";//aqui le movi para chekar  and free_spot=0
          $sons = User::whereRaw($condition)->orderBy('hijo_posicion', 'asc')->limit(2)->get();
          
          foreach($sons as $s){
            $pagados=$s->getHijosPagados($s, $pagados, $topparent, $nivel);
            if ($s->pedido_mes_pagado and $s->free_spot==0){$pagados++;}
          }
        }

        return $pagados;    
    }

    public function getHijos($person=null, $pagados=0, $topparent=null, $nivel=0, $cadh=""){
        $person ? $me=$person : $me=$this;
        $maximo_nivel_comision=Config::getMaximoNivelComision();
        if (!$person){ 
          $topparent=$this->id; 
        }else{
          $nivel++;
        } 

        //relacion entre $me y $topparent
        if ($nivel<$maximo_nivel_comision){
          $userid=$me->id;
          $condition="padre_id='$userid' ";//and free_spot=0
          $sons = User::whereRaw($condition)->orderBy('hijo_posicion', 'asc')->limit(2)->get();
          
          foreach($sons as $s){
            $pagados=$s->getHijos($s, $pagados, $topparent, $nivel, $cadh);
            if ($s->free_spot==0){$pagados++; $cadh=$cadh.",".$s->id;}
            //$pagados++;
          }
        }
        //return $cadh; 
        return $pagados;    
    }

    public function getComision($force=false){
        $comisionPar=Config::getComisionPar();
        $pagados=$this->getHijosPagados();      
        if (($this->pedido_mes_pagado and $this->directos>=2)or $force){
          $comision = floor($pagados / 2)*$comisionPar;
          return $comision;
        }else{
          return 0;
        }   
    }

    public function getBono($person=null, $force=false){
        $person ? $me=$person : $me=$this;
        
        $condition="presentador_id='".$me->id."' and free_spot=0 and corte_ingreso='201705' and pedido_mes_pagado=1"; 
        $directos = User::whereRaw($condition)->count();
        $bono=$directos*50;
        return $bono;
        /*
        foreach($directos as $d){
          $p=$d->get_ultimo_pedido();
          if ($p->pagado){
            $bono=$bono*50;
          }
        }
        */

        //---Bono Pasado por invitar a 2
        /*
        $pagados=$this->getHijosPagados();
        if ($this->pedido_mes_pagado or $force){   
          //Bono de 300 pesos si solo si invito a 2 directos a su cuenta entre  4 al 25 abril
          $condition="presentador_id='".$me->id."' and corte_ingreso='201704' and fecha_ultimo_pago_aceptado>='2017-04-04' and fecha_ultimo_pago_aceptado<= '2017-04-27' and pedido_mes_pagado=1 "; 
          $invitaciones = User::whereRaw($condition)->get();
          if ( count($invitaciones)>= 2 ){
            return 300;
          }else{
            return 0;
          }
        }else{
          return 0;
        }
        */
        //---Bono Pasado por invitar a 2
    }

    public function getComisionTienda($person=null, $force=false){
        $person ? $me=$person : $me=$this;
        
        $mescorte=Config::getMescorte();

        $comision_tienda=0;
        $condition="presentador_id='".$me->id."' and mespedido='$mescorte' and pagado='1' "; 
        $directos = TPedido::whereRaw($condition)->get();
        foreach($directos as $d){ 
          $comision_tienda=$comision_tienda+$d->comision;  
        }
        return $comision_tienda;
    }
    
    public function getPuntosTienda($person=null, $force=false){
        $person ? $me=$person : $me=$this;
        
        $mescorte=Config::getMescorte();

        $puntos_tienda=0;
        $condition="user_id='".$me->id."' and pagado='1' "; 
        $tpedidos = TPedido::whereRaw($condition)->get();
        foreach($tpedidos as $tp){ 
          $puntos_tienda=$puntos_tienda+$tp->puntos;  
        }
        return $puntos_tienda;
    }



    public function getTotalPedidoMes($person=null){
      $person ? $me=$person : $me=$this;

      $mescorte=Config::getMescorte();

      $condition="user_id='".$me->id."' and mespedido='".$mescorte."'"; 
      $pedido = Pedido::whereRaw($condition)->first();

      if ($pedido){
        return $pedido->total;
      }
      else{
        return 0;
      }
      
    }


    public function getInvitacionesPendientes($person=null){
      $person ? $me=$person : $me=$this;

      $condition="presentador_id='".$me->id."' and (status='new' or status='preactivo')"; 
      $invitaciones = User::whereRaw($condition)->get();

      return count($invitaciones);

    }

    public function getSociosInvitarFaltantes($person=null){ 
      $person ? $me=$person : $me=$this;

      $condition="presentador_id='".$me->id."' and free_spot=0"; 
      $invitaciones = User::whereRaw($condition)->get();

      return count($invitaciones);
    }

    public function getComisionTotal($pedido=350, $force=false){
        $comisionPar=Config::getComisionPar();
        $comisionTienda=$this->getComisionTienda();
        
        $pagados=$this->getHijosPagados();
        $p=$this->get_ultimo_pedido();
        if ($this->pedido_mes_pagado or $force){
          if ($this->solo_red==1 or $p->hasDescuento()){
            $pedido=$pedido-75; 
          }
          $comision = floor($pagados / 2)*$comisionPar;
          $comision = $comision + $comisionTienda - $pedido;
          return number_format((float)$comision,2);
        }else{
          return number_format(0,2);
        } 
        
    }


    public function getNivel($person=null, $nivel=0){
        
        $maximo_nivel_comision=Config::getMaximoNivelComision();

        $person ? $me=$person : $me=$this;
        $sessionusr=\Auth::user();
        $userid=$me->id;

        if ($nivel<$maximo_nivel_comision){
          $nivel++;
          $condition="padre_id='$userid'";
          $sons = User::whereRaw($condition)->orderBy('hijo_posicion', 'asc')->limit(2)->get();
          
          if (count($sons)==0){
            return $nivel-1;
          }else{
            foreach($sons as $s){
              $nivel_aux[]=$s->getNivel($s, $nivel);
              //if ($nivel_aux>$nivel) {$nivel=$nivel_aux;}
            }
            return $value = max($nivel_aux);  
          }
        }
        else{
          return $nivel;
        }

    }

    public function ultimo_dia_pago(){
      $dia_corte=date("d",strtotime($this->created_at));
      //return date("Y-m-").$dia_corte;
      if (date('d')<=25){
        return date("Y-m-25");
      }else{
        return date('Y-m-25', strtotime('+1 month',strtotime(date('Y-m-d'))));
      } 
    }

    public function getPhoto(){
        
        if (!$this->isactive){
          return asset('').'vendor/theme/images/avatars/blocked.png';
        }
        if(file_exists(public_path().'/uploads/perfil/'.$this->foto) and $this->foto!="" ){
            return asset('').'uploads/perfil/'.$this->foto;
        }else{
            is_numeric($this->foto_default) ? $f=$this->foto_default : $f=1;
            return asset('').'vendor/theme/images/avatars/'.$f.'.png';
        }

        //if ($this->foto)
        //return asset('');//$this->name;
    }

    public function getPrintFreeSpot($parentId, $hijoPosicion){
        $output="
        <li>
          <a href='#' class='mymodal' hijoPosicion='".$hijoPosicion."' parentId='".$parentId."'>
               <div class='tree-pic'>
                    <img src='".asset('')."vendor/theme/images/newuser.png' alt=''/>
                    <div></div>
               </div>
            </a>
        </li>"; 

        return $output;
    }

    public function getPrintFreeSpotRemplace($parentId, $hijoPosicion, $person=null, $nivel=0){
        $person ? $me=$person : $me=$this;
        $sessionusr=\Auth::user();

        $output="
        <li>
          <a href='#' class='mymodal' hijoPosicion='".$hijoPosicion."' parentId='".$parentId."' userid='".$me->id."'>
               <div class='tree-pic tree-pic-remplaced'>
                    <img src='".asset('')."vendor/theme/images/newuser.png' alt=''/>
                    <div></div>
               </div>
            </a>
            ".$this->getPrintSons($me, $nivel+1)."
        </li>"; 

        return $output;
    }

    public function getPrintPerson($person=null, $nivel=0){
        $person ? $me=$person : $me=$this;
        $sessionusr=\Auth::user();

        //calses de status 
        //tree-pic-paid tree-pic-new

        

        ($me->status=="new" or $me->status=="preactivo") ? $nuevo="bg-info" : $nuevo="bg-inverse";
        $me->pedido_mes_pagado ? $paid="tree-pic-paid " : $paid= $me->pago_ultimos_status_color(); 
        $me->fundador ?  $fundador='<i class="fa fa-certificate fundador_icon" aria-hidden="true"></i>' : $fundador=''; 

        $linkusr= $sessionusr->hasRole('admin') ? url('/admin/viewusr?usrid='.$me->id) : "#linkusr";

        $output="
        <li id='firstnode'>
          <a href='".$linkusr."' data-original-title='".$me->nombrecompleto()."' data-content='Socio: ".$me->id." <br/> # Patrocinados: ".$me->getSociosInvitarFaltantes()." <br/>  ' 
                   data-placement='bottom' data-trigger='hover' class='popovers'>
               <div class='tree-pic ".$paid."text-center'>
                    <img src='".$me->getPhoto()."' alt=''/>".
                    $fundador.
                    "<span class='badge ".$nuevo."'>$nivel</span>".
                    "<div>".$me->shortname()."</div>
               </div>
            </a>
            ".$this->getPrintSons($me, $nivel+1)."
        </li>";
        return $output;
    }

    public function getPrintSons($me, $nivel){
        $userid=$me->id;
        $sessionusr=\Auth::user();
        $maximo_nivel_comision=Config::getMaximoNivelComision();

        //if ($nivel<=$maximo_nivel_comision){
        if ($nivel<=$maximo_nivel_comision or $sessionusr->hasRole('admin')){
          $condition="padre_id='$userid'";
          $sons = User::whereRaw($condition)->orderBy('hijo_posicion', 'asc')->limit(2)->get();
          $sons_output="";
          $i=1;

          foreach($sons as $s){
              if ($s->hijo_posicion==2 and count($sons)==1){
                  $sons_output.=$this->getPrintFreeSpot($userid, $i);
                  $i++;
              }

              if ($s->free_spot==1){
                $sons_output.=$this->getPrintFreeSpotRemplace($userid, $i, $s, $nivel);
              }else{
                $sons_output.=$this->getPrintPerson($s, $nivel); 
              }
              $i++;
          }

          for ($i = $i; $i <= 2; $i++) {
              $sons_output.=$this->getPrintFreeSpot($userid, $i);
          }


          $output="
          <ul>
              ".$sons_output."
          </ul>
          ";
          return $output;
        }
    }

    




}
