<?php namespace App\Forms;

use Kris\LaravelFormBuilder\Form;
use App\Role;
use \DB;
class UserEditme extends Form
{
    public function buildForm()
    {
        //dd($this->model->can_edit);
        // Add fields here...
        if ($this->model->can_edit==1){
            $this->add('nombre','text', ['label' => 'Nombre'])->add('apellidos','text', ['label' => 'Apellidos']);
        }else{
            //$this->add('nombre','text', ['label' => 'Nombre', ['disabled' => 'disabled'] ])->add('apellidos','text', ['label' => 'Apellidos', 'disabled' => 'disabled']);
        }

        $this->add('foto','file', ['label'     => 'Foto'])
            ->add('fecha_nac','date', ['label'     => 'fecha_nac'])
            ->add('telefono','tel', ['label'     => 'telefono'])
            ->add('celular','tel', ['label'     => 'celular'])
            ->add('direccion','text', ['label'     => 'direccion'])
            ->add('colonia','text', ['label'     => 'colonia'])
            ->add('ciudad','text', ['label'     => 'ciudad'])
            ->add('estado','text', ['label'     => 'estado'])
            ->add('cp','text', ['label'     => 'cp']);
        $this->add('genero', 'select', [
                'choices'   => ['h' => 'Hombre', 'm' => 'Mujer'],
                //'selected'  => h, //Selecciona el id del rol si ya existe o el 1 si es nuevo
                'label'     => 'Genero'
        ]);

        if ($this->model->can_edit==1 or $this->model->banco_nombre==''){
        $this->add('banco_nombre','text', ['label'     => 'banco_nombre'])
            ->add('banco_titular','text', ['label'     => 'banco_titular'])
            ->add('banco_cuenta','text', ['label'     => 'banco_cuenta'])
            ->add('banco_clabe','text', ['label'     => 'banco_clabe']);
        }    
        $this->add('password', 'repeated', [
                'type' => 'password',    // can be anything that fits <input type="type-here">
                'second_name' => 'password_confirmation', // defaults to name_confirmation
                'first_options' => ['label' => 'Contraseña', 'value' => ''],   // Same options available as for text type
                'second_options'    => ['label' => 'Repetir Contraseña'],   // Same options available as for text type
            ]);

        $this->add('nombre_beneficiario','text', ['label'     => 'Nombre Beneficiario']);
        $this->add('solo_red','checkbox', ['label'     => 'Aplica para descuento de SOLO LA RED -$75 pesos en tu próximo pedido']);

            /*
            ->add('restaurant_id', 'select', [
                'choices'   => Restaurant::orderBy(DB::Raw('LENGTH(name), name'))->lists('name', 'id'),
                //'selected'  => $this->model ? $this->model->roles()->first()->id : 1, //Selecciona el id del rol si ya existe o el 1 si es nuevo
                'label'     => 'Location'
            ])
            ->add('district_id', 'select', [
                'choices'   => District::all()->lists('name', 'id'),
                //'selected'  => $this->model ? $this->model->roles()->first()->id : 1, //Selecciona el id del rol si ya existe o el 1 si es nuevo
                'label'     => 'District'
            ])
            ->add('can_view_d9', 'checkbox', [

                //'selected'  => $this->model ? $this->model->roles()->first()->id : 1, //Selecciona el id del rol si ya existe o el 1 si es nuevo
                'label'     => 'Can View D9'
            ])
            ->add('can_legal', 'checkbox', [

                //'selected'  => $this->model ? $this->model->roles()->first()->id : 1, //Selecciona el id del rol si ya existe o el 1 si es nuevo
                'label'     => 'Can View Legal'
            ])
            */


            $this->add('guardar', 'submit', [
                'attr' => ['class' => 'btn btn-primary']
            ]);
    }
}