<?php namespace App\Forms;

use Kris\LaravelFormBuilder\Form;
use App\Role;
use \DB;
class UserForm extends Form
{
    public function buildForm()
    {
        // Add fields here...
        $this
            ->add('nombre','text', ['label'     => 'Nombre'])
            ->add('apellidos','text')
            ->add('celular','text')
            ->add('email','email')
            ->add('presentador_id','number')
            ->add('password', 'repeated', [
                'type' => 'password',    // can be anything that fits <input type="type-here">
                'second_name' => 'password_confirmation', // defaults to name_confirmation
                'first_options' => ['label' => 'Contraseña', 'value' => ''],   // Same options available as for text type
                'second_options'    => ['label' => 'Repetir Contraseña'],   // Same options available as for text type
                
            ])
            ->add('role_id', 'select', [
                'choices'   => ['3' => 'Socio', '1' => 'Administrador', '2' => 'Moderador de Pagos'],
                'selected'  => 3, //Selecciona el id del rol si ya existe o el 1 si es nuevo
                'label'     => 'Rol'
            ])
            ->add('genero', 'select', [
                'choices'   => ['h' => 'Hombre', 'm' => 'Mujer'],
                //'selected'  => h, //Selecciona el id del rol si ya existe o el 1 si es nuevo
                'label'     => 'Genero'
            ])


            ->add('foto','file', ['label'     => 'Foto'])
            ->add('fecha_nac','date', ['label'     => 'fecha_nac'])
            ->add('telefono','tel', ['label'     => 'telefono'])
            ->add('direccion','text', ['label'     => 'direccion'])
            ->add('colonia','text', ['label'     => 'colonia'])
            ->add('ciudad','text', ['label'     => 'ciudad'])
            ->add('estado','text', ['label'     => 'estado'])
            ->add('cp','text', ['label'     => 'cp'])
            
            ->add('banco_nombre','text', ['label'     => 'banco_nombre'])
            ->add('banco_titular','text', ['label'     => 'banco_titular'])
            ->add('banco_cuenta','text', ['label'     => 'banco_cuenta'])
            ->add('banco_clabe','text', ['label'     => 'banco_clabe'])

            ->add('nombre_beneficiario','text', ['label'     => 'Nombre Beneficiario'])


            ->add('fundador','checkbox')
            ->add('solo_red','checkbox', ['label'     => 'Aplica para descuento de SOLO LA RED'])
            ->add('isactive','checkbox', ['label'     => 'Activado'])
            ->add('can_edit','checkbox', ['label'     => 'Puede editar'])
            ->add('free_spot','checkbox', ['label'     => 'Espacio Libre'])
            /*
            ->add('restaurant_id', 'select', [
                'choices'   => Restaurant::orderBy(DB::Raw('LENGTH(name), name'))->lists('name', 'id'),
                //'selected'  => $this->model ? $this->model->roles()->first()->id : 1, //Selecciona el id del rol si ya existe o el 1 si es nuevo
                'label'     => 'Location'
            ])
            ->add('district_id', 'select', [
                'choices'   => District::all()->lists('name', 'id'),
                //'selected'  => $this->model ? $this->model->roles()->first()->id : 1, //Selecciona el id del rol si ya existe o el 1 si es nuevo
                'label'     => 'District'
            ])
            ->add('can_view_d9', 'checkbox', [

                //'selected'  => $this->model ? $this->model->roles()->first()->id : 1, //Selecciona el id del rol si ya existe o el 1 si es nuevo
                'label'     => 'Can View D9'
            ])
            ->add('can_legal', 'checkbox', [

                //'selected'  => $this->model ? $this->model->roles()->first()->id : 1, //Selecciona el id del rol si ya existe o el 1 si es nuevo
                'label'     => 'Can View Legal'
            ])
            */
            ->add('guardar', 'submit', [
                'attr' => ['class' => 'btn btn-primary']
            ])

        ;
    }
}