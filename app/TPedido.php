<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\User;
use App\Producto;
use App\TPedido;
use App\TPedidoDetalle;
use App\Config;


class TPedido extends Model
{
  public function user()
  {
      return $this->belongsTo('App\User');
  }

  public function pedidosDetalles(){
      return $this->hasMany('App\TPedidoDetalle');
  }

  public function autorizadopor() 
    {
        $user = User::find($this->autorizacion_user_id);
        if ($user){
          $nombres = explode(" ", $user->nombre);
          $apellidos = explode(" ", $user->apellidos);
          return $nombres[0]." ".$apellidos[0]." ".$this->autorizacion_fecha  ;
        }
    }

  public function presentadorNombre() 
    {
        $user = User::find($this->presentador_id);
        if ($user){
          $nombres = explode(" ", $user->nombre);
          $apellidos = explode(" ", $user->apellidos);
          return $nombres[0]." ".$apellidos[0]  ;
        }
    }

  public function getStatusCss(){
		//pendiente validacion aceptado rechazado
		switch ($this->status) {
	    case "pendiente":
	        $class ="primary";
	        break;
	    case "validacion":
	    		$class ="info";
	        break;
	    case "aceptado":
	        $class ="success";
	        break;
	    case "rechazado":
	        $class ="danger";
	        break;
      case "entregado":
          $class ="inverse";
          break;
	    default:
	       $class = "default";
	       break;
		}
		return $class;	
	}
    
    public function save(array $options = [])
   {

      $total=0;
      $puntos=0;
      foreach (TPedidoDetalle::where('t_pedido_id','=',$this->id)->get() as $pd) {
        //$total=$total+($pd->cantidad*$pd->costo_unitario);
        $producto = Producto::find($pd->producto_id);
        $pd->puntos=$pd->cantidad*$producto->puntos;
        $pd->save();
        $puntos=$puntos+($pd->cantidad*$producto->puntos);
      }
        
      $this->puntos=$puntos;
      
      //despues de haber creado crear su primer pedido 
      parent::save();

   }

}
