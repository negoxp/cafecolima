<?php

namespace App;

//use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Auth\Authenticatable;
use Illuminate\Notifications\Notification;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use Illuminate\Support\Facades\Auth;
use DB;
use App\User;
use App\Producto;
use App\Pedido;
use App\PedidosDetalle;
use App\Config;

class Pedido extends Model
{
  public function user()
  {
      return $this->belongsTo('App\User');
  }

  public function pedidosDetalles(){
      return $this->hasMany('App\PedidosDetalle');
  }

  public function autorizadopor() 
    {
        $user = User::find($this->autorizacion_user_id);
        if ($user){
          $nombres = explode(" ", $user->nombre);
          $apellidos = explode(" ", $user->apellidos);
          return $nombres[0]." ".$apellidos[0]." ".$this->autorizacion_fecha  ;
        }
    }

  public function getStatusCss(){
		//pendiente validacion aceptado rechazado
		switch ($this->status) {
	    case "pendiente":
	        $class ="primary";
	        break;
	    case "validacion":
	    		$class ="info";
	        break;
	    case "aceptado":
	        $class ="success";
	        break;
	    case "rechazado":
	        $class ="danger";
	        break;
      case "entregado":
          $class ="inverse";
          break;
	    default:
	       $class = "default";
	       break;
		}
		return $class;	
	}

  public function checkDescuento(){ 
    //Si tiene pedido. buscar si tiene descuento
    /*
    $ban=0;
    foreach (PedidosDetalle::where('pedido_id','=',$this->id)->get() as $pd) {
      if ($pd->id=="4" or $pd=="5"){
        $ban=1;
      }
    }

    if ($ban==0 ){
        $producto_inicio = Producto::find(4);
        $pedidoDetalle = new PedidosDetalle();
        $pedidoDetalle->pedido_id = $this->id;
        $pedidoDetalle->producto_id = $producto_inicio->id;
        $pedidoDetalle->cantidad = 1;
        $pedidoDetalle->costo_unitario = $producto_inicio->costo_neto;
        $pedidoDetalle->total = $producto_inicio->costo_neto;
        $pedidoDetalle->save();
      
    }
*/
    $mescorte=Config::getMescorte();
    $user = User::find($this->user_id);
    if ($user->solo_red){
      //agregar el descuento
      $ban=0;
      $recompra=0;
      $kitinicio=0;
      foreach (PedidosDetalle::where('pedido_id','=',$this->id)->get() as $pd) {
        if ($pd->producto_id==4 or $pd->producto_id==5){
          $ban=1;
        }
        if ($pd->producto_id==2 ){
          $recompra=1;
        }
        if ($pd->producto_id==3 ){
          $kitinicio=1;
        }

      }
      if ($ban==0 ){
        //identificar si es el kit de inicio o el de re compra
        if ($kitinicio==1){
          $producto_inicio = Producto::find(4);
          $pedidoDetalle = new PedidosDetalle();
          $pedidoDetalle->pedido_id = $this->id;
          $pedidoDetalle->producto_id = $producto_inicio->id;
          $pedidoDetalle->cantidad = 1;
          $pedidoDetalle->costo_unitario = $producto_inicio->costo_neto;
          $pedidoDetalle->total = $producto_inicio->costo_neto;
          $pedidoDetalle->save();
        }
        if ($recompra==1){
          $producto_inicio = Producto::find(5);
          $pedidoDetalle = new PedidosDetalle();
          $pedidoDetalle->pedido_id = $this->id;
          $pedidoDetalle->producto_id = $producto_inicio->id;
          $pedidoDetalle->cantidad = 1;
          $pedidoDetalle->costo_unitario = $producto_inicio->costo_neto;
          $pedidoDetalle->total = $producto_inicio->costo_neto;
          $pedidoDetalle->save();
        } 
      }

    }else{
      //remover todos los descuentos
      foreach (PedidosDetalle::where('pedido_id','=',$this->id)->get() as $pd) {
        PedidosDetalle::where('id','=',$pd->id)->where('producto_id','=',4)->delete();
        PedidosDetalle::where('id','=',$pd->id)->where('producto_id','=',5)->delete();
      }
    }

    $this->save();

  }


  public function hasDescuento(){ 
    $mescorte=Config::getMescorte();
    $user = User::find($this->user_id);

    $ban=false;
    $recompra=0;
    $kitinicio=0;
    foreach (PedidosDetalle::where('pedido_id','=',$this->id)->get() as $pd) {
      if ($pd->producto_id==4 or $pd->producto_id==5){
        $ban=true;
      }
      if ($pd->producto_id==2 ){
        $recompra=1;
      }
      if ($pd->producto_id==3 ){
        $kitinicio=1; 
      }

    }

    return $ban;

  }
	
  public function save(array $options = [])
   {
      // before save code 
      //$this->telefono='3121084130';
      //Recalcular totales

      $total=0;
      foreach (PedidosDetalle::where('pedido_id','=',$this->id)->get() as $pd) {
        $total=$total+($pd->cantidad*$pd->costo_unitario);
      }

      $this->total=$total;
      $this->sub_total = $total/1.16;
      $this->iva = $total-($total/1.16);

      //despues de haber creado crear su primer pedido 
      parent::save();

      $mescorte=Config::getMescorte();

      if($this->status=="aceptado" or $this->status=="entregado"){
      	if ($this->mespedido==$mescorte){
          $user = User::find($this->user_id);
        	$user->pedido_mes_pagado=true;
          $user->ultimo_pedido_id=$this->id;
          $user->status="activo";
        	$user->save();
        }
      }else{
        if ($this->mespedido==$mescorte){
        	$user = User::find($this->user_id);
        	$user->pedido_mes_pagado=false;
          $user->ultimo_pedido_id=null;
        	$user->save();
        }
      }

      //exit();
      // after save code 
   }
}
