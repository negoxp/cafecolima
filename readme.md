# Café Sabor a Colima

[![Build Status](https://travis-ci.org/laravel/framework.svg)](https://travis-ci.org/laravel/framework)
[![Total Downloads](https://poser.pugx.org/laravel/framework/d/total.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Stable Version](https://poser.pugx.org/laravel/framework/v/stable.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Unstable Version](https://poser.pugx.org/laravel/framework/v/unstable.svg)](https://packagist.org/packages/laravel/framework)
[![License](https://poser.pugx.org/laravel/framework/license.svg)](https://packagist.org/packages/laravel/framework)


[![N|Solid](http://floresc.com/cafecolima/images/logo.png)](http://www.cafecolima.com)

Desarrollo de sistema multinivel de emprendedores para “Café Sabor a Colima” 

Reglas de Negocio. 
1.	Sistema multinivel de recompensas basado en un árbol jerárquico.
2.	Máximo 2 hijos en cada nodo (puede haber espacio entre la red) 
3.	Compra de Ingreso $500 en el mes. 
4.	Compra mínima de cada mes posterior a la inscripción $350 (Puede variar según nivel de membresía)
5.	Como miembro si no haces el consumo personal se pierde todo derecho a comisión 
6.	Se descontará de manera automática el consumo mínimo del siguiente mes de las comisiones a pagar, parcial o total. 
7.	Se elimina/caduca un miembro de la red si no ha hecho la compra mínima.  
8.	La comisión se calcula por cada par de hijos que pagaron el consumo mínimo  hasta el nivel 11 como máximo de $40
9.	El corte se hará de manera mensual el último día de cada mes. 

Estructura del sistema.

Módulos
	Usuarios (Miembro, Moderador de pagos, Admin)
-	Acceso usuarios (email y contraseña)
-	Recuperación de contraseña
-	Bloque de cuentas
	
	Miembros (Vistas)
-	Escritorio
o	Listado de usuarios de tu red (Nuevos, pagados, deben)
o	Proyección de comisiones
o	Detalle de usuario en particular (información general)
-	Alta Miembros 
-	Historial Compras
o	Impresión recibo
o	Subir comprobante de pago
-	Historial de Comisiones
o	Detalle de comisiones
	

	Moderados Pagos
-	Escritorio
o	Listado de pagos pendientes 
o	Aceptar pago
-	Historial pagos moderados
o	Revertir status de pago (si es en el mismo mes, antes del corte
	



	Admin
-	Variables de Negocio 
o	Compra Inicial
o	Mínimo de compra 
o	Máximo nivel de comisión 
o	Máximo de hijos por nodo
o	Mínimo de compra según nivel de membresía 
o	Tiempo en que caducidad de usuario ( 4 meses default)
o	Comisión por par 
o	Día de corte 
-	Alta de Usuarios 
o	Nuevos admin
o	Moderadores pago
-	Pagos pendientes moderar 
-	Pedidos
o	Predicción mes en curso 
o	Historial meses anteriores pedidos
-	Histórico Cortes
-	Cortes
o	Calculo de comisiones (Imprimir detalle)
o	Orden de nuevo pedido automático
o	Descuento pedido de comisiones 
-	Red
o	Detalle cada Usuario
o	Historial comisiones
o	Red por Usuario
o	Historial del compras


Módulos a futuro (No incluidos) 
-	App (IOS / Andorid)
-	Conectar con Facebook 
-	Firma de contratos digital con PDF SIGN 
-	Factura digital (Timbrado SAT)
-	Premios y metas
-	Pago con Pay-pal – Conekta 

### Desarrollo Jorge Flores



