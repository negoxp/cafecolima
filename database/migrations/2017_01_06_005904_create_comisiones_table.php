<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComisionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comisiones', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
            $table->integer('pedido_id')->unsigned();
            $table->foreign('pedido_id')
                ->references('id')->on('pedidos')
                ->onDelete('cascade');
            $table->decimal('pago_pedido',10,2);
            $table->integer('red_total'); 
            $table->integer('red_pagados'); 
            $table->integer('red_sin_pagar'); 
            $table->decimal('red_comisiones_total',10,2);
            $table->decimal('comision_total',10,2);
            $table->decimal('descuento_proximo_pedidos',10,2);
            $table->decimal('comision_pagar',10,2);
            $table->timestamps(); 
        }); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('comisiones');
    }
}
