<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTPedidosDetalles2Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    
        Schema::create('t_pedido_detalles', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('t_pedido_id')->unsigned();
            $table->foreign('t_pedido_id')
                ->references('id')->on('t_pedidos')
                ->onDelete('cascade');
            $table->integer('producto_id')->unsigned();
            $table->foreign('producto_id')
                ->references('id')->on('productos')
                ->onDelete('cascade');
            $table->integer('cantidad');
            $table->decimal('costo_unitario',10,2);
            $table->decimal('total',10,2);
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('t_pedido_detalles');
    }
}
