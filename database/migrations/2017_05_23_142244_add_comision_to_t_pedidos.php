<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddComisionToTPedidos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('t_pedidos', function (Blueprint $table) {
            $table->decimal('comision',10,2);
            $table->integer('presentador_id')->unsigned();
            $table->string('comprobante');
            $table->timestamp('fecha_pago');
            $table->boolean('pagado');
            $table->timestamp('autorizacion_fecha');
            $table->integer('autorizacion_user_id');
            $table->string('autorizacion_tipo');
            $table->boolean('entregado');
            $table->string('referencia');
            $table->boolean('vigente')->default(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('t_pedidos', function (Blueprint $table) {
            $table->dropColumn('comision');
            $table->dropColumn('presentador_id');
            $table->dropColumn('comprobante');
            $table->dropColumn('fecha_pago');
            $table->dropColumn('pagado');
            $table->dropColumn('autorizacion_fecha');
            $table->dropColumn('autorizacion_user_id');
            $table->dropColumn('autorizacion_tipo');
            $table->dropColumn('entregado');
            $table->dropColumn('referencia');
            $table->dropColumn('vigente');
        });
    }
}
