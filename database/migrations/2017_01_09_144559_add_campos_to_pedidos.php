<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCamposToPedidos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::table('pedidos', function($table) {
            $table->string('comprobante');
            $table->timestamp('fecha_pago');
            $table->boolean('pagado');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::table('pedidos', function(Blueprint $table)
        {
            $table->dropColumn('comprobante');
            $table->dropColumn('fecha_pago');
            $table->dropColumn('pagado');
        });    
   }
}
