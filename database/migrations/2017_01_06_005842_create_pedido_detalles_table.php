<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePedidoDetallesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pedidos_detalles', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('pedido_id')->unsigned();
            $table->foreign('pedido_id')
                ->references('id')->on('pedidos')
                ->onDelete('cascade');
            $table->integer('producto_id')->unsigned();
            $table->foreign('producto_id')
                ->references('id')->on('productos')
                ->onDelete('cascade');
            $table->integer('cantidad');
            $table->decimal('costo_unitario',10,2);
            $table->decimal('total',10,2);
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pedidos_detalles');
    }
}
