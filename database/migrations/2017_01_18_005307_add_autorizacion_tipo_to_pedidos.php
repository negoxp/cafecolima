<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAutorizacionTipoToPedidos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::table('pedidos', function($table) {
            $table->string('autorizacion_tipo');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::table('pedidos', function(Blueprint $table)
        {
            $table->dropColumn('autorizacion_tipo');
        });    
   }
}
