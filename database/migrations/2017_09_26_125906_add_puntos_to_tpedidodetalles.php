<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPuntosToTpedidodetalles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('t_pedido_detalles', function (Blueprint $table) {
            $table->integer('puntos')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('t_pedido_detalles', function (Blueprint $table) {
            $table->dropColumn('puntos');
        });
    }
}
