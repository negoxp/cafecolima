<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddExtraFieldToComisiones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::table('comisiones', function (Blueprint $table) {
            $table->boolean('pagado')->default(false);
            $table->integer('mespedido');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('comisiones', function(Blueprint $table)
        {
            $table->dropColumn('pagado');
            $table->dropColumn('mescorte');
        });
    }
}
