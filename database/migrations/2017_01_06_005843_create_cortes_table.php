<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCortesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cortes', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');

            $table->integer('mescorte');
            $table->integer('red_total');
            $table->integer('red_pagados');
            $table->integer('red_sin_pagar');
            $table->decimal('red_comisiones_total',10,2);
            $table->decimal('ingresos_pedidos',10,2); 

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cortes');
    }
}
