<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAutorizacionToPedidos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
    {
       Schema::table('pedidos', function($table) {
            $table->timestamp('autorizacion_fecha');
            $table->integer('autorizacion_user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::table('pedidos', function(Blueprint $table)
        {
            $table->dropColumn('autorizacion_fecha');
            $table->dropColumn('autorizacion_user_id');
        });    
   }

   
}
