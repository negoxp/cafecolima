<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddExtraFieldToCortes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::table('cortes', function (Blueprint $table) {
            $table->decimal('red_comisiones_retenida',10,2);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cortes', function(Blueprint $table)
        {
            $table->dropColumn('red_comisiones_retenida');
        });
    }
}
