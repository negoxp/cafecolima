<?php

use Illuminate\Database\Seeder;
use App\Producto;

class ProductosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void 
     */
    public function run()
    {

       $productos = [
        	[
        		'nombre' => 'Descuento por comisión',
        		'costo_neto' => '0.00',
        	],
        	[
        		'nombre' => 'Café sabor a Colima ½ kg.',
        		'costo_neto' => '350.00',
        	],
        	[
        		'nombre' => 'Kit inicio - café sabor a Colima 1kg.',
        		'costo_neto' => '500.00',
        	],
            [
                'nombre' => 'Kit inicio - Descuento *** SOLO RED ***',
                'costo_neto' => '-150.00',
            ],
            [
                'nombre' => 'Café sabor a Colima ½ kg. - Descuento *** SOLO RED ***',
                'costo_neto' => '-150.00', 
            ],
        ];

        foreach ($productos as $key => $value) {
        	Producto::create($value);
        }
    }
}
