<?php
namespace App;

use Illuminate\Database\Seeder;
use App\Permission;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permission = [
        	[
        		'name' => 'corte',
        		'display_name' => 'Corte',
        		'description' => 'Can run balance of echa month'
        	],
        	[
        		'name' => 'pagos',
        		'display_name' => 'pagos',
        		'description' => 'Puede verificar los pagos'
        	]
        ];

        foreach ($permission as $key => $value) {
        	Permission::create($value);
        }
    }
}