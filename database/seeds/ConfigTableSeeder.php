<?php

use Illuminate\Database\Seeder;
use App\Config;

class ConfigTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $configs = [
        	[
        		'configuracion' => 'compra_inicial',
        		'valor' => '500',
        	],
        	[
        		'configuracion' => 'minimo_compra',
        		'valor' => '350',
        	],
        	[
        		'configuracion' => 'maximo_nivel_comision',
        		'valor' => '11',
        	],
        	[
        		'configuracion' => 'maximo_hijos',
        		'valor' => '2',
        	],
        	[
        		'configuracion' => 'caducidad_socio',
        		'valor' => '120',
        	],
        	[
        		'configuracion' => 'comision_par',
        		'valor' => '40',
        	],
        	[
        		'configuracion' => 'dia_corte',
        		'valor' => '1',
        	],
        ];

        foreach ($configs as $key => $value) {
        	Config::create($value);
        }
    }
}
