@extends('layouts.admin')

@section('content')

{{ Form::model($pedido, array('route' => array('tpedidos.dupdate', $pedido->id), 'method' => 'PUT',  'files' => 'true')) }}

<div class="row">
<div class="col-md-6 col-sm-6 pull-right">
      <h4>Pedido {{$pedido->id}}</h4>
      <div class="row">
          <div class="col-md-4 col-sm-5 inv-label">Socio </div>
          <div class="col-md-8 col-sm-7">{{$pedido->user_id}}</div>
      </div>

      <div class="row">
          <div class="col-md-4 col-sm-5 inv-label">Total </div>
          <div class="col-md-8 col-sm-7">{{$pedido->total}}</div>
      </div>
      <div class="row">
          <div class="col-md-4 col-sm-5 inv-label">Status</div>
          <div class="col-md-8 col-sm-7">{{$pedido->status}}</div>
      </div>
      <div class="row">
          <div class="col-md-4 col-sm-5 inv-label">Corte </div>
          <div class="col-md-8 col-sm-7">{{$pedido->mespedido}}</div>
      </div>
      <div class="row">
          <div class="col-md-4 col-sm-5 inv-label">Fecha Pago </div>
          <div class="col-md-8 col-sm-7">{{$pedido->fecha_pago}}</div>
      </div>
      <div class="row">
          <div class="col-md-4 col-sm-5 inv-label">Autorización</div>
          <div class="col-md-8 col-sm-7">{{$pedido->autorizadopor()}}</div>
      </div>
      <div class="row">
          <div class="col-md-4 col-sm-5 inv-label">Autorización Fecha</div>
          <div class="col-md-8 col-sm-7">{{$pedido->autorizacion_fecha}}</div>
      </div>
      <div class="row">
          <div class="col-md-4 col-sm-5 inv-label">Autorización Tipo</div>
          <div class="col-md-8 col-sm-7">{{$pedido->autorizacion_tipo}}</div>
      </div>
      <div class="row">
          <div class="col-md-4 col-sm-5 inv-label">Entregado</div>
          <div class="col-md-8 col-sm-7">{{$pedido->entregado}}</div>
      </div>
   
			<hr/>
      <div class="row">
          <div class="col-md-4 col-sm-5 inv-label">Presentador</div>
          <div class="col-md-8 col-sm-7">{{$pedido->presentadorNombre()}} </div>
      </div>
      <div class="row">
          <div class="col-md-4 col-sm-5 inv-label">Comisión</div>
          <div class="col-md-8 col-sm-7">$ {{$pedido->comision}}</div>
      </div>


  </div>
<div class="col-md-6 col-sm-6">

    <div class="form-group">
        {{ Form::label('user_id', 'Usuario ID') }} 
        {{ Form::text('user_id', $pedido->user_id, array('class' => 'form-control' )) }}
    </div>
</div>
</div>   


    {{ Form::submit('Editar', array('class' => 'btn btn-primary')) }}

{{ Form::close() }} 

@endsection