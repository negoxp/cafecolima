@extends('layouts.admin')

@section('content')

{{ Form::model($pedido, array('route' => array('tpedidos.update', $pedido->id), 'method' => 'PUT',  'files' => 'true')) }}

    <div class="form-group">
        {{ Form::label('referencia', 'Referencia') }} 
        {{ Form::text('referencia', $pedido->user_id."-".$pedido->id, array('class' => 'form-control' )) }}
    </div>
    <div class="form-group">
        {{ Form::label('comprobante', 'Comprobante') }}
        {{ Form::file('comprobante', null, array('class' => 'form-control', 'required' => 'required')) }}
    </div>


    {{ Form::submit('Subir', array('class' => 'btn btn-primary')) }}

{{ Form::close() }}

@endsection