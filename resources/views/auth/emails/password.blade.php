
<table class="m_-7586449795964574897content" align="center" cellpadding="0" cellspacing="0" border="0" style="width:600px">
<tbody><tr>
    <td>
        
<table cellpadding="0" cellspacing="0" border="0" style="border-collapse:collapse;min-width:100%;width:100%">
    <tbody>
    <tr>
        <td style="background-color:#1fb5ad;height:32px;text-align:center;padding:29px 0">
            <a style="display:inline-block" href="http://www.saboracolima.com">
                <img src="http://saboracolima.com/img/logotop.png" alt="CafeLogo" width="183" height="32" style="border-width:0;display:block" border="0" class="CToWUd">
            </a>
        </td>
    </tr>
</tbody></table>
    </td>
</tr>




<tr>
    <td>
        <h1 class="m_-7586449795964574897hello" style="text-align:center;font-family:'Source Sans Pro',Arial,sans-serif;font-weight:200;font-size:70pt;line-height:52pt;margin:40px 70px 5px 70px;color:#1fb5ad">
            Felicidades!
        </h1>
    </td>
</tr>
<tr>
    <td>
        <h2 class="m_-7586449795964574897sub-header-text" style="margin:10px 50px 30px 50px;font-family:Arial,sans-serif;font-size:16pt;line-height:24pt;text-align:center;color:#3b404d">
            <span class="il">Te han invitado</span> al NEGOCIO DEL CAFÉ SABOR A COLIMA.
        </h2>
    </td>
</tr>
<tr>
    <td style="text-align:center">
        <img class="m_-7586449795964574897main-image CToWUd a6T" src="http://saboracolima.com/img/bienvenido.png" style="margin:15px 0" tabindex="0"><div class="a6S" dir="ltr" style="opacity: 0.01; left: 1144.5px; top: 470px;"><div id=":3l6" class="T-I J-J5-Ji aQv T-I-ax7 L3 a5q" title="Descargar" role="button" tabindex="0" aria-label="Descargar el archivo adjunto " data-tooltip-class="a1V"><div class="aSK J-J5-Ji aYr"></div></div></div>
    </td>
</tr>



<tr class="m_-7586449795964574897login" style="background-color:#ebeced">
    <td>
        <p class="m_-7586449795964574897content" style="margin:20px 43px 10px 43px;font-family:Arial,sans-serif;font-size:14pt;line-height:20pt;text-align:center;color:#767982">

            <a href="#" style="color:#767982;font-weight:bold;text-decoration:none" target="_blank">Inicia en tu cuenta </a> para:
        </p>
    </td>
</tr>


<tr style="background-color:#ebeced">
    <td style="padding:0 20px 20px 20px">
        <table style="width:100%;border-collapse:collapse">
            
            <tbody><tr style="height:100px">
                <td style="width:13%;min-width:50px;vertical-align:central;text-align:center" bgcolor="#21C4D9">
                    <img src="https://ci5.googleusercontent.com/proxy/khDrUlQGx-PsqEi4dXiGOf67lY_wnkBEMFk0sxKW2qn18KPj-DUVgANyp3zLeHJYY-nXQS0pu1cCmlDs25ICa9BZqytv3nFjBFPWQsuY7lWy8cFqa7v_aoCU0-Wsi37gmAoFkw0=s0-d-e1-ft#http://www.skyscanner.net/images/email/welcomeredesign/1_price-alerts_33x39.png" alt="Configura alertas de precios" width="33px" height="39px" class="CToWUd">
                </td>

                <td style="font-size:14pt;font-family:Arial,sans-serif;background-color:#fff;vertical-align:top">
                    <a href="{{ $link = url('password/reset', $token).'?email='.urlencode($user->getEmailForPasswordReset()) }}" style="text-decoration:none" target="_blank" >
                        <p style="color:#000000;text-decoration:none;font-weight:bold;margin:5px 15px 0 15px">
                            Configura y crea tu contraseña
                        </p>
                    </a>
                    <p style="font-size:14pt;margin:10px 10px 10px 15px;color:#1fb5ad">
                        Lo primero que tienes que hacer, es entrar y configurar tu cuenta. 
                        <a href="{{ $link }}">
                        haz click aquí
                        </a>
                        <!--  {{ $link }} -->
                    </p>
                </td>
            </tr>
            
            <tr class="m_-7586449795964574897blank-row-in-table">
                <td style="width:12%;min-width:50px;vertical-align:central;text-align:center;background-color:#ebeced"></td>
                <td style="font-size:14pt;font-family:Arial,sans-serif;padding:15px 0 15px;background-color:#ebeced"></td>
            </tr>
                      
        </tbody></table>
        </td>
</tr>



<tr class="m_-7586449795964574897about" style="background-color:#1fb5ad">
    <td>
        <p class="m_-7586449795964574897content" style="margin:30px 0px 10px 0px;font-family:Arial,sans-serif;font-size:16pt;line-height:24pt;text-align:center;color:#fff">
            Felicidades! ya eres parte de <a href="http://www.saboracolima.com" style="font-weight:bold;color:#fff;text-decoration:none" target="_blank" >Café Sabor a Colima?</a>
        </p>
        <p class="m_-7586449795964574897content" style="margin:5px 50px 30px 50px;font-family:Arial,sans-serif;font-size:14pt;line-height:20pt;text-align:center;color:#006982">
        Hoy te damos la bienvenida al primer día de tu nueva vida. A partir de este momento, tienes frente a ti la oportunidad de unirte a este equipo de líderes y triunfadores, que como tú, han tomado la decisión de proyectar un mejor futuro para si mismos y sus familias.

Café Sabor a Colima ha desarrollado un modelo de negocio exitoso que te permitirá consolidar tus metas a través de un sistema de compensación justo y equitativo, en el cual todos ganan con un producto tan apetecido en los hogares como lo es el café de nuestra tierra. Para garantizar el crecimiento de tu unidad de negocio, contamos con diferentes herramientas de apoyo, entre las cuales destacamos la Escuela de Formación de Líderes Café Sabor a Colima, donde aprenderás diversas habilidades profesionales que te permitirán administrar y dirigir de manera eficiente, el crecimiento exponencial de tu red.

Creemos firmemente en la familia mexicana, su fuerza y capacidad de trabajo, por ello queremos que tomes el control de tu futuro y vivas con nosotros el primero, de muchos días llenos de posibilidades en tu nueva vida . 

¿Estas preparado para hacer de tus metas, una realidad? 
Si es así, adelante. Ya cuentas con nosotros.
        </p>
    </td>
</tr>



<tr class="m_-7586449795964574897apps" style="background-color:#3b404d">
    <td>
        <p class="m_-7586449795964574897content" style="margin:30px 50px 10px 50px;font-family:Arial,sans-serif;font-size:16pt;line-height:24pt;text-align:center;color:#fff">
            Inicia tu negocio, <a href="#" style="color:#fff;text-decoration:none;font-weight:bold" target="_blank" > y haz crecer tu RED</a>
        </p>

        <p class="m_-7586449795964574897content" style="margin:5px 50px 10px 50px;font-family:Arial,sans-serif;font-size:14pt;line-height:20pt;text-align:center;color:#1fb5ad">
            Emprendedores, Inovación y Negocio.
        </p>
    </td>
</tr>

<tr>
    <td>
        
<table cellpadding="0" cellspacing="0" border="0" style="background-color:#ebeced;border-collapse:collapse;width:100%">

    <tbody><tr>
        <td style="padding-top:18px">
        </td>
    </tr>

    
    <tr>
        <td style="font-family:Arial,sans-serif;font-size:9pt;line-height:15pt;text-align:center;padding:0 8% 0 8%;color:#b0b3b8;text-align:center;text-decoration:none">
            <span class="m_-7586449795964574897appleLinksGrey">
            Productos Sabor a COlima, S.P.R. de R.L.
            Calle Sin Nombre, Sin Numero, EL nuevo Naranjal.
            Villa de Álvarez, Colima. C.P. 28950
            Teléfonos 01(312)16 14845 
            </span>
        </td>
    </tr>

    
    <tr>
        <td>
            <table cellpadding="0" cellspacing="0" border="0" style="border-collapse:collapse;width:100%">
                <tbody><tr>
                    <td style="padding:0 8% 15px 8%;line-height:15pt;font-family:Arial,sans-serif;font-size:9pt;text-align:center">
                        <a href="http://www.saboracolima.com" title="Política de privacidad" style="color:#767982;text-decoration:none" target="_blank" >
                            Política de privacidad
                        </a> |
                        <a href="http://www.saboracolima.com" title="Ponte en contacto con nosotros" style="color:#767982;text-decoration:none" target="_blank" d>
                            Ponte en contacto con nosotros
                        </a> |
                        <a href="https://www.facebook.com/Caf%C3%A9-Sabor-a-Colima-239613873104359/?fref=ts" title="www.espanol.skyscanner.com" style="color:#767982;text-decoration:none" target="_blank" >
                            Facebook
                        </a>
                    </td>
                </tr>
            </tbody></table>
        </td>
    </tr>
</tbody></table>
    </td>
</tr>
</tbody></table>