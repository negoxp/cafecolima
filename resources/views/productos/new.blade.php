@extends('layouts.admin')

@section('content')

{{ Form::open(array('url' => 'productos')) }}

    <div class="form-group">
        {{ Form::label('nombre', 'Nombre') }} 
        {{ Form::text('nombre', null, array('class' => 'form-control', 'required' => 'required' )) }}
    </div>
    <div class="form-group">
        {{ Form::label('costo_neto', 'Costo Neto') }}
        {{ Form::text('costo_neto', null, array('class' => 'form-control', 'required' => 'required')) }}
    </div>
    <div class="form-group">
        {{ Form::label('puntos', 'Puntos') }}
        {{ Form::text('puntos', null, array('class' => 'form-control', 'required' => 'required')) }}
    </div>
    <div class="form-group">
        {{ Form::label('foto', 'Foto') }}
        {{ Form::file('foto', null, array('class' => 'form-control')) }}
    </div>
    <div class="form-group">
        
        {{ Form::checkbox('is_in_tienda', 1, 1, array('class' => 'form-control')) }}
        {{ Form::label('is_in_tienda', 'Que se muestre en tienda? ') }}
    </div>


    {{ Form::submit('Subir', array('class' => 'btn btn-primary')) }}

{{ Form::close() }}

@endsection