@extends('layouts.admin')

@section('content')

<h2>Productos 
<div class="pull-right"><a href="{{url('productos/create')}}" class="btn btn-success">Agregar <span class="icon  icon-plus"></span></a></div>
</h2>
<div class="row">
    <div class="col-sm-12">
       <section id="flip-scroll" class="panel">
        
        <table class="table table-invoice table-responsive">
            <thead class="cf">
            <tr>
                <th>ID Producto</th>
                <th>Nombre</th>
                <th class="text-center">Costo</th>
                <th class="text-center">Tienda</th>
                <th class="text-center">Foto</th>
                <th class="text-center">Puntos</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
			@foreach($productos as $p)

            <tr>
                <td data-title="Id">{{$p->id}}</td>
                <td>{{$p->nombre}}</td>
                <td>{{number_format($p->costo_neto,2)}}</td>
                <td>{{$p->is_in_tienda ? "SI" : "NO"}}</td>
                <td class="text-center">
                    @if(file_exists(public_path().$p->foto) and $p->foto)
                        <img src="{{ asset('') }}{{$p->foto}}" alt="" width="50">
                    @endif
                </td>
                <td>{{$p->puntos}}</td>
                <td class="text-center">
                    <a href="{{ url('/productos/'.$p->id.'/edit') }}" class="btn btn-warning btn-xs"><i class="fa fa-pencil"></i> Editar</a>                	
                </td>
            </tr>

            @endforeach

            </tbody>
        </table>

       </section>
    </div>
</div>







@endsection