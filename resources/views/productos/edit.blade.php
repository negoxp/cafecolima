@extends('layouts.admin')

@section('content')

{{ Form::model($producto, array('route' => array('productos.update', $producto->id), 'method' => 'PUT',  'files' => 'true')) }}

    <div class="form-group">
        {{ Form::label('nombre', 'Nombre') }} 
        {{ Form::text('nombre', $producto->nombre, array('class' => 'form-control', 'required' => 'required' )) }}
    </div>
    <div class="form-group">
        {{ Form::label('costo_neto', 'Costo Neto') }}
        {{ Form::text('costo_neto', $producto->costo_neto, array('class' => 'form-control', 'required' => 'required')) }}
    </div>
    <div class="form-group">
        {{ Form::label('puntos', 'Puntos') }}
        {{ Form::text('puntos', $producto->puntos, array('class' => 'form-control', 'required' => 'required')) }}
    </div>
    <div class="form-group">
        {{ Form::label('foto', 'Foto') }}
        {{ Form::file('foto', null, array('class' => 'form-control')) }}
    </div>
    <div class="form-group">
        
        {{ Form::checkbox('is_in_tienda', 1, $producto->is_in_tienda, array('class' => 'form-control')) }}
        {{ Form::label('is_in_tienda', 'Que se muestre en tienda? ') }}
    </div>


    {{ Form::submit('Subir', array('class' => 'btn btn-primary')) }}

{{ Form::close() }}

@endsection