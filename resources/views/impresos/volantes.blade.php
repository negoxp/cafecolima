
@extends('layouts.admin')

@section('content')

<!-- page start-->

        <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <header class="panel-heading">
                        Volantes
                        <span class="tools pull-right">
                            <a href="javascript:;" class="fa fa-chevron-down"></a>
                         </span>
                    </header>
                    <div class="panel-body">

                        <div id="gallery" class="media-gal">
                            <div class="images item " >
                                <a href="#myModal" data-toggle="modal">
                                <img src="images/gallery/image1.jpg" alt="" />
                                    </a>
                                <p>img01.jpg </p>
                            </div>

                            <div class=" audio item " >
                                <a href="#myModal" data-toggle="modal">
                                <img src="images/gallery/image2.jpg" alt="" />
                                    </a>
                                <p>img02.jpg </p>
                            </div>

                            <div class=" video item " >
                                <a href="#myModal" data-toggle="modal">
                                <img src="images/gallery/image3.jpg" alt="" />
                                    </a>
                                <p>img03.jpg </p>
                            </div>

                            <div class=" images audio item " >
                                <a href="#myModal" data-toggle="modal">
                                <img src="images/gallery/image4.jpg" alt="" />
                                    </a>
                                <p>img04.jpg </p>
                            </div>

                            <div class=" images documents item " >
                                <a href="#myModal" data-toggle="modal">
                                <img src="images/gallery/image5.jpg" alt="" />
                                    </a>
                                <p>img05.jpg </p>
                            </div>

                            <div class=" audio item " >
                                <a href="#myModal" data-toggle="modal">
                                <img src="images/gallery/image1.jpg" alt="" />
                                    </a>
                                <p>img01.jpg </p>
                            </div>

                            <div class=" documents item " >
                                <a href="#myModal" data-toggle="modal">
                                <img src="images/gallery/image2.jpg" alt="" />
                                </a>
                                <p>img02.jpg </p>
                            </div>
                            <div class=" video item " >
                                <a href="#myModal" data-toggle="modal">
                                <img src="images/gallery/image3.jpg" alt="" />
                                </a>
                                <p>img03.jpg </p>
                            </div>

                            <div class=" images item " >
                                <a href="#myModal" data-toggle="modal">
                                <img src="images/gallery/image4.jpg" alt="" />
                                </a>
                                <p>img04.jpg </p>
                            </div>

                            <div class=" documents item " >
                                <a href="#myModal" data-toggle="modal">
                                    <img src="images/gallery/image5.jpg" alt="" />
                                </a>
                                <p>img05.jpg </p>
                            </div>

                            <div class=" video item " >
                                <a href="#myModal" data-toggle="modal">
                                    <img src="images/gallery/image1.jpg" alt="" />
                                </a>
                                <p>img01.jpg </p>
                            </div>

                            <div class=" audio images item " >
                                <a href="#myModal" data-toggle="modal">
                                    <img src="images/gallery/image2.jpg" alt="" />
                                </a>
                                <p>img02.jpg </p>
                            </div>

                        </div>


                    </div>
                </section>
            </div>
        </div>
        <!-- page end-->

@endsection
