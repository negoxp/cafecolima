@extends('layouts.app')
@section('content')


<div class="container">


    <div class="row">
        <div class="col-md-10 col-md-offset-1">


        	<form method="POST" action="{{ url('/invite') }}" accept-charset="UTF-8">

                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                  <div class="form-group">
                      <label>Nombre(s)</label>
                      <input type="text" class="form-control" name="nombres" id="nombres" placeholder="Nombre(s)" required>
                  </div>
                  <div class="form-group">
                      <label>Apellidos</label>
                      <input type="text" class="form-control" name="apellidos" id="apellidos" placeholder="Apellidos" required>
                  </div>
                  <div class="form-group">
                      <label>Sexo</label>
                      <select class="form-control" name="genero" id="genero">
                        <option value="">Selecciona una opcion</option>
                        <option value="h">Hombre</option>
                        <option value="m">Mujer</option>
                      </select>
                  </div>
                  <div class="form-group">
                      <label>Email</label>
                      <input type="email" class="form-control" name="email" id="email" placeholder="Email" required>
                  </div>

                  <div class="form-group">
                      <label>Celular</label>
                      <input type="celular" class="form-control phone" name="celular" id="celular" placeholder="Celular" required>
                  </div>

                  <div class="form-group">
                      <label>¿Cómo te enteraste?</label>
                      <textarea class="form-control" name="enteraste" id="enteraste"></textarea>
                  </div>

                  <br/>
                  <div class="row" id="defaultimg">
                    
                    @for ($i = 1; $i <= 6; $i++)
                      <div class="col-xs-2 radio_button_img defaulh">
                        <input type="radio" name="foto_default" value="{{ $i }}" id="fd{{ $i }}" {{$i==1 ? "checked='checked'" : "" }} />
                        <label for="fd{{ $i }}"><img src="{{ asset('') }}vendor/theme/images/avatars/{{ $i }}.png" class="img-responsive"/></label>
                      </div>
                    @endfor

                    @for ($i = 7; $i <= 12; $i++)
                      <div class="col-xs-2 radio_button_img defaulm">
                        <input type="radio" name="foto_default" value="{{ $i }}" id="fd{{ $i }}" {{$i==1 ? "checked='checked'" : "" }} />
                        <label for="fd{{ $i }}"><img src="{{ asset('') }}vendor/theme/images/avatars/{{ $i }}.png" class="img-responsive"/></label>
                      </div>
                    @endfor
 
                  </div>

                  <button type="submit" class="btn btn-info" style="width:100%">Solicitar Unirme a la Red</button>
              </form>

        </div>
    </div>
</div>

@endsection