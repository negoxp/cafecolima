@extends('layouts.admin')

@section('content')

<h2>Mis Directos</h2>
<div class="row">
    <div class="col-sm-12">
       <section id="flip-scroll" class="panel">
        
        <table class="table table-invoice table-responsive">
            <thead class="cf">
            <tr>
                <th>Socio</th>
                <th>Status</th>
                <th>Mes Actual Pagado</th>
                <th>Nombre</th>
                <th>Apellidos</th>
                <th>Email</th>
                <th>Telefono</th>
                <th>Celular</th>
                <th>Fecha Inscripción</th>  
            </tr>
            </thead>
            <tbody>
              @foreach($directos as $u)
            <tr>
                <td>{{$u->id}}</td>
                <td>{{$u->status}}</td>
                <td>{{$u->pedido_mes_pagado==1 ? "Si" :  "No"}}</td>
                <td>{{$u->nombre}}</td>
                <td>{{$u->apellidos}}</td>
                <td>{{$u->email}}</td>
                <td>{{$u->telefono}}</td>
                <td>{{$u->celular}}</td>
                <td>{{$u->created_at}}</td>
                
            </tr>
            @endforeach

            </tbody>
        </table>
       </section>
    </div>
</div>

@endsection