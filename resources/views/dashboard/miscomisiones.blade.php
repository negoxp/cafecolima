@extends('layouts.admin')

@section('content')

<h2>Mis Comisiones</h2>
<div class="row">
    <div class="col-sm-12">
       <section id="flip-scroll" class="panel">
        
        <table class="table table-invoice table-responsive">
            <thead class="cf">
            <tr>
                <th>ID</th>
                <th>Mes</th>
                <th>Pago pedido</th>
                <th class="text-center">Red Total</th>
                <th class="text-center">Red Pagados</th>
                <th class="text-center">Comision</th>
                <th class="text-center">Descuento Próximo Pedido</th>
                <th class="text-center">Bono</th>
                <th class="text-center">C. Tienda</th>
                <th class="text-center">Comisión a pagar</th>
            </tr>
            </thead>
            <tbody>
			@foreach($comisiones as $c)
            <tr>
                <td>{{$c->id}}</td>
                <td>{{$c->mespedido}}</td>
                <td>{{$c->pago_pedido}}</td>
                <td class="text-center">{{$c->red_total}}</td>
                <td class="text-center">{{$c->red_pagados}}</td>
                <td class="text-center">{{$c->comision_total}}</td>
                <td class="text-center">{{$c->descuento_proximo_pedidos}}</td>
                <td class="text-center">{{$c->bono}}</td>
                <td class="text-center">{{$c->comision_tienda}}</td>
                <td class="text-center">{{$c->comision_pagar}}</td>
            </tr>
            @endforeach

            </tbody>
        </table>

       </section>
    </div>
</div>

@endsection
