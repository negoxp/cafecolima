@extends('layouts.admin')

@section('content')
<?php
use App\PedidosDetalle;
?>

@if(\Auth::user()->hasRole('admin')) 
    <a href="<?php echo $preference['response']['sandbox_init_point']; ?>" class="btn btn-success">Pagar mercado Pago</a>
@endif
<h2>Historial de Pedidos RED </h2>
<div class="row">
    <div class="col-sm-12">
       <section id="flip-scroll" class="panel">
        
        <table class="table table-invoice table-responsive">
            <thead class="cf">
            <tr>
                <th># Pedido</th>
                <th>Mes</th>
                <th>Des.</th>
                <th class="text-center">Total</th>
                <th class="text-center">Estado</th>
                <th class="text-center">Pago</th>
            </tr>
            </thead>
            <tbody>
			@foreach($pedidos as $p)

            <tr>
                <td data-title="Id">{{$p->id}}</td>
                <td>{{$p->mespedido}}</td>
                <td>
                    @foreach ($p->pedidosDetalles as $pd)
                        <h4>{{ $pd->producto->nombre }} - {{ $pd->total }}</h4>
                    @endforeach
                </td>
                <td class="text-center">{{$p->total>0 ? number_format((float)$p->total,2) : number_format(0,2)}}</td>
                <td class="text-center">
                	   <span class="label label-{{$p->getStatusCss()}} label-mini">{{$p->status}}</span>
                </td>
                <td class="text-center">
                	@if ($p->mespedido==$mescorte)
                        @if (!$p->pagado)
                        	<a href="{{ url('/pedidos/'.$p->id.'/edit') }}" class="btn btn-info btn-xs"><i class="fa  fa-cloud-upload"></i> Subir Comprobante</a> 
                        @endif  
                        @if ($p->status=="validacion" and $p->comprobante) 
                        	<a downpath="{{ url('download/'.$p->id) }}" imgpath="{{ URL::to('/').$p->comprobante }}" href="#" class="btn btn-success btn-xs pull-right showImg" style="margin-right:3px;"><i class="fa  fa-download"></i> Descargar</a> 
                        @endif 
                        @if ($p->status=="aceptado" and $p->comprobante)
                        	&nbsp;<a downpath="{{ url('download/'.$p->id) }}" imgpath="{{ URL::to('/').$p->comprobante }}" href="#" class="btn btn-success btn-xs pull-right showImg" style="margin-right:3px;"><i class="fa  fa-download"></i> Descargar</a> &nbsp;
                        @endif 
                        &nbsp;<a href="{{ url('/pedidos/invoice/'.$p->id) }}" class="btn btn-warning btn-xs pull-right" style="margin-right:3px;"><i class="fa  fa-barcode"></i> Imprimir </a>&nbsp;  
                    @endif                      	
                </td>
            </tr>

            @endforeach

            </tbody>
        </table>

       </section>
    </div>
</div>

<h2>Tu Carrito </h2>
<div class="row">
    <div class="col-sm-12">
       <section id="flip-scroll" class="panel">
        
        <table class="table table-invoice table-responsive">
            <thead class="cf">
            <tr>
                <th># Pedido</th>
                <th>Mes</th>
                <th>Des.</th>
                <th class="text-center">Total</th>
                <th class="text-center">Estado</th>
                <th class="text-center">Puntos</th>
                <th class="text-center">Pago</th>
            </tr>
            </thead>
            <tbody>
            @foreach($tpedidos as $p)

            <tr>
                <td data-title="Id">{{$p->id}}</td>
                <td>{{$p->mespedido}}</td>
                <td>
                    @foreach ($p->pedidosDetalles as $pd)
                        <h4>{{ $pd->producto->nombre }} - {{ $pd->total }}</h4>
                    @endforeach
                </td>
                <td class="text-center">{{$p->total>0 ? number_format((float)$p->total,2) : number_format(0,2)}}</td>
                <td class="text-center">
                       <span class="label label-{{$p->getStatusCss()}} label-mini">{{$p->status}}</span>
                </td>
                <td class="text-center">
                       <span class="label label-{{$p->getStatusCss()}} label-mini">{{$p->puntos}}</span>
                </td>
                <td class="text-center">
                    @if ($p->mespedido==$mescorte)
                        @if (!$p->pagado)
                            <a href="{{ url('/tpedidos/'.$p->id.'/edit') }}" class="btn btn-info btn-xs"><i class="fa  fa-cloud-upload"></i> Subir Comprobante</a> 
                        @endif  
                        @if ($p->status=="validacion" and $p->comprobante) 
                            <a downpath="{{ url('tdownload/'.$p->id) }}" imgpath="{{ URL::to('/').$p->comprobante }}" href="#" class="btn btn-success btn-xs pull-right showImg" style="margin-right:3px;"><i class="fa  fa-download"></i> Descargar</a> 
                        @endif 
                        @if ($p->status=="aceptado" and $p->comprobante)
                            &nbsp;<a downpath="{{ url('tdownload/'.$p->id) }}" imgpath="{{ URL::to('/').$p->comprobante }}" href="#" class="btn btn-success btn-xs pull-right showImg" style="margin-right:3px;"><i class="fa  fa-download"></i> Descargar</a> &nbsp;
                        @endif 
                        &nbsp;<a href="{{ url('/tpedidos/invoice/'.$p->id) }}" class="btn btn-warning btn-xs pull-right" style="margin-right:3px;"><i class="fa  fa-barcode"></i> Imprimir </a>&nbsp;  
                    @endif                          
                </td>
            </tr>

            @endforeach

            </tbody>
        </table>

       </section>
    </div>
</div>

@endsection