@extends('layouts.admin')

@section('content')

<div class="row">
    <div class="col-md-12">
        <section class="panel">
            <div class="panel-body profile-information">
               <div class="col-md-2">
                   <div class="profile-pic text-center {{$me->pedido_mes_pagado ? 'tree-pic-paid' : $me->pago_ultimos_status_color() }}"> 
                       <img src="{{$me->getPhoto()}}" alt=""/>
                       <?php
                         $p=$me->get_ultimo_pedido();
                         ?>
                         
                       @if(\Auth::user()->hasRole('admin'))
                         
                         <div class="row">
                            <div class="col-md-12">
                              Pagado
                              <input type="checkbox" pedidoId="{{$p->id}}" class="switch-small pagadoclass"  {{$p->pagado ? 'checked=""' : ''}} data-on="success" data-off="danger" data-on-label="SI" data-off-label="NO">
                            </div>
                            <div class="col-md-12">
                              Entregado
                              <input type="checkbox" pedidoId="{{$p->id}}" class="switch-small entregadoclass"  {{$p->entregado ? 'checked=""' : ''}} data-on="success" data-off="danger" data-on-label="SI" data-off-label="NO">
                            </div>
                            <div class="col-md-12">
                              Descuento
                              <input type="checkbox" pedidoId="{{$p->id}}" class="switch-small descuentoclass"  {{$p->hasDescuento() ? 'checked=""' : ''}} data-on="success" data-off="danger" data-on-label="SI" data-off-label="NO">
                            </div> 
                         </div>
                       @endif 
                   </div>
                   
               </div>
               <div class="col-md-7">
                   <div class="profile-desk">
                       <h1><span class="mini-stat-icon tar">{{$me->getHijos()}}</span>{{$me->nombre}} <small>{{$me->apellidos}}</small></h1>
                       
                       <br/>
                        <p style="margin-left:0px; padding-bottom:0px;">
                        <span class="text-muted" style="padding-bottom: 2px;">  
                          # Socio <span class="label label-primary">{{ $me->id }}</span> 
                          <br/>
                          Red <span class="label label-primary">{{ $me->getHijos() }}</span>  
                          Nivel <span class="label label-primary">{{ $me->getNivel() }}</span>
                          Pagados <span class="label label-primary">{{ $me->getHijosPagados() }}</span>
                          % <span class="label label-success" style="padding-bottom: 4px;">{{$me->getHijosPagadosPercent()}}%</span>
                          <br/>
                          Invitaciones Pendientes <span class="label label-info">{{ $me->getInvitacionesPendientes() }}/30</span>
                          @if (!$me->pedido_mes_pagado)
                          <br/>
                          <span class="label label-danger" style="width: 100%;"> Fecha límite de pago: <strong>{{$me->ultimo_dia_pago()}}</strong></span>
                          @endif
                        </span>

                        <!--
                         <br/>
                         <span class="label label-primary" style="padding-bottom: 4px;"> 4 </span> Usuarios Nuevos   
                         --> 
                      </p>

                        <div class="row" >
                            <div class="col-md-12">
                  @if ($me->pedido_mes_pagado)
                   <div class="alert alert-success ">
                      <span class="alert-icon"><i class="fa fa-thumbs-up"></i></span>
                      <div class="notification-info">
                          <ul class="clearfix notification-meta">
                              <li class="pull-left notification-sender">El pago del mes ha sido aceptado</li>
                              <!--<li class="pull-right notification-time">1 min ago</li>-->
                          </ul>
                          <p style="padding-bottom:0px;">
                              <a href="{{ url('/pedidos/invoice/'.$me->ultimo_pedido_id)}}" target="_blank"> <i class="fa fa-print" aria-hidden="true"></i> Imprimir comprobante</a>
                          </p>
                      </div>
                   </div>
                   @else ($me->pedido_mes_pagado)
                   <div class="alert alert-warning">
                      <span class="alert-icon"><i class="fa fa fa-bell-o"></i></span>
                      <div class="notification-info">
                          <ul class="clearfix notification-meta">
                              <li class="pull-left notification-sender">Aún no hemos procesado tu pago del mes</li>
                          </ul>
                          <p style="padding-bottom:0px;">
                            <i class="fa fa-shopping-cart"></i> Ver <a href="{{ url('/home/pedidos') }}"><span>Historial Pedidos</span></a>
                          </p>
                      </div>
                   </div>
                   @endif
                                
                            </div>
    
                        </div>

                   </div>
                   <div class="col-md-12 col-xs-12 invoice-block pull-right">
                                <ul class="unstyled amounts db_profile">
                                      @if ($me->getSociosInvitarFaltantes()>=3)
                                        <li style="font-size:10px; font-weight:bold;">
                                            AL SEGUIR INVITANDO SOCIOS AYUDAS A TU GENTE Y TE DARÁ BONOS LLEGADO EL MOMENTO
                                        </li>
                                      @else
                                        <li class="grand-total">
                                          <span class="pull-left">
                                          SOCIOS POR INVITAR
                                          <br>
                                          <small>PARA SEGUIR CRECIENDO TODOS</small>
                                          </span>
                                          <span class="pull-righ">
                                            <span class="mini-stat-icon socios_invitar">
                                              {{2-$me->getSociosInvitarFaltantes()}}
                                            </span>
                                          </span>
                                        </li>
                                      @endif
                                </ul>
                            </div>
               </div>
               <div class="col-md-3">
                   <div class="profile-statistics">

                        <div class="row">
                            
                            
                            <div class="col-md-12 col-xs-12 invoice-block pull-right">
                                <ul class="unstyled amounts db_profile">
                                    <li>Puntos: {{$me->getPuntosTienda()}}</li>
                                </ul>
                            </div>
                            <div class="col-md-12 inv-label">
                                <h4>TU PEDIDO <strong>DICIEMBRE</strong></h4>
                            </div>
                            <div class="col-md-12 col-xs-12 invoice-block pull-right">
                                <ul class="unstyled amounts db_profile">
                                    @if ($me->pedido_mes_pagado)
                                      
                                      <li class="grand-total">
                                        <a href="{{ url('/pedidos/invoice/'.$pedido->id) }}" target="_blank">
                                        <i class="fa fa-print" aria-hidden="true"></i>
                                      </a>
                                        PAGADO
                                      </li>
                                    @else
                                    <li class="grand-total-pedido-actual">
                                      <a href="{{ url('/pedidos/invoice/'.$pedido->id) }}" target="_blank">
                                        <i class="fa fa-print" aria-hidden="true"></i>
                                      </a>
                                      TOTAL A PAGAR: $ {{$me->cantidad_ultimo_pedido()}}
                                    </li>
                                    @endif
                                </ul>
                            </div>

                            <div class="col-md-12 inv-label">
                                <h4>PRÓXIMA COMISIÓN <strong>ENERO</strong></h4>
                            </div>
                            <div class="col-md-12 col-xs-12 invoice-block pull-right">
                                <ul class="unstyled amounts db_profile">
                                    <li>Comisión Tienda: ${{$me->getComisionTienda()}}</li>
                                    <li>Red: ${{$me->getComision(true)}}</li>
                                    <li>Próximo Pedido: $350.00 </li>
                                    <?php 

                                    ?>
                                    @if ($me->solo_red or $p->hasDescuento())
                                      <li>Descuento solo red: - $75.00 </li>
                                    @endif
                                    <li>Sub- Total: ${{$me->getComisionTotal(350,true)}}</li>
                                    @if ($me->pedido_mes_pagado)
                                      <li class="grand-total">Gran Total : ${{$me->getComisionTotal()}}</li>
                                    @else
                                      <li class="grand-total-sp">SIN DERECHO A COMISIÓN</li>
                                    @endif
                                </ul>
                            </div>
                        </div>
                        <!--
                       <ul>
                           <li>
                               <a href="#">
                                   <i class="fa fa-facebook"></i>
                               </a>
                           </li>
                           <li class="active">
                               <a href="#">
                                   <i class="fa fa-twitter"></i>
                               </a>
                           </li>
                           <li>
                               <a href="#">
                                   <i class="fa fa-google-plus"></i>
                               </a>
                           </li>
                       </ul>
                     -->
                   </div>
               </div>
            </div>
        </section> 
    </div>
    <div class="col-md-12">
      <?php /*
      <table border=0 cellpadding=1 cellspacing=1><tr><td valign=top>
      <iframe id="srchform" src="{{ url('/home/srchform') }}" width=220 height=34 border=0 frameborder=0 scrolling=no>
      </iframe>
      </td><td valign=top width=99 style="padding-top:3px;"><g:plusone size="medium"></g:plusone></td>
      </tr></table>

      */ ?>

       <section class="panel DocumentList" style="max-height: 100%; overflow: auto">
        <div id="arbol">
        <div class="tree"> 
          <ul>
            {!! $me->getPrintPerson() !!} 
          </ul>
        </div>
      </div>



<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
  <div class="modal-dialog">
      <div class="modal-content">
          <div class="modal-header">
              <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
              <h4 class="modal-title">Invitación a ser Socio</h4>
          </div>
          <div class="modal-body">

              <form method="POST" action="{{ url('/home/invite') }}" accept-charset="UTF-8">

                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input name="parentId" id="parentId" type="hidden" value="">
                <input name="hijo_posicion" id="hijo_posicion" type="hidden" value="">
                <input name="userid" id="userid" type="hidden" value="">


                  <div class="form-group">
                      <label>Nombre(s)</label>
                      <input type="text" class="form-control" name="nombres" id="nombres" placeholder="Nombre(s)" required>
                  </div>
                  <div class="form-group">
                      <label>Apellidos</label>
                      <input type="text" class="form-control" name="apellidos" id="apellidos" placeholder="Apellidos" required>
                  </div>
                  <div class="form-group">
                      <label>Sexo</label>
                      <select class="form-control" name="genero" id="genero">
                        <option value="">Selecciona una opcion</option>
                        <option value="h">Hombre</option>
                        <option value="m">Mujer</option>
                      </select>
                  </div>
                  <div class="form-group">
                      <label>Email</label>
                      <input type="email" class="form-control" name="email" id="email" placeholder="Email" required>
                  </div>

                  <div class="form-group">
                      <label>Celular</label>
                      <input type="celular" class="form-control phone" name="celular" id="celular" placeholder="Celular" required>
                  </div>

                  <div class="form-group">
                      <label>Patrocinador ID</label>
                      
                      <div class="input-group m-bot15">
                        <input type="number" class="form-control patrocinadorMask" name="patrocinador_id" id="patrocinador_id" placeholder="Patrocinador ID">
                          <span class="input-group-btn">
                            <button class="btn btn-info" id="verificar_usr" type="button">Verificar</button>
                          </span>
                      </div>
                      <span id="patrocinador_id_res" class="help-block">Este Valor es opcional en caso de saber quien es tu patrocinador o si es diferente al usuario que te esta registrando.</span>
                      
                  </div>

                  <br/>
                  <div class="row" id="defaultimg">
                    
                    @for ($i = 1; $i <= 6; $i++)
                      <div class="col-xs-2 radio_button_img defaulh">
                        <input type="radio" name="foto_default" value="{{ $i }}" id="fd{{ $i }}" {{$i==1 ? "checked='checked'" : "" }} />
                        <label for="fd{{ $i }}"><img src="{{ asset('') }}vendor/theme/images/avatars/{{ $i }}.png" class="img-responsive"/></label>
                      </div>
                    @endfor

                    @for ($i = 7; $i <= 12; $i++)
                      <div class="col-xs-2 radio_button_img defaulm">
                        <input type="radio" name="foto_default" value="{{ $i }}" id="fd{{ $i }}" {{$i==1 ? "checked='checked'" : "" }} />
                        <label for="fd{{ $i }}"><img src="{{ asset('') }}vendor/theme/images/avatars/{{ $i }}.png" class="img-responsive"/></label>
                      </div>
                    @endfor
 
                  </div>

                  <button type="submit" class="btn btn-info" style="width:100%">Agregar Socio</button>
              </form>
          </div>
      </div>
  </div>
</div> 



        <!--
        <div class="tree">
            <ul>
                <li>
                    <a href="#">
                       <div class="tree-pic tree-pic-paid text-center">
                            <img src="{{ asset('') }}vendor/theme/images/jorge.jpg" alt=""/>
                            <div> Jorge Flores </div>
                       </div>
                    </a>
                    <ul>
                        <li>
                            <a href="#">
                                <div class="tree-pic tree-pic-new text-center">
                                    <img src="{{ asset('') }}vendor/theme/images/scavatar1.jpg" alt=""/>
                                    <div> Sara Carrillo</div>
                               </div>
                            </a>
                            <ul>
                                <li>
                                    <a href="#">
                                        <div class="tree-pic tree-pic-new text-center">
                                            <img src="{{ asset('') }}vendor/theme/images/scavatar2.jpg" alt=""/>
                                            <div> Juan Flores</div>
                                       </div>
                                    </a>
                                    <ul>
                                        <li>
                                            <a href="saborcolima_registro.html">
                                                <div class="tree-pic text-center">
                                                    <img src="{{ asset('') }}vendor/theme/images/newuser.png" alt=""/>
                                                    <div> </div>
                                               </div>
                                            </a>
                                        </li>
                                        <li>
                                             <a href="saborcolima_registro.html">
                                                <div class="tree-pic text-center">
                                                    <img src="{{ asset('') }}vendor/theme/images/newuser.png" alt=""/>
                                                    <div> </div>
                                               </div>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="#">
                                        <div class="tree-pic tree-pic-new text-center">
                                            <img src="{{ asset('') }}vendor/theme/images/scavatar3.jpg" alt=""/>
                                            <div> Heriberto Garcia</div>
                                       </div>
                                    </a>
                                    <ul>
                                        <li>
                                            <a href="saborcolima_registro.html">
                                                <div class="tree-pic text-center">
                                                    <img src="{{ asset('') }}vendor/theme/images/newuser.png" alt=""/>
                                                    <div> </div>
                                               </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="saborcolima_registro.html">
                                                <div class="tree-pic text-center">
                                                    <img src="{{ asset('') }}vendor/theme/images/newuser.png" alt=""/>
                                                    <div> </div>
                                               </div>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#">
                                <div class="tree-pic tree-pic-paid text-center">
                                    <img src="{{ asset('') }}vendor/theme/images/juanpablo.jpg" alt=""/>
                                    <div> Juan Pablo Rocha</div>
                               </div>
                            </a>
                            <ul>
                                <li><a href="#">
                                    <div class="tree-pic tree-pic-new tree-pic-paid text-center">
                                    <img src="{{ asset('') }}vendor/theme/images/scavatar4.jpg" alt=""/>
                                    <div> Manuel Garcia</div>
                               </div>
                                </a>
                                    <ul>
                                        <li>
                                            <a href="saborcolima_registro.html">
                                                <div class="tree-pic text-center">
                                                    <img src="{{ asset('') }}vendor/theme/images/newuser.png" alt=""/>
                                                    <div> </div>
                                               </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="saborcolima_registro.html">
                                                <div class="tree-pic text-center">
                                                    <img src="{{ asset('') }}vendor/theme/images/newuser.png" alt=""/>
                                                    <div> </div>
                                               </div>
                                            </a>
                                        </li>
                                    </ul>

                                </li>
                                <li>
                                    <a href="#">
                                        <div class="tree-pic tree-pic-paid text-center">
                                            <img src="{{ asset('') }}vendor/theme/images/scavatar5.jpg" alt=""/>
                                            <div> Veronica Ruiz</div>
                                       </div>
                                    </a>
                                    <ul>
                                        <li>
                                            <a href="#">
                                                <div class="tree-pic text-center">
                                                    <img src="{{ asset('') }}vendor/theme/images/scavatar6.jpg" alt=""/>
                                                    <div> Oscar Cervantes</div>
                                               </div>
                                            </a>
                                            <ul>
                                                <li>
                                                    <a href="saborcolima_registro.html">
                                                        <div class="tree-pic text-center">
                                                            <img src="{{ asset('') }}vendor/theme/images/newuser.png" alt=""/>
                                                            <div> </div>
                                                       </div>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="saborcolima_registro.html">
                                                        <div class="tree-pic text-center">
                                                            <img src="{{ asset('') }}vendor/theme/images/newuser.png" alt=""/>
                                                            <div> </div>
                                                       </div>
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li>
                                            <a href="saborcolima_registro.html">
                                                <div class="tree-pic text-center">
                                                    <img src="{{ asset('') }}vendor/theme/images/newuser.png" alt=""/>
                                                    <div> </div>
                                               </div>
                                            </a>
                                        </li>
                                    </ul>
                                </li>   
                            </ul>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
        -->


       </section>
    </div>
    </div>

@endsection
