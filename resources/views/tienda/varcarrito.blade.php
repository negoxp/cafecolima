<form method="GET" action="{{ url('/tienda/checkout') }}" accept-charset="UTF-8">
      <meta name="csrf-token" content="{{ csrf_token() }}">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
      @foreach($cart as $c)
      <?php $p=$c->model; ?>
      <div class="row">   
        <div class="col-md-3">
          <h3>
          @if(file_exists(public_path().$p->foto) and $p->foto)
              <img src="{{ asset('') }}{{$p->foto}}" alt="" class="img-thumbnail" width="110">
          @endif
          </h3>
        </div>
        
        <div class="col-md-6">
          <h4>{{$p->nombre}}</h4>
          	<span class="label label-default">$ {{number_format($p->costo_neto,2)}}</span>
              <br/><br/>
              <div class="input-group number-spinner col-md-6 pull-right">
                  <span class="input-group-btn">
                      <button class="btn btn-default cartMenos" data-dir="dwn" productoId="{{$p->id}}" ><span class="glyphicon glyphicon-minus"></span></button>
                  </span>
                  <input class="form-control text-center" value="{{$c->qty}}" type="number" max="50" min="1" step="1">
                  <span class="input-group-btn">
                      <button class="btn btn-default cartMas" data-dir="up" productoId="{{$p->id}}" ><span class="glyphicon glyphicon-plus"></span></button>
                  </span>
              </div>
        </div>
        <div class="col-md-3 item-cart">
          <h3>$ {{number_format($p->costo_neto*$c->qty,2)}}</h3>

          <button data-toggle="button" class="btn btn-white pull-right shoppingCart_orderLine_delete eliminarCarrito" productoId="{{$p->id}}">
              <i class="fa fa-trash"></i>
          </button>

        </div>
      </div>
      <hr>
      @endforeach

      <div class="row">  
      	<div class="col-md-12">
      		<h3 class="pull-right">SUBTOTAL $ {{$subtotal}}</h3>
     		</div>
      </div> 
</form>






