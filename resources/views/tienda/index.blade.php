@extends('layouts.admin')

@section('content')

<h4>Tienda </h4>


<!-- page start-->

        <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <header class="panel-heading">
                        Productos
                        <span class="tools pull-right">
                            <a href="javascript:;" class="fa fa-chevron-down"></a>
                         </span>
                    </header>
                    <div class="panel-body">

                        <div id="gallery" class="media-gal">
                            
                            @foreach($productos as $p)
                                <div class="images item " >
                                    <a href="#myModal" data-toggle="modal">
                                        @if(file_exists(public_path().$p->foto) and $p->foto)
                                            <img src="{{ asset('') }}{{$p->foto}}" alt="">
                                        @endif
                                    </a>
                                    <div class="row">
                                    	<div class="col-sm-6">
                                    		{{$p->nombre}}
                                    	</div>
                                    	<div class="col-sm-6">
                                    		<div class="installments-price pull-right">$ {{number_format($p->costo_neto,2)}}</div>
                                            @if ($p->puntos>0)
                                                <div class="pull-right badge badge-success">Puntos: {{$p->puntos,0}}</div>
                                            @endif
                                    	</div>
                                    	<br/><br/>
                                    	<div class="col-sm-6">
                                    	</div>
                                    	<div class="col-sm-6">
                                    		<button type="button" class="btn btn-primary myCar" productoId="{{$p->id}}" productoNombre="{{$p->nombre}}" productoCosto="{{$p->costo_neto}}" >
                                                <i class="fa fa-plus"></i> Agregar</button>
                                    	</div>		
                                    </div>	
                                </div>
                            @endforeach

                      
                        </div>


                    </div>
                </section>
            </div>
        </div>
        <!-- page end--> 

@endsection