<div class="prf-contacts">
    <h2> <span><i class="fa fa-map-marker"></i></span> Solicitud de Socio</h2>
    <div class="location-info">
        <p>
        	Nombre: <strong> {{$request->get('nombres')}} {{$request->get('apellidos')}}</strong><br>
        	Celular: <strong> {{$request->get('celular')}}</strong><br>
        	Email: <strong> {{$request->get('email')}}</strong><br>
        	Genero: <strong> {{$request->get('genero')}}</strong><br>
        	¿Cómo te enteraste?: <strong> {{$request->get('enteraste')}}</strong><br>
         </p>

    </div>
</div>