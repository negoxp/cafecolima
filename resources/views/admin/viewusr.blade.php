@extends('layouts.admin')

@section('content')

<div class="row">
    <div class="col-md-12">
        <section class="panel">
            <div class="panel-body profile-information">
               <div class="col-md-2">
                   <div class="profile-pic text-center {{$me->pedido_mes_pagado ? 'tree-pic-paid' : $me->pago_ultimos_status_color() }}"> 
                       <img src="{{$me->getPhoto()}}" alt=""/>
                       @if(\Auth::user()->hasRole('admin'))
                         <?php
                         $p=$me->get_ultimo_pedido();
                         ?>
                         <div class="row">
                            <div class="col-md-12">
                              Pagado
                              <input type="checkbox" pedidoId="{{$p->id}}" class="switch-small pagadoclass"  {{$p->pagado ? 'checked=""' : ''}} data-on="success" data-off="danger" data-on-label="SI" data-off-label="NO">
                            </div>
                            
                            <div class="col-md-12">
                              </br>
                              Entregado
                            @if ($p->user->solo_red)
                              
                              <span class="label label-info label-mini">SOLO RED</span> 
                              </br>
                              </br>
                            @else
                              <input type="checkbox" pedidoId="{{$p->id}}" class="switch-small entregadoclass"  {{$p->entregado ? 'checked=""' : ''}} data-on="success" data-off="danger" data-on-label="SI" data-off-label="NO">
                            @endif
                            </div>


                            <div class="col-md-12">
                              Descuento
                              <input type="checkbox" pedidoId="{{$p->id}}" class="switch-small descuentoclass"  {{$p->hasDescuento() ? 'checked=""' : ''}} data-on="success" data-off="danger" data-on-label="SI" data-off-label="NO">
                            </div>
                         </div>
                       @endif 
                   </div>
                   
               </div>
               <div class="col-md-7">
                   <div class="profile-desk">
                       <h1><span class="mini-stat-icon tar">{{$me->getHijos()}}</span>{{$me->nombre}} <small>{{$me->apellidos}}</small></h1>
                       
                       <br/>
                        <p style="margin-left:0px; padding-bottom:0px;">
                        <span class="text-muted" style="padding-bottom: 2px;">  
                          # Socio <span class="label label-primary">{{ $me->id }}</span> 
                          <br/>
                          Red <span class="label label-primary">{{ $me->getHijos() }}</span> 
                          Nivel <span class="label label-primary">{{ $me->getNivel() }}</span>
                          Pagados <span class="label label-primary">{{ $me->getHijosPagados() }}</span>
                          % <span class="label label-success" style="padding-bottom: 4px;">{{$me->getHijosPagadosPercent()}}%</span>
                          <br/>
                          Invitaciones Pendientes <span class="label label-info">{{ $me->getInvitacionesPendientes() }}/30</span>
                          @if (!$me->pedido_mes_pagado)
                          <br/>
                          <span class="label label-danger" style="width: 100%;"> Fecha límite de pago: <strong>{{$me->ultimo_dia_pago()}}</strong></span>
                          @endif
                        </span>


                        <!--
                         <br/>
                         <span class="label label-primary" style="padding-bottom: 4px;"> 4 </span> Usuarios Nuevos   
                         --> 
                      </p>

                        <div class="row" >
                            <div class="col-md-12">
                  @if ($me->pedido_mes_pagado)
                   <div class="alert alert-success ">
                      <span class="alert-icon"><i class="fa fa-thumbs-up"></i></span>
                      <div class="notification-info">
                          <ul class="clearfix notification-meta">
                              <li class="pull-left notification-sender">El pago del mes ha sido aceptado</li>
                              <!--<li class="pull-right notification-time">1 min ago</li>-->
                          </ul>
                          <p style="padding-bottom:0px;">
                              <a href="{{ url('/pedidos/invoice/'.$me->ultimo_pedido_id) }}" target="_blank"> <i class="fa fa-print" aria-hidden="true"></i> Imprimir comprobante</a>
                          </p>
                      </div>
                   </div>
                   @else ($me->pedido_mes_pagado)
                   <div class="alert alert-warning">
                      <span class="alert-icon"><i class="fa fa fa-bell-o"></i></span>
                      <div class="notification-info">
                          <ul class="clearfix notification-meta">
                              <li class="pull-left notification-sender">Aún no hemos procesado tu pago del mes</li>
                          </ul>
                          <p style="padding-bottom:0px;">
                            <i class="fa fa-shopping-cart"></i> Ver <a href="{{ url('/home/pedidos') }}"><span>Historial Pedidos</span></a>
                          </p>
                      </div>
                   </div>
                   @endif
                                
                            </div>
    
                        </div>

                   </div>
               </div>
               <div class="col-md-3">
                   <div class="profile-statistics">

                        <div class="row">
                            
                            <div class="col-md-12 col-xs-12 invoice-block pull-right">
                                <ul class="unstyled amounts db_profile">
                                    <li>Puntos: {{$me->getPuntosTienda()}}</li>
                                </ul>
                            </div>
                            
                            <div class="col-md-12 inv-label">
                                <h4>TU PEDIDO <strong>DICIEMBRE</strong></h4>
                            </div>
                            <div class="col-md-12 col-xs-12 invoice-block pull-right">
                                <ul class="unstyled amounts db_profile">
                                    @if ($me->pedido_mes_pagado)
                                      
                                      <li class="grand-total">
                                        <a href="{{ url('/pedidos/invoice/'.$pedido->id) }}" target="_blank">
                                        <i class="fa fa-print" aria-hidden="true"></i>
                                      </a>
                                        PAGADO
                                      </li>
                                    @else
                                    <li class="grand-total-pedido-actual">
                                      <a href="{{ url('/pedidos/invoice/'.$pedido->id) }}" target="_blank">
                                        <i class="fa fa-print" aria-hidden="true"></i>
                                      </a>
                                      TOTAL A PAGAR: $ {{$me->cantidad_ultimo_pedido()}}
                                    </li>
                                    @endif
                                </ul>
                            </div>

                            <div class="col-md-12 inv-label">
                                <h4>PRÓXIMA COMISION <strong>ENERO</strong></h4>
                            </div>
                            <div class="col-md-12 col-xs-12 invoice-block pull-right">
                                <ul class="unstyled amounts db_profile">
                                    <li>Comisión Tienda: ${{$me->getComisionTienda()}}</li>
                                    <li>Red: ${{$me->getComision(true)}}</li>
                                    <li>Próximo Pedido: $350.00 </li>
                                    @if ($me->solo_red or $p->hasDescuento())
                                      <li>Descuento solo red: - $75.00 </li>
                                    @endif
                                    <li>Sub- Total: ${{$me->getComisionTotal(350,true)}}</li>
                                    @if ($me->pedido_mes_pagado)
                                      <li class="grand-total">Gran Total : ${{$me->getComisionTotal()}}</li>
                                    @else
                                      <li class="grand-total-sp">SIN DERECHO A COMISIÓN</li>
                                    @endif
                                </ul>
                            </div>
                        </div>

                   </div>
               </div>
            </div>
        </section> 
    </div>
    <div class="col-md-12">
       <section class="panel DocumentList" style="max-height: 100%; overflow: auto">
        <div id="arbol">
        <div class="tree"> 
          <ul>
            {!! $me->getPrintPerson($me) !!}
          </ul>
        </div>
      </div>






       </section>
    </div>
    </div>

@endsection
