@extends('layouts.admin')

@section('content')

<h2>Validar Pedidos RECIBO</h2>
<div class="row">
    <div class="col-sm-12">
       <section id="flip-scroll" class="panel">
        
        <table class="table table-invoice table-responsive" id="dynamic-table">
            <thead class="cf">
            <tr>
                <th>ID</th>
                <th>Mes</th>
                <th>Nombre</th>
                <th class="text-center">Total</th>
                <th class="text-center">Estado</th>
                <th class="text-center">PAGADO</th>
            </tr>
            </thead>
            <tbody>
						@foreach($pedidos as $p)
            <tr>
                <td data-title="Id">{{$p->user_id}}</td>
                <td>{{$p->mespedido}}</td>
                <td>{{$p->user->nombrecompleto() }}</td>
                <td class="text-center">{{number_format((float)$p->total,2)}}</td>
                <td class="text-center">
                	<span class="label label-{{$p->getStatusCss()}} label-mini">{{$p->status}}</span>
                </td>
                <td class="text-center">
                    <input type="checkbox" pedidoId="{{$p->id}}" class="switch-small pagadoReciboclass"  {{$p->pagado ? 'checked=""' : ''}} data-on="success" data-off="danger" data-on-label="SI" data-off-label="NO">
                    
                    &nbsp;<a href="{{ url('/proceso/rechazar_pago/'.$p->id) }}" class="btn btn-danger btn-xs pull-right" style="margin-right:3px;"><i class="fa  fa-close"></i> Rechazar </a>&nbsp;

                    <a href="{{ url('download/'.$p->id) }}" class="btn btn-success btn-xs pull-right" style="margin-right:3px;"><i class="fa  fa-download"></i> Descargar</a> 
                	 
                    &nbsp;<a href="{{ url('/pedidos/invoice/'.$p->id) }}" class="btn btn-warning btn-xs pull-right" style="margin-right:3px;"><i class="fa  fa-barcode"></i> Imprimir </a>&nbsp; 

                </td>
            </tr>
            @endforeach

            </tbody>
        </table>

       </section>
    </div>
</div>

@endsection