@extends('layouts.admin')

@section('content')

<h2>Corte 2017 05</h2>


<div class="row">
    <div class="col-sm-12">
       <section id="flip-scroll" class="panel">
        
        <table class="table table-invoice table-responsive" id="dynamic-table2">
            <thead class="cf">
            <tr>
                <th>ID</th>
                <th>Nombre</th>
                <th>Red Total</th>
                <th>Red Pagado</th>
                <th>Comision</th>
                <th>Recompra</th>
                <th>Bono</th>
                <th>C. Tienda</th>
                <th>Comision a Pagar</th>
                <th>Banco Nombre</th>
                <th>Titular</th>
                <th>Cuenta</th>
                <th>Clabe</th>
            </tr>
            </thead>
            <tbody> 
            	@foreach($comisiones as $c)
            		<tr>
                <td>{{$c->user->id}}</td>
                <td>{{$c->user->nombrecompleto()}}</td>
                <td>{{$c->red_total}}</td>
                <td>{{$c->red_pagados}}</td>
                <td>{{$c->comision_total}}</td>
                <td>{{$c->descuento_proximo_pedidos}}</td>
                <td>{{$c->bono}}</td>
                <td>{{$c->comision_tienda}}</td>
                <td><strong>{{$c->comision_pagar}}</strong></td>
                <td>{{$c->user->banco_nombre}}</td>
                <td>{{$c->user->banco_titular}}</td>
                <td>{{$c->user->banco_cuenta}}</td>
                <td>{{$c->user->banco_clabe}}</td>
            		</tr>
            	@endforeach
            </tbody>
        </table>

       </section>
    </div>
</div>

@endsection
