@extends('layouts.admin')

@section('content')

<h2>Usuarios</h2>
<div class="row">
    <div class="col-sm-12">
       <section id="flip-scroll" class="panel">
        
        <table class="table table-invoice table-responsive" id="dynamic-table">
            <thead class="cf">
            <tr>
                <th>ID</th>
                <th>Usuario</th>
                <th>Pedido Cantidad</th>
                <th>Pagó</th>
                <th>Red</th>
                <th>Red S/Pagar</th>
                <th>Red Pagados</th>
                
                <th>Comision</th>
                
                <th>Pagar Proximo Pedido</th>
                <th>Comision Retenida</th>

                <th>Comisión a Pagar</th>
            </tr>
            </thead>
            <tbody>
			<?php
            $i=1;
            $tPedidoCantidad=0;
            $tPago=0;
            $tRed=0;
            $tRedSpagar=0;
            $tRedPagados=0;
            $tComision=0;
            $tProxPedido=0;
            $tComisionRetenida=0;
            $tPagarComision=0;

            ?>
            @foreach($usuarios as $s)
            <?php
                $pedidoCantidad=$s->getTotalPedidoMes();
                $pago=$s->pedido_mes_pagado ? $s->getTotalPedidoMes() : 0;
                $red=$s->getHijos();
                $redPagados=$s->getHijosPagados();
                $redSpagar=$s->getHijos() - $redPagados;
                $comision=$s->getComision();
                $proxPedido=$comision > 350 ? 0 : (350-$comision);
                $comisionRetenida= $comision >= 350 ? 350  : $comision ;
                $pagarComision= ($comision-350) > 0 ? ($comision-350)  : 0 ;

                $tPedidoCantidad+=$pedidoCantidad;
                $tPago+=$pago;
                $tRed+=$red;
                $tRedPagados+=$redPagados;
                $tRedSpagar+=$redSpagar;
                $tComision+=$comision;
                $tProxPedido+=$proxPedido;
                $tComisionRetenida+=$comisionRetenida;
                $tPagarComision+=$pagarComision;
            ?>
            
            <tr>
                <td data-title="Id">{{$i++}}</td>
                <td data-title="Usuario">{{$s->shortname()}}</td>
                <td data-title="Cantidad Pago">$ {{number_format($pedidoCantidad,2)}}</td>
                <td data-title="pago">$ {{number_format($pago,2)}}</td>
                
                <td data-title="red">{{$red}}</td>
                <td data-title="s/pagar">{{$redSpagar}}</td>
                <td data-title="pagados">{{$redPagados}}</td>
                
                <td data-title="comision">$ {{number_format($comision,2)}}</td>
                <td data-title="Proximo Pedido">$ {{number_format($proxPedido,2)}}</td>
                <td data-title="Comision Retenida">$ {{number_format($comisionRetenida,2)}}</td>
                <td data-title="Pagar comision">$ {{number_format($pagarComision,2)}}</td>
            </tr>
            @endforeach

            </tbody>
            <tfoot>
            <tr>
                <th>ID</th>
                <th>Usuario</th>
                <td >$ {{number_format($tPedidoCantidad,2)}}</td>
                <td >$ {{number_format($tPago,2)}}</td>
                
                <td >{{$tRed}}</td>
                <td >{{$tRedSpagar}}</td>
                <td >{{$tRedPagados}}</td>
                
                <td >$ {{number_format($tComision,2)}}</td>
                <td >$ {{number_format($tProxPedido,2)}}</td>
                <td >$ {{number_format($tComisionRetenida,2)}}</td>
                <td >$ {{number_format($tPagarComision,2)}}</td>
            </tr>
            </tfoot>
        </table>

       </section>
    </div>
</div>

@endsection