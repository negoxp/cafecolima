@extends('layouts.admin')

@section('content')

<form id="sform" action="{{ url('/admin/pedidos-entregados') }}" method="get">
<h2>Pedidos ENTREGADOS 
<select name="corte" id="scorte" url="">
    <option name="{{ $mescorte }}">{{ $mescorte }}</option>
    @foreach($cortes as $c)
    <option name="{{ $c->mescorte }}" {{ $c->mescorte==$scorte ? "selected": ""}}>{{ $c->mescorte }}</option>
    @endforeach
</select>
</h2>
</form>
<div class="row">
    <div class="col-sm-12">
       <section id="flip-scroll" class="panel">
        
        <table class="table table-invoice table-responsive" id="dynamic-table">
            <thead class="cf">
            <tr>
                <th>User-ID</th>
                <th>Mes</th>
                <th>Nombre</th>
                <th class="text-center">Total</th>
                <th class="text-center">Autorizado</th>
                <th class="text-center">Estado</th>
                <th class="text-center">ENTREGADO</th>
            </tr>
            </thead>
            <tbody>
						@foreach($pedidos as $p)
            <tr>
                <td data-title="Id">{{$p->user_id}}</td>
                <td>{{$p->mespedido}}</td> 
                <td>{{$p->user->nombrecompleto() }}</td>
                <td class="text-center">{{number_format((float)$p->total,2)}}</td>
                <td>{{$p->autorizadopor() }}</td> 
                <td class="text-center">
                	<span class="label label-{{$p->getStatusCss()}} label-mini">{{$p->status}}</span> 
                    
                </td>
                <td class="text-center">
                    @if ($p->user->solo_red)
                    <span class="label label-info label-mini">SOLO RED</span> 
                    @else
                    <input type="checkbox" pedidoId="{{$p->id}}" class="switch-small entregadoclass"  {{$p->entregado ? 'checked=""' : ''}} data-on="success" data-off="danger" data-on-label="SI" data-off-label="NO">
                    @endif
                    
                     	
                </td>
            </tr>
            @endforeach

            </tbody>
        </table>

       </section>
    </div>
</div>

@endsection