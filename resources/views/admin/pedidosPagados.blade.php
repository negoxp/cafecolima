@extends('layouts.admin')

@section('content') 

<form id="sform" action="{{ url('/admin/pedidos-pagados') }}" method="get">
<h2>Pedidos PAGADOS 
<select name="corte" id="scorte" url="">
    <option name="{{ $mescorte }}">{{ $mescorte }}</option>
    @foreach($cortes as $c)
    <option name="{{ $c->mescorte }}" {{ $c->mescorte==$scorte ? "selected": ""}}>{{ $c->mescorte }}</option>
    @endforeach
</select>
</h2>
</form>


<div class="row">
    <div class="col-sm-12">
       <section id="flip-scroll" class="panel">
        
        <table class="table table-invoice table-responsive" id="dynamic-table-pagados">
            <thead class="cf">
            <tr>
                <th>ID</th>
                <th>Mes</th>
                <th>Nombre</th>
                <th class="text-center">Total</th>
                <th class="text-center">Autorizado</th>
                <th>Fecha </th>
                <th class="text-center">Forma</th>
                <th class="text-center">Estado</th>
                <th class="text-center">PAGADO</th>
                <th class="text-center"></th>
            </tr>
            </thead>
            <tbody>
						@foreach($pedidos as $p)
            <tr>
                <td data-title="Id">{{$p->user_id}}</td>
                <td>{{$p->mespedido}}</td>
                <td>{{$p->user->nombrecompleto() }}</td>
                <td class="text-center">{{number_format((float)$p->total,2)}}</td>
                <td>{{$p->autorizadopor() }}</td>
                <td>{{$p->autorizacion_fecha}}</td>
                <td>{{$p->autorizacion_tipo }}</td>
                <td class="text-center">
                	<span class="label label-{{$p->getStatusCss()}} label-mini">{{$p->status}}</span>
                </td>
                <td class="text-center">

                    <input type="checkbox" pedidoId="{{$p->id}}" class="switch-small pagadoclass"  {{$p->pagado ? 'checked=""' : ''}} data-on="success" data-off="danger" data-on-label="SI" data-off-label="NO" {{$p->entregado ? 'disabled' : ''}}>
                 </td>
                <td class="text-center">
                    @if ($p->autorizacion_tipo=="recibo")
                    	&nbsp;<a href="{{ url('download/'.$p->id) }}" class="btn btn-success btn-xs pull-right" style="margin-right:3px;"><i class="fa  fa-download"></i> Descargar</a> &nbsp;
                    @endif 
                    &nbsp;<a href="{{ url('/pedidos/invoice/'.$p->id) }}" class="btn btn-warning btn-xs pull-right" style="margin-right:3px;"><i class="fa  fa-barcode"></i> Imprimir </a>&nbsp;                        	
                </td>
            </tr>
            @endforeach

            </tbody>
        </table>

       </section>
    </div>
</div>

@endsection