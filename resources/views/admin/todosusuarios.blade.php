@extends('layouts.admin')

@section('content')

<h2>Usuarios</h2>
<div class="row">
    <div class="col-sm-12">
       <section id="flip-scroll" class="panel">
        
        <table class="table table-invoice table-responsive" id="dynamic-table2">
            <thead class="cf">
            <tr>
                <th>ID</th>
                <th>Nombre</th>
                <th>Apellidos</th>
                <th>Email</th>
                <th>Telefono</th>
                <th>Celular</th>
                <th>Genero</th>
                <th>Dirección</th>
                <th>Colonia</th>
                <th>Ciudad</th>
                <th>Estado</th>
                <th>CP</th>
                <th>Banco</th>
                <th>Titular</th>
                <th>Cuenta</th>
                <th>Clabe</th>
                <th>Fundador</th> 
                <th>Fecha</th> 
                <th>Padre</th>
                <th>Presentador</th>
                <th>Registro</th>
                <th>Corte 201703</th>
            </tr>
            </thead>
            <tbody>
		      @foreach($usuarios as $u)
            <tr>
                <td><a href="users/{{$u->id}}/edit">{{$u->id}}</a></td>
                <td>{{$u->nombre}}</td>
                <td>{{$u->apellidos}}</td>
                <td>{{$u->email}}</td>
                <td>{{$u->telefono}}</td>
                <td>{{$u->celular}}</td>
                <td>{{$u->genero}}</td>
                <td>{{$u->direccion}}</td>
                <td>{{$u->colonia}}</td>
                <td>{{$u->ciudad}}</td>
                <td>{{$u->estado}}</td>
                <td>{{$u->cp}}</td>
                <td>{{$u->banco_nombre}}</td>
                <td>{{$u->banco_titular}}</td>
                <td>{{$u->banco_cuenta}}</td>
                <td>{{$u->banco_clabe}}</td>
                <td>{{$u->fundador}}</td>
                <td>{{$u->created_at}}</td>
                <td>{{$u->getNameid($u->padre_id) }}</td>
                <td>{{$u->getNameid($u->presentador_id) }}</td>
                <td>{{$u->getNameid($u->registro_id) }}</td>
                <td>{{$u->status_corte_pasado() }}</td>
                
            </tr>
            @endforeach

            </tbody>
        </table>

       </section>
    </div>
</div>

@endsection