@extends('layouts.admin')

@section('content')

<h2>Validar Pedidos CARRITO</h2>
<div class="row">
    <div class="col-sm-12">
       <section id="flip-scroll" class="panel">
        
        <table class="table table-invoice table-responsive" id="dynamic-table3">
            <thead class="cf">
            <tr>
                <th>Pedido ID</th>
                <th>Mes</th>
                <th>Fecha</th>
                <th>Socio</th>
                <th>Nombre</th>
                <th>Presentador</th>
                <th class="text-center">Total</th>
                <th class="text-center">Estado</th>
                <th class="text-center">PAGADO</th>
            </tr>
            </thead>
            <tbody>
						@foreach($pedidos as $p)
            <tr>
                <td data-title="Id">{{$p->id}}</td>
                <td>{{$p->mespedido}}</td>
                <td>{{$p->created_at}}</td>
                <td>{{$p->user_id}}</td>
                <td>{{$p->user->nombrecompleto() }}</td>
                <td>{{$p->presentadorNombre() }}</td>
                
                <td class="text-center">{{number_format((float)$p->total,2)}}</td>
                <td class="text-center">
                	<span class="label label-{{$p->getStatusCss()}} label-mini">{{$p->status}}</span>
                </td>
                <td class="text-center">
                    <input type="checkbox" pedidoId="{{$p->id}}" class="switch-small tpagadoReciboclass"  {{$p->pagado ? 'checked=""' : ''}} data-on="success" data-off="danger" data-on-label="SI" data-off-label="NO">
                    
                    &nbsp;<a href="{{ url('/proceso/t_rechazar_pago/'.$p->id) }}" class="btn btn-danger btn-xs pull-right" style="margin-right:3px;"><i class="fa  fa-close"></i> Rechazar </a>&nbsp;

                	<a downpath="{{ url('tdownload/'.$p->id) }}" imgpath="{{ URL::to('/').$p->comprobante }}" href="#" class="btn btn-success btn-xs pull-right showImg" style="margin-right:3px;"><i class="fa  fa-download"></i> </a> 

                    &nbsp;<a href="{{ url('/tpedidos/'.$p->id.'/editp') }}" class="btn btn-default btn-xs"><i class="fa  fa-pencil"></i></a> 
                </td>
            </tr>
            @endforeach

            </tbody>
        </table>

       </section>
    </div>
</div>

@endsection