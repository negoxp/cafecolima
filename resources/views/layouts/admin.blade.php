    <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Jorge Flores">
    <!--<link rel="shortcut icon" href="images/favicon.png">-->

    <title>Café sabor a Colima</title>


    <!--Core CSS -->
    <link href="{{ asset('') }}vendor/theme/bs3/css/bootstrap.min.css" rel="stylesheet">
    <link href="{{ asset('') }}vendor/theme/css/bootstrap-reset.css" rel="stylesheet">
    <link href="{{ asset('') }}vendor/theme/font-awesome/css/font-awesome.css" rel="stylesheet" />

    <!-- Custom styles for this template -->
    <link href="{{ asset('') }}vendor/theme/css/style.css" rel="stylesheet">
    <link href="{{ asset('') }}vendor/theme/css/style-responsive.css" rel="stylesheet" />
    <link href="{{ asset('') }}vendor/theme/css/table-responsive.css" rel="stylesheet" />
    <link rel="stylesheet" href="{{ asset('') }}vendor/theme/css/bootstrap-switch.css" />

    <!--dynamic table-->
    <link href="{{ asset('') }}vendor/theme/js/advanced-datatable/css/demo_page.css" rel="stylesheet" />
    <link href="{{ asset('') }}vendor/theme/js/advanced-datatable/css/demo_table.css" rel="stylesheet" />
    <link rel="stylesheet" href="{{ asset('') }}vendor/theme/js/data-tables/DT_bootstrap.css" />

    <link href="{{ asset('') }}vendor/theme/css/tree2.css" rel="stylesheet"> 
    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]>
    <script src="{{ asset('') }}vendor/theme/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <script type="text/javascript">
        var url_general="{{ url('/')}}";
    </script>

</head>
<body id="app-layout">

<section id="container" >
<!--header start-->
<header class="header fixed-top clearfix">
<!--logo start-->
<div class="brand">

    <a href="{{ url('/home') }}" class="logo">
        <img src="{{ asset('') }}img/logotop.png" alt="CafeLogo" width="183" height="32" style="border-width:0;display:block" border="0" class="CToWUd">
    </a>
    <div class="sidebar-toggle-box">
        <div class="fa fa-bars"></div>
    </div>
</div>
<!--logo end-->

<div class="nav notify-row" id="top_menu">
    <!--  notification start -->
    <ul class="nav top-menu">
        <!-- settings start -->
        <li class="dropdown">
            <a class="myCarmodal" href="#">
                <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                <span class="badge bg-warning">{{Cart::content()->count()}}</span>
            </a>
        </li>
        <!-- settings end -->
        <?php
        /*
        <!-- inbox dropdown start-->
        
        <li id="header_inbox_bar" class="dropdown">
            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                <i class="fa fa-envelope-o"></i>
                <span class="badge bg-important">4</span>
            </a>
            <ul class="dropdown-menu extended inbox">
                <li>
                    <p class="red">You have 4 Mails</p>
                </li>
                <li>
                    <a href="#">
                        <span class="photo"><img alt="avatar" src="{{ asset('') }}vendor/theme/images/avatar-mini.jpg"></span>
                                <span class="subject">
                                <span class="from">Jonathan Smith</span>
                                <span class="time">Just now</span>
                                </span>
                                <span class="message">
                                    Hello, this is an example msg.
                                </span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <span class="photo"><img alt="avatar" src="{{ asset('') }}vendor/theme/images/avatar-mini-2.jpg"></span>
                                <span class="subject">
                                <span class="from">Jane Doe</span>
                                <span class="time">2 min ago</span>
                                </span>
                                <span class="message">
                                    Nice admin template
                                </span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <span class="photo"><img alt="avatar" src="{{ asset('') }}vendor/theme/images/avatar-mini-3.jpg"></span>
                                <span class="subject">
                                <span class="from">Tasi sam</span>
                                <span class="time">2 days ago</span>
                                </span>
                                <span class="message">
                                    This is an example msg.
                                </span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <span class="photo"><img alt="avatar" src="{{ asset('') }}vendor/theme/images/avatar-mini.jpg"></span>
                                <span class="subject">
                                <span class="from">Mr. Perfect</span>
                                <span class="time">2 hour ago</span>
                                </span>
                                <span class="message">
                                    Hi there, its a test
                                </span>
                    </a>
                </li>
                <li>
                    <a href="#">See all messages</a>
                </li>
            </ul>
        </li>

        <!-- inbox dropdown end -->
        <!-- notification dropdown start-->
        <li id="header_notification_bar" class="dropdown">
            <a data-toggle="dropdown" class="dropdown-toggle" href="#">

                <i class="fa fa-bell-o"></i>
                <span class="badge bg-warning">3</span>
            </a>
            <ul class="dropdown-menu extended notification">
                <li>
                    <p>Notifications</p>
                </li>
                <li>
                    <div class="alert alert-info clearfix">
                        <span class="alert-icon"><i class="fa fa-bolt"></i></span>
                        <div class="noti-info">
                            <a href="#"> Server #1 overloaded.</a>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="alert alert-danger clearfix">
                        <span class="alert-icon"><i class="fa fa-bolt"></i></span>
                        <div class="noti-info">
                            <a href="#"> Server #2 overloaded.</a>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="alert alert-success clearfix">
                        <span class="alert-icon"><i class="fa fa-bolt"></i></span>
                        <div class="noti-info">
                            <a href="#"> Server #3 overloaded.</a>
                        </div>
                    </div>
                </li>

            </ul>
        </li>
        */ ?>
        <!-- notification dropdown end -->
    </ul>
    <!--  notification end -->
</div>

<div class="top-nav clearfix">
    <!--search & user info start-->
    <ul class="nav pull-right top-menu">
        <!--
        <li>
            <input type="text" class="form-control search" placeholder=" Search">
        </li>
    -->
        <!-- user login dropdown start-->
        @if(\Auth::user())
        <li class="dropdown">
            <a data-toggle="dropdown" class="dropdown-toggle {{Auth::user()->pedido_mes_pagado ? 'pic-paid_name' : ''}}" href="#">
                <img alt="" src="{{ Auth::user()->getPhoto()}}" class="{{Auth::user()->pedido_mes_pagado ? 'pic-paid_top' : ''}}">
                <span class="username">{{ Auth::user()->shortname() }}</span>
                <b class="caret"></b>
            </a>
            <ul class="dropdown-menu extended logout">
                <li><a href="{{ url('/users/'.\Auth::user()->id.'/editme') }}"><i class=" fa fa-suitcase"></i>Perfil</a></li>
                <!--<li><a href="#"><i class="fa fa-cog"></i> Settings</a></li>-->
                <li><a href="{{ url('/logout') }}"><i class="fa fa-key"></i>Salir</a></li>
            </ul>
        </li>
         @endif
        <!-- user login dropdown end -->
        <!--
        <li>
            <div class="toggle-right-box">
                <div class="fa fa-bars"></div>
            </div>
        </li>
    -->
    </ul>
    <!--search & user info end-->
</div>
</header>
<!--header end-->
<aside>
    <div id="sidebar" class="nav-collapse">
        <!-- sidebar menu start-->
        <div class="leftside-navigation">
            <ul class="sidebar-menu" id="nav-accordion">
            <!-- Authentication Links -->
            @if(\Auth::user())
                @if(!\Auth::user()->hasRole('moderador'))
                    <li>
                        <a href="{{ url('/home') }}">
                            <i class="fa fa-share-alt"></i>
                            <span>Mi Red</span>
                        </a>
                    </li>
                    <li class="sub-menu">
                        <a href="javascript:;">
                            <i class="fa fa-bars" aria-hidden="true"></i>
                            <span>Listas</span>
                        </a>
                    
                        <ul class="sub">
                            <?php /*
                            <li>
                                <a href="{{ url('/home/milista') }}">
                                    <i class="fa fa-share-alt"></i>
                                    <span>Mi Red Lista</span>
                                </a>
                            </li>
                            */ ?>
                            <li>
                                <a href="{{ url('/home/misdirectos') }}">
                                    <i class="fa fa-users"></i>
                                    <span>Mis Directos</span>
                                </a>
                            </li>
                            

                             
                        </ul>
                    </li>
                    <li>
                        <a href="{{ url('/home/pedidos') }}">
                            <i class="fa fa-shopping-cart"></i>
                            <span>Historial Pedidos</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('/home/comisiones') }}">
                            <i class="fa fa-money"></i>
                            <span>Mi Comision</span>
                        </a>
                    </li>
                    <li>
                        <a href="https://login.justhost.com/hosting/webmail" target="_blank">
                            <i class="fa fa-envelope-o"></i>
                            <span>Mail</span>
                        </a>
                    </li>
                    <!--
                    <li class="sub-menu">
                        <a href="javascript:;">
                            <i class="fa fa-file-image-o" aria-hidden="true"></i>
                            <span>Impresos</span>
                        </a>
                        <ul class="sub">
                            <li>
                                <a href="{{ url('/home/impresos-volantes') }}">
                                    <i class="fa fa-newspaper-o"></i>
                                    <span>Descargables</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    -->
                    <li>
                        <a href="{{ url('/tienda/') }}">
                            <i class="fa fa-shopping-bag" aria-hidden="true"></i>
                            <span>Tienda</span>
                        </a>
                    </li>


                    
                @endif
                @if(\Auth::user()->hasRole('admin')) 
                    

                    <li class="sub-menu">
                        <a href="javascript:;">
                            <i class="fa fa-th"></i>
                            <span>Administrador</span>
                        </a>
                        <ul class="sub">
                            <li>
                                <a href="{{ url('/admin/comisiones') }}">
                                    <i class="fa  fa-money"></i>
                                    <span>Comisiones</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{ url('/admin/comisionespasadas') }}">
                                    <i class="fa fa-scissors"></i>
                                    <span>Corte Mes Anterior</span>
                                </a>
                            </li>
                            <li >
                                  <a href="{{ url('/users') }}" title="Users">
                                      <i class="fa  fa-user"></i>
                                      <span>Usuarios</span>
                                  </a>
                            </li>
                            <li >
                                  <a href="{{ url('/admin/viewusr?usrid=1') }}" title="Users">
                                      <i class="fa fa-globe"></i>
                                      <span>Toda la Red</span>
                                  </a>
                            </li>
                            <li >
                                  <a href="{{ url('/admin/todosusuarios') }}" title="Users">
                                      <i class="fa fa-database"></i>
                                      <span>Base de Datos</span>
                                  </a>
                            </li>
                            <li >
                                  <a href="{{ url('/admin/nuevos') }}" title="Users">
                                      <i class="fa fa-user-circle-o"></i>
                                      <span>Nuevos Usuarios</span>
                                  </a>
                            </li>
                            <li >
                                  <a href="{{ url('/productos/') }}" title="Users">
                                      <i class="fa fa-shopping-basket"></i>
                                      <span>Productos</span>
                                  </a>
                            </li>
                            <!--
                            <li >
                                  <a href="{{ url('/admin/basededatos') }}" title="Users">
                                      <i class="fa fa-database"></i>
                                      <span>Base de Datos</span>
                                  </a>
                            </li>
                            -->
                        </ul>
                    </li>

                    <li class="sub-menu">
                        <a href="javascript:;">
                            <i class="fa fa-eye"></i>
                            <span>Moderador</span>
                        </a>
                        <ul class="sub">
                            <li>
                                <a href="{{ url('/admin/validar-pedidos-efectivo') }}">
                                    <i class="fa fa-usd"></i>
                                    <span>Moderar Pago en Efectivo</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{ url('/admin/validar-pedidos-recibo') }}">
                                    <i class="fa fa-credit-card"></i>
                                    <span>Moderar Pago con Recibo</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{ url('/admin/pedidos-pagados') }}">
                                    <i class="fa fa-handshake-o"></i>
                                    <span>Pedidos Pagados</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{ url('/admin/pedidos-entregados') }}">
                                    <i class="fa fa-dropbox"></i>
                                    <span>Pedidos Entregados</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{ url('/admin/validar-pedidos-carrito') }}">
                                    <i class="fa fa-shopping-bag" aria-hidden="true"></i><i class="fa fa-usd"></i>
                                    <span>Moderar Pago Carrito</span>
                                </a> 
                            </li>
                        </ul>
                    </li>


                @endif

                @if(\Auth::user()->hasRole('moderador'))
                    

                    <li class="sub-menu">
                        <a href="javascript:;">
                            <i class="fa fa-eye"></i>
                            <span>Moderador</span>
                        </a>
                        <ul class="sub">
                            <li>
                                <a href="{{ url('/admin/validar-pedidos-recibo') }}">
                                    <i class="fa fa-credit-card"></i>
                                    <span>Moderar Pago con Recibo</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{ url('/admin/pedidos-pagados') }}">
                                    <i class="fa fa-handshake-o"></i>
                                    <span>Pedidos Pagados</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{ url('/admin/pedidos-entregados') }}">
                                    <i class="fa fa-dropbox"></i>
                                    <span>Pedidos Entregados</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{ url('/admin/validar-pedidos-recibo') }}">
                                    <i class="fa fa-shopping-bag" aria-hidden="true"></i><i class="fa fa-credit-card"></i>
                                    <span>Moderar Pago Carrito</span>
                                </a> 
                            </li>
                        </ul>
                    </li>


                @endif


            @endif


            <!--
            <li class="sub-menu">
                <a href="javascript:;">
                    <i class="fa fa-laptop"></i>
                    <span>Layouts</span>
                </a>
                <ul class="sub">
                    <li><a href="boxed_page.html">Boxed Page</a></li>
                    <li><a href="horizontal_menu.html">Horizontal Menu</a></li>
                    <li><a href="language_switch.html">Language Switch Bar</a></li>
                </ul>
            </li>
            <li class="sub-menu">
                <a href="javascript:;">
                    <i class="fa fa-book"></i>
                    <span>UI Elements</span>
                </a>
                <ul class="sub">
                    <li><a href="general.html">General</a></li>
                    <li><a href="buttons.html">Buttons</a></li>
<li><a href="typography.html">Typography</a></li>
                    <li><a href="widget.html">Widget</a></li>
                    <li><a href="slider.html">Slider</a></li>
                    <li><a href="tree_view.html">Tree View</a></li>
                    <li><a href="nestable.html">Nestable</a></li>
                    <li><a href="grids.html">Grids</a></li>
                    <li><a href="calendar.html">Calender</a></li>
                    <li><a href="draggable_portlet.html">Draggable Portlet</a></li>
                </ul>
            </li>
            <li>
                <a href="fontawesome.html">
                    <i class="fa fa-bullhorn"></i>
                    <span>Fontawesome </span>
                </a>
            </li>
            <li class="sub-menu">
                <a href="javascript:;">
                    <i class="fa fa-th"></i>
                    <span>Data Tables</span>
                </a>
                <ul class="sub">
                    <li><a href="basic_table.html">Basic Table</a></li>
                    <li><a href="responsive_table.html">Responsive Table</a></li>
                    <li><a href="dynamic_table.html">Dynamic Table</a></li>
                    <li><a href="editable_table.html">Editable Table</a></li>
                </ul>
            </li>
            <li class="sub-menu">
                <a href="javascript:;">
                    <i class="fa fa-tasks"></i>
                    <span>Form Components</span>
                </a>
                <ul class="sub">
                    <li><a href="form_component.html">Form Elements</a></li>
                    <li><a href="advanced_form.html">Advanced Components</a></li>
                    <li><a href="form_wizard.html">Form Wizard</a></li>
                    <li><a href="form_validation.html">Form Validation</a></li>
                    <li><a href="file_upload.html">Muliple File Upload</a></li>
                    
                    <li><a href="dropzone.html">Dropzone</a></li>
                    <li><a href="inline_editor.html">Inline Editor</a></li>

                </ul>
            </li>
            <li class="sub-menu">
                <a href="javascript:;">
                    <i class="fa fa-envelope"></i>
                    <span>Mail </span>
                </a>
                <ul class="sub">
                    <li><a href="mail.html">Inbox</a></li>
                    <li><a href="mail_compose.html">Compose Mail</a></li>
                    <li><a href="mail_view.html">View Mail</a></li>
                </ul>
            </li>
            <li class="sub-menu">
                <a href="javascript:;">
                    <i class=" fa fa-bar-chart-o"></i>
                    <span>Charts</span>
                </a>
                <ul class="sub">
                    <li><a href="morris.html">Morris</a></li>
                    <li><a href="chartjs.html">Chartjs</a></li>
                    <li><a href="flot_chart.html">Flot Charts</a></li>
                    <li><a href="c3_chart.html">C3 Chart</a></li>
                </ul>
            </li>
            <li class="sub-menu">
                <a href="javascript:;">
                    <i class=" fa fa-bar-chart-o"></i>
                    <span>Maps</span>
                </a>
                <ul class="sub">
                    <li><a href="google_map.html">Google Map</a></li>
                    <li><a href="vector_map.html">Vector Map</a></li>
                </ul>
            </li>
            <li class="sub-menu">
                <a href="javascript:;" class="active">
                    <i class="fa fa-glass"></i>
                    <span>Extra</span>
                </a>
                <ul class="sub">
                    <li><a href="blank.html">Blank Page</a></li>
                    <li><a href="lock_screen.html">Lock Screen</a></li>
                    <li class="active"><a href="profile.html">Profile</a></li>
                    <li><a href="invoice.html">Invoice</a></li>
                    <li><a href="pricing_table.html">Pricing Table</a></li>
                    <li><a href="timeline.html">Timeline</a></li>                    
<li><a href="gallery.html">Media Gallery</a></li><li><a href="404.html">404 Error</a></li>
                    <li><a href="500.html">500 Error</a></li>
                    <li><a href="registration.html">Registration</a></li>
                </ul>
            </li>
            <li>
                <a href="login.html">
                    <i class="fa fa-user"></i>
                    <span>Login Page</span>
                </a>
            </li>
        -->
        </ul></div>        
<!-- sidebar menu end-->
    </div>
</aside>
<!--sidebar end-->
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
        <!-- page start-->
        @if(Session::has('message-info'))
        <p class="alert alert-info">{{ Session::get('message-info') }}</p>
        @endif
        @if(Session::has('message-warning'))
        <p class="alert alert-warning">{{ Session::get('message-warning') }}</p>
        @endif
        @if(Session::has('message-danger'))
        <p class="alert alert-danger">{{ Session::get('message-danger') }}</p>
        @endif
        @if(Session::has('message-success'))
        <div class="alert alert-success ">
            <span class="alert-icon"><i class="fa fa-thumbs-up"></i></span>
            <div class="notification-info">
                <ul class="clearfix notification-meta">
                    <li class="pull-left notification-sender">
                        <p><h4>{{ Session::get('message-success') }}</h4></p>   
                    </li>
                    <li class="pull-right notification-time">EXITO</li>
                </ul>
            </div>
        </div>

        @endif

          @yield('content')


        <!-- page end-->
        </section>
    </section>
    <!--main content end-->
<!--right sidebar start-->
<div class="right-sidebar">
<div class="search-row">
    <input type="text" placeholder="Search" class="form-control">
</div>
<div class="right-stat-bar">
<ul class="right-side-accordion">
<li class="widget-collapsible">
    <a href="#" class="head widget-head red-bg active clearfix">
        <span class="pull-left">work progress (5)</span>
        <span class="pull-right widget-collapse"><i class="ico-minus"></i></span>
    </a>
    <ul class="widget-container">
        <li>
            <div class="prog-row side-mini-stat clearfix">
                <div class="side-graph-info">
                    <h4>Target sell</h4>
                    <p>
                        25%, Deadline 12 june 13
                    </p>
                </div>
                <div class="side-mini-graph">
                    <div class="target-sell">
                    </div>
                </div>
            </div>
            <div class="prog-row side-mini-stat">
                <div class="side-graph-info">
                    <h4>product delivery</h4>
                    <p>
                        55%, Deadline 12 june 13
                    </p>
                </div>
                <div class="side-mini-graph">
                    <div class="p-delivery">
                        <div class="sparkline" data-type="bar" data-resize="true" data-height="30" data-width="90%" data-bar-color="#39b7ab" data-bar-width="5" data-data="[200,135,667,333,526,996,564,123,890,564,455]">
                        </div>
                    </div>
                </div>
            </div>
            <div class="prog-row side-mini-stat">
                <div class="side-graph-info payment-info">
                    <h4>payment collection</h4>
                    <p>
                        25%, Deadline 12 june 13
                    </p>
                </div>
                <div class="side-mini-graph">
                    <div class="p-collection">
                        <span class="pc-epie-chart" data-percent="45">
                        <span class="percent"></span>
                        </span>
                    </div>
                </div>
            </div>
            <div class="prog-row side-mini-stat">
                <div class="side-graph-info">
                    <h4>delivery pending</h4>
                    <p>
                        44%, Deadline 12 june 13
                    </p>
                </div>
                <div class="side-mini-graph">
                    <div class="d-pending">
                    </div>
                </div>
            </div>
            <div class="prog-row side-mini-stat">
                <div class="col-md-12">
                    <h4>total progress</h4>
                    <p>
                        50%, Deadline 12 june 13
                    </p>
                    <div class="progress progress-xs mtop10">
                        <div style="width: 50%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="20" role="progressbar" class="progress-bar progress-bar-info">
                            <span class="sr-only">50% Complete</span>
                        </div>
                    </div>
                </div>
            </div>
        </li>
    </ul>
</li>
<li class="widget-collapsible">
    <a href="#" class="head widget-head terques-bg active clearfix">
        <span class="pull-left">contact online (5)</span>
        <span class="pull-right widget-collapse"><i class="ico-minus"></i></span>
    </a>
    <ul class="widget-container">
        <li>
            <div class="prog-row">
                <div class="user-thumb">
                    <a href="#"><img src="{{ asset('') }}vendor/theme/images/avatar1_small.jpg" alt=""></a>
                </div>
                <div class="user-details">
                    <h4><a href="#">Jonathan Smith</a></h4>
                    <p>
                        Work for fun
                    </p>
                </div>
                <div class="user-status text-danger">
                    <i class="fa fa-comments-o"></i>
                </div>
            </div>
            <div class="prog-row">
                <div class="user-thumb">
                    <a href="#"><img src="{{ asset('') }}vendor/theme/images/avatar1.jpg" alt=""></a>
                </div>
                <div class="user-details">
                    <h4><a href="#">Anjelina Joe</a></h4>
                    <p>
                        Available
                    </p>
                </div>
                <div class="user-status text-success">
                    <i class="fa fa-comments-o"></i>
                </div>
            </div>
            <div class="prog-row">
                <div class="user-thumb">
                    <a href="#"><img src="{{ asset('') }}vendor/theme/images/chat-avatar2.jpg" alt=""></a>
                </div>
                <div class="user-details">
                    <h4><a href="#">John Doe</a></h4>
                    <p>
                        Away from Desk
                    </p>
                </div>
                <div class="user-status text-warning">
                    <i class="fa fa-comments-o"></i>
                </div>
            </div>
            <div class="prog-row">
                <div class="user-thumb">
                    <a href="#"><img src="{{ asset('') }}vendor/theme/images/avatar1_small.jpg" alt=""></a>
                </div>
                <div class="user-details">
                    <h4><a href="#">Mark Henry</a></h4>
                    <p>
                        working
                    </p>
                </div>
                <div class="user-status text-info">
                    <i class="fa fa-comments-o"></i>
                </div>
            </div>
            <div class="prog-row">
                <div class="user-thumb">
                    <a href="#"><img src="{{ asset('') }}vendor/theme/images/avatar1.jpg" alt=""></a>
                </div>
                <div class="user-details">
                    <h4><a href="#">Shila Jones</a></h4>
                    <p>
                        Work for fun
                    </p>
                </div>
                <div class="user-status text-danger">
                    <i class="fa fa-comments-o"></i>
                </div>
            </div>
            <p class="text-center">
                <a href="#" class="view-btn">View all Contacts</a>
            </p>
        </li>
    </ul>
</li>
<li class="widget-collapsible">
    <a href="#" class="head widget-head purple-bg active">
        <span class="pull-left"> recent activity (3)</span>
        <span class="pull-right widget-collapse"><i class="ico-minus"></i></span>
    </a>
    <ul class="widget-container">
        <li>
            <div class="prog-row">
                <div class="user-thumb rsn-activity">
                    <i class="fa fa-clock-o"></i>
                </div>
                <div class="rsn-details ">
                    <p class="text-muted">
                        just now
                    </p>
                    <p>
                        <a href="#">Jim Doe </a>Purchased new equipments for zonal office setup
                    </p>
                </div>
            </div>
            <div class="prog-row">
                <div class="user-thumb rsn-activity">
                    <i class="fa fa-clock-o"></i>
                </div>
                <div class="rsn-details ">
                    <p class="text-muted">
                        2 min ago
                    </p>
                    <p>
                        <a href="#">Jane Doe </a>Purchased new equipments for zonal office setup
                    </p>
                </div>
            </div>
            <div class="prog-row">
                <div class="user-thumb rsn-activity">
                    <i class="fa fa-clock-o"></i>
                </div>
                <div class="rsn-details ">
                    <p class="text-muted">
                        1 day ago
                    </p>
                    <p>
                        <a href="#">Jim Doe </a>Purchased new equipments for zonal office setup
                    </p>
                </div>
            </div>
        </li>
    </ul>
</li>
<li class="widget-collapsible">
    <a href="#" class="head widget-head yellow-bg active">
        <span class="pull-left"> shipment status</span>
        <span class="pull-right widget-collapse"><i class="ico-minus"></i></span>
    </a>
    <ul class="widget-container">
        <li>
            <div class="col-md-12">
                <div class="prog-row">
                    <p>
                        Full sleeve baby wear (SL: 17665)
                    </p>
                    <div class="progress progress-xs mtop10">
                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                            <span class="sr-only">40% Complete</span>
                        </div>
                    </div>
                </div>
                <div class="prog-row">
                    <p>
                        Full sleeve baby wear (SL: 17665)
                    </p>
                    <div class="progress progress-xs mtop10">
                        <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 70%">
                            <span class="sr-only">70% Completed</span>
                        </div>
                    </div>
                </div>
            </div>
        </li>
    </ul>
</li>
</ul>
</div>
</div>
<!--right sidebar end-->

</section>

<!-- Placed js at the end of the document so the pages load faster -->


    <!-- JavaScripts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js" integrity="sha384-I6F5OKECLVtK/BL+8iSLDEHowSAfUo76ZL9+kGAgTRdiByINKJaqTPH/QVNS1VDb" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
    {{-- <script src="{{ elixir('js/app3.js') }}"></script> --}}



<!--Core js-->
<script class="include" type="text/javascript" src="{{ asset('') }}vendor/theme/js/jquery.dcjqaccordion.2.7.js"></script>
<script src="{{ asset('') }}vendor/theme/js/jquery.scrollTo.min.js"></script>
<script src="{{ asset('') }}vendor/theme/js/jQuery-slimScroll-1.3.0/jquery.slimscroll.js"></script>
<script src="{{ asset('') }}vendor/theme/js/jquery.nicescroll.js"></script>
<!--Easy Pie Chart-->
<script src="{{ asset('') }}vendor/theme/js/easypiechart/jquery.easypiechart.js"></script>
<!--Sparkline Chart-->
<script src="{{ asset('') }}vendor/theme/js/sparkline/jquery.sparkline.js"></script>
<!--jQuery Flot Chart-->
<script src="{{ asset('') }}vendor/theme/js/flot-chart/jquery.flot.js"></script>
<script src="{{ asset('') }}vendor/theme/js/flot-chart/jquery.flot.tooltip.min.js"></script>
<script src="{{ asset('') }}vendor/theme/js/flot-chart/jquery.flot.resize.js"></script>
<script src="{{ asset('') }}vendor/theme/js/flot-chart/jquery.flot.pie.resize.js"></script>
<script src="{{ asset('') }}vendor/theme/js/bootstrap-switch.js"></script>
<!--dynamic table-->
<script type="text/javascript" language="javascript" src="{{ asset('') }}vendor/theme/js/advanced-datatable/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="{{ asset('') }}vendor/theme/js/data-tables/DT_bootstrap.js"></script>

<!--common script init for all pages-->
<script src="{{ asset('') }}vendor/theme/js/scripts.js"></script>
<script src="{{ asset('') }}js/jquery.maskedinput.min.js"></script>

<script type="text/javascript" src="https://cdn.datatables.net/tabletools/2.2.4/js/dataTables.tableTools.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/tabletools/2.2.2/swf/copy_csv_xls_pdf.swf"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.1.2/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.1.2/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script type="text/javascript" src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.1.2/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.1.2/js/buttons.print.min.js"></script>
 

<!--dynamic table initialization -->
<script src="{{ asset('') }}vendor/theme/js/dynamic_table_init.js"></script>

<script src="{{ asset('') }}js/app3.js"></script>


<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-93905353-1', 'auto');
  ga('send', 'pageview');

</script>

<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myCar" class="modal fade">
  <div class="modal-dialog">
      <div class="modal-content">
          <div class="modal-header">
              <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
              <h3 class="modal-title"> <i class="fa fa-shopping-cart" aria-hidden="true"></i> TU CARRITO</h3>
          </div>
          <div class="modal-body cartBody">

              <form method="GET" action="{{ url('/tienda/checkout') }}" accept-charset="UTF-8">
                    <meta name="csrf-token" content="{{ csrf_token() }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
              </form>
          </div>
          <br/>
          <a href="{{ url('/tienda/checkout') }}" class="btn btn-info" style="width:100%">ORDENAR</a>
          <br/>
      </div>
  </div>
</div>

<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="showImg" class="modal fade">
  <div class="modal-dialog">
      <div class="modal-content">
          <div class="modal-header">
              <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
          </div>
          <div class="modal-body">
            <img id="my_image" src="" width="100%" height="100%" />
          </div>
          <br/>
          <a href="#" id="my_down" class="btn btn-info" id style="width:100%">DESCARGAR</a>
          <br/>
      </div>
  </div>
</div>


</body>
</html>