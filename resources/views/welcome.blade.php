@extends('layouts.app')

@section('content')
<div class="container">
    <center>
      <a href="{{ url('/login') }}" class="btn btn-primary btn-lg"><i class="fa fa-btn fa-sign-in"></i> Ingresar</a>
    </center>
    <br/>    

    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Plan de Negocios</div>

                <div class="panel-body">
                    

                    <section class="panel">
                        <div id="c-slide" class="carousel slide auto panel-body">
                            <ol class="carousel-indicators out">
                                <li class="active" data-slide-to="0" data-target="#c-slide"></li>
                                <li class="" data-slide-to="1" data-target="#c-slide"></li>
                                <li class="" data-slide-to="2" data-target="#c-slide"></li>
                                <li class="" data-slide-to="3" data-target="#c-slide"></li>
                                <li class="" data-slide-to="4" data-target="#c-slide"></li>
                                <li class="" data-slide-to="5" data-target="#c-slide"></li>
                                <li class="" data-slide-to="6" data-target="#c-slide"></li>
                                <li class="" data-slide-to="7" data-target="#c-slide"></li>
                                <li class="" data-slide-to="8" data-target="#c-slide"></li>
                                <li class="" data-slide-to="9" data-target="#c-slide"></li>
                                <li class="" data-slide-to="10" data-target="#c-slide"></li>
                                <li class="" data-slide-to="11" data-target="#c-slide"></li>
                                <li class="" data-slide-to="12" data-target="#c-slide"></li>
                                <li class="" data-slide-to="13" data-target="#c-slide"></li>
                                <li class="" data-slide-to="14" data-target="#c-slide"></li>
                                <li class="" data-slide-to="15" data-target="#c-slide"></li>
                                <li class="" data-slide-to="16" data-target="#c-slide"></li>
                                <li class="" data-slide-to="17" data-target="#c-slide"></li>
                            </ol>
                            <div class="carousel-inner">
                                <div class="item text-center active">
                                    <img src="{{ asset('') }}img/201705b/Diapositiva1.JPG" alt="CafeLogo" width="100%">
                                </div>
                                <div class="item text-center">
                                    <img src="{{ asset('') }}img/201705b/Diapositiva2.JPG" alt="CafeLogo" width="100%">
                                </div>
                                <div class="item text-center">
                                    <img src="{{ asset('') }}img/201705b/Diapositiva3.JPG" alt="CafeLogo" width="100%">
                                </div>
                                <div class="item text-center">
                                    <img src="{{ asset('') }}img/201705b/Diapositiva4.JPG" alt="CafeLogo" width="100%">
                                </div>
                                <div class="item text-center">
                                    <img src="{{ asset('') }}img/201705b/Diapositiva5.JPG" alt="CafeLogo" width="100%">
                                </div>
                                <div class="item text-center">
                                    <img src="{{ asset('') }}img/201705b/Diapositiva6.JPG" alt="CafeLogo" width="100%">
                                </div>
                                <div class="item text-center">
                                    <img src="{{ asset('') }}img/201705b/Diapositiva7.JPG" alt="CafeLogo" width="100%">
                                </div>
                                <div class="item text-center">
                                    <img src="{{ asset('') }}img/201705b/Diapositiva8.JPG" alt="CafeLogo" width="100%">
                                </div>
                                <div class="item text-center">
                                    <img src="{{ asset('') }}img/201705b/Diapositiva9.JPG" alt="CafeLogo" width="100%">
                                </div>
                                <div class="item text-center">
                                    <img src="{{ asset('') }}img/201705b/Diapositiva10.JPG" alt="CafeLogo" width="100%">
                                </div>
                                <div class="item text-center">
                                    <img src="{{ asset('') }}img/201705b/Diapositiva11.JPG" alt="CafeLogo" width="100%">
                                </div>
                                <div class="item text-center">
                                    <img src="{{ asset('') }}img/201705b/Diapositiva12.JPG" alt="CafeLogo" width="100%">
                                </div>
                                <div class="item text-center">
                                    <img src="{{ asset('') }}img/201705b/Diapositiva13.JPG" alt="CafeLogo" width="100%">
                                </div>
                                <div class="item text-center">
                                    <img src="{{ asset('') }}img/201705b/Diapositiva14.JPG" alt="CafeLogo" width="100%">
                                </div>
                                <div class="item text-center">
                                    <img src="{{ asset('') }}img/201705b/Diapositiva15.JPG" alt="CafeLogo" width="100%">
                                </div>
                                <div class="item text-center">
                                    <img src="{{ asset('') }}img/201705b/Diapositiva16.JPG" alt="CafeLogo" width="100%">
                                </div>
                                <div class="item text-center">
                                    <img src="{{ asset('') }}img/201705b/Diapositiva17.JPG" alt="CafeLogo" width="100%">
                                </div>
                                <div class="item text-center">
                                    <img src="{{ asset('') }}img/201705b/Diapositiva18.JPG" alt="CafeLogo" width="100%">
                                </div>
                                <div class="item text-center">
                                    <h3>CONTACTO</h3>
                                    <p>¿QUIERES CONOCER MÁS? ¡CONTÁCTAME!</p>
                                </div>
                            </div>
                            <a data-slide="prev" href="#c-slide" class="left carousel-control">
                                <i class="fa fa-angle-left"></i>
                            </a>
                            <a data-slide="next" href="#c-slide" class="right carousel-control">
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </div>
                    </section>

                    <a href="{{ url('/unirme') }}" type="button" class="btn btn-success btn-lg btn-block">Quiero unirme a la RED!!!</a>
                    <br/>
                    <br/>
                    <br/>


                </div>
            </div>
        </div>
    </div>
</div>
@endsection
