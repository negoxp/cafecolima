@extends('layouts.admin')
@section('content')
<h1 class="page-header text-gray">
    Usuarios
    <div class="pull-right"><a href="{{url('users/create')}}" class="btn btn-success">Agregar <span class="icon  icon-plus"></span></a></div>
</h1>
<table id="example2" class="table table-bordered table-hover">
    <thead>
        <tr>
            <th>Id</th>
            <th>Fecha</th>
            <th>Email</th>
            <th>Nombre</th>
            <th>Role</th>
        </tr>
    </thead>
    <tbody>
        @foreach($users as $user)
        <tr>
            <td><a href="users/{{$user->id}}/edit">{{$user->id}}</a></td>
            <td>{{$user->created_at }}</td> 
            <td><a href="users/{{$user->id}}/edit">{{$user->email}}</a></td>
            <td>{{$user->nombrecompleto() }}</td> 
            <td>{{ $user->roles->implode('name',',') }}</td>
        </tr>
        @endforeach
    </tbody>
    <tfoot>
        <tr>
            <th>Id</th>
            <th>Email</th>
            <th>Nombre</th>
            <th>Role</th>
        </tr>
    </tfoot>
</table>


<script type="text/javascript">
    (function($) {
        "use strict";

        $('#example2').dataTable({
            "bPaginate": true,
            "bLengthChange": true,
            "bFilter": true,
            "bSort": true,
            "bInfo": true,
            "bAutoWidth": true
        });
    })(jQuery);


</script>
@endsection