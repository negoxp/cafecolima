@extends('layouts.admin')
@section('content')

<h1 class="page-header text-gray">
    {{$title}}


        <div class="pull-right btn-group">
            <a href="{{url('users')}}" class="btn btn-default">Regresar <span class="icon   icon-arrow-thin-left"></span></a>
        </div>
        <?php /*

        {!! Form::open(array('route' => ['users.destroy', isset($id) ? $id : 0], 'method' => 'delete', 'onsubmit'=>'return confirm("Are you sure you want to delete?")')) !!}
        <div class="pull-right btn-group">
            <a href="{{url('users')}}" class="btn btn-default">Regresar <span class="icon   icon-arrow-thin-left"></span></a>
            @if($title=='Edit User')
                <button type="submit" class="btn btn-danger">Eliminar <span class="icon icon-cross"></span></button>
            @endif
        </div>
        {!! Form::close() !!}
        */ ?>



</h1>

<!--
<ul>
    @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
    @endforeach
</ul>
-->
{!! form($form) !!}

@endsection