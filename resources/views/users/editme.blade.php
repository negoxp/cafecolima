@extends('layouts.admin')
@section('content')

<h1 class="page-header text-gray">
    Mi Perfil <strong>{{ $me->nombrecompleto()}}</strong>
</h1>
<p>Una vez que se editen los datos, se bloquerá para futuros cambios. Si en un futuro deseas cambiar los datos es necesario la autorización de una administrador (delrinconelizondo@gmail.com). Pude mandar un mail para solicitar el cambio. </p>
 
<ul>
    @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
    @endforeach
</ul>

<section class="panel">
    <div class="panel-body">
    {!! form($form) !!}
    </div>
</section>







@endsection