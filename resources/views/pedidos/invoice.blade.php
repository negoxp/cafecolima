@extends('layouts.admin')

@section('content')
<div class="col-md-12">
    <section class="panel">
        <div class="panel-body invoice">
            <div class="invoice-header">
                <div class="invoice-title col-md-12 col-xs-12">
                    <img src="http://saboracolima.com/img/logotop.png" alt="CafeLogo" width="183" style="border-width:0;display:block" border="0">
                </div>
            </div>
            <div class="row invoice-to">
                <div class="col-md-6 col-sm-6 pull-left">
                    <h2>Productos Sabor a Colima</h2>
                    <p>
                        Calle Sin Nombre, Sin Numero, El Nuevo Naranjal<br>
                        Villa de Álvarez, Colima. C.P. 28950<br>
                        Telefono: 01 312 16 14845<br>
                        Email : contacto@saboracolima.com<br>
                        Facebook : Sabor a Colima<br>
                        @if ($pedido->pagado)
                            <span class="label label-success">PAGADO</span><br>
                        @else
                            <span class="label label-warning">PENDIENTE</span><br>
                        @endif
                    </p>
                </div>
                <div class="col-md-5 col-sm-6 pull-right">
                    <h4>{{$cliente->nombre}} {{$cliente->apellidos}}</h4>
                    <div class="row">
                        <div class="col-md-4 col-sm-5 inv-label">Socio </div>
                        <div class="col-md-8 col-sm-7">{{$cliente->id}}</div>
                    </div>

                    <div class="row">
                        <div class="col-md-4 col-sm-5 inv-label">Folio </div>
                        <div class="col-md-8 col-sm-7">{{$pedido->id}}</div>
                    </div>

                    

                    <div class="row">
                        <div class="col-md-4 col-sm-5 inv-label">Referencia </div>
                        <div class="col-md-8 col-sm-7">{{$cliente->id}}-{{$pedido->id}}</div>
                    </div>

                    <div class="row">
                        <div class="col-md-4 col-sm-5 inv-label">Fecha </div>
                        <div class="col-md-8 col-sm-7">{{$pedido->created_at}}</div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-12 inv-label">
                            <h3>TOTAL</h3>
                        </div>
                        <div class="col-md-12">
                            <h1 class="amnt-value">$ {{number_format((float)$pedido->total,2)}}</h1>
                        </div>
                    </div>


                </div>
            </div>
            <table class="table table-invoice">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Desc.</th>
                    <th class="text-center">Total</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($pedidoDetalles as $pd) { ?>
                <tr>
                    <td>1</td>
                    <td>
                        <h4>{{$pd->producto->nombre}}</h4>
                    </td>
                    <td class="text-center">$ {{number_format((float)$pd->total,2)}}</td>
                </tr>
                <?php } ?>
                </tbody>
            </table>
            <div class="row">
                <div class="col-md-8 col-xs-7 payment-method">
                    <h4>Formas de Pago</h4>
                    <p>1. Deposito en BANCOMER cuenta: 0110222038.<br/>
                        Benficiario:FEDERICO RINCON ELIZONDO<br/>
                    Ventanilla y Cajeros Bancomer 24 HORAS</p>
                    <p>2. Transferencia electrónica: 012098001102220389 (Bancomer)</p>
                    <p>3. OXXO: PLASTICO 5204 1602 1449 3392. BANAMEX<br/>
                    <br>
                    <strong>REFERENCIA: {{$cliente->id}}-{{$pedido->id}}</strong>
                    <br>
                    <h3 class="inv-label itatic">Gracias por tu Compra</h3>
                </div>
                <div class="col-md-4 col-xs-5 invoice-block pull-right">
                    <ul class="unstyled amounts">
                        <!--
                        <li>Sub - Total : {{number_format((float)$pedido->sub_total,2)}}</li>
                        <li>IVA : {{number_format((float)$pedido->iva,2)}}</li>
                        -->
                        <li class="grand-total">Total : {{number_format((float)$pedido->total,2)}}</li>
                    </ul>
                </div>
            </div>

            <div class="text-center invoice-btn">
                <!--<a class="btn btn-success btn-lg"><i class="fa fa-check"></i> Submit Invoice </a>-->
                <a href="{{ url('/pedidos/invoice_print/'.$pedido->id) }}" target="_blank" class="btn btn-primary btn-lg pull-right" target="_blank"><i class="fa fa-print"></i> Imprimir </a>
            </div>

        </div>
    </section>
</div>
@endsection