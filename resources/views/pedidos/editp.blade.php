@extends('layouts.admin')

@section('content')

{{ Form::model($pedido, array('route' => array('pedidos.update', $pedido->id), 'method' => 'PUT',  'files' => 'true')) }}

    <div class="form-group">
        {{ Form::label('user_id', 'Usuario ID') }} 
        {{ Form::text('user_id', $pedido->user_id, array('class' => 'form-control' )) }}
    </div>
    


    {{ Form::submit('Subir', array('class' => 'btn btn-primary')) }}

{{ Form::close() }} 

@endsection