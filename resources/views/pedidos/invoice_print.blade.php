<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Jorge Flores">
    <!--<link rel="shortcut icon" href="images/favicon.png">-->

    <title>Café sabor a Colima</title>


    <!--Core CSS -->
    <link href="{{ asset('') }}vendor/theme/bs3/css/bootstrap.min.css" rel="stylesheet">
    <link href="{{ asset('') }}vendor/theme/css/bootstrap-reset.css" rel="stylesheet">
    <link href="{{ asset('') }}vendor/theme/font-awesome/css/font-awesome.css" rel="stylesheet" />

    <!-- Custom styles for this template -->
    <link href="{{ asset('') }}vendor/theme/css/style.css" rel="stylesheet">
    <link href="{{ asset('') }}vendor/theme/css/style-responsive.css" rel="stylesheet" />
    <link href="{{ asset('') }}vendor/theme/css/table-responsive.css" rel="stylesheet" />

    <link href="{{ asset('') }}vendor/theme/css/tree2.css" rel="stylesheet">
    <link href="{{ asset('') }}vendor/theme/css/invoice-print.css" rel="stylesheet" media="all">
    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]>
    <script src="{{ asset('') }}vendor/theme/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

</head>
<body>

<section id="container" class="print" >

    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
        <!-- page start-->

     <div class="col-md-12">
    <section class="panel">
        <div class="panel-body invoice">
            <div class="invoice-header">
                <div class="invoice-title col-md-12 col-xs-12">
                    <img src="http://saboracolima.com/img/logotop.png" alt="CafeLogo" width="183" style="border-width:0;display:block" border="0">
                </div>
            </div>
            <div class="row invoice-to">
                <div class="col-md-5 col-sm-5 pull-left">
                    <h3>Productos Sabor a Colima</h3>
                    <p>
                        Calle Sin Nombre, Sin Numero, El Nuevo Naranjal<br>
                        Villa de Álvarez, Colima. C.P. 28950<br>
                        Telefono: 01 312 16 14845<br>
                        Email : contacto@saboracolima.com<br>
                        Facebook : Sabor a Colima<br>
                    </p>
                </div>
                <div class="col-md-5 col-sm-6 pull-right">
                    <h3>{{$cliente->nombre}} {{$cliente->apellidos}}</h3>
                    <div class="row">
                        <div class="col-md-4 col-sm-5 inv-label">Socio </div>
                        <div class="col-md-8 col-sm-7">{{$cliente->id}}</div>
                    </div>

                    <div class="row">
                        <div class="col-md-4 col-sm-5 inv-label">Folio </div>
                        <div class="col-md-8 col-sm-7">{{$pedido->id}}</div>
                    </div>

                    <div class="row">
                        <div class="col-md-4 col-sm-5 inv-label">Referencia </div>
                        <div class="col-md-8 col-sm-7">{{$cliente->id}}-{{$pedido->id}}</div>
                    </div>

                    <div class="row">
                        <div class="col-md-4 col-sm-5 inv-label">Fecha </div>
                        <div class="col-md-8 col-sm-7">{{$pedido->created_at}}</div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-12 inv-label">
                            <h3>TOTAL</h3>
                        </div>
                        <div class="col-md-12">
                            <h1 class="amnt-value">$ {{number_format((float)$pedido->total,2)}}</h1>
                        </div>
                    </div>


                </div>
            </div>
            <table class="table table-invoice">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Desc.</th>
                    <th class="text-center">Total</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($pedidoDetalles as $pd) { ?>
                <tr>
                    <td>1</td>
                    <td>
                        <h4>{{$pd->producto->nombre}}</h4>
                    </td>
                    <td class="text-center">$ {{number_format((float)$pd->total,2)}}</td>
                </tr>
                <?php } ?>
                </tbody>
            </table>
            <div class="row">
                <div class="col-md-8 col-xs-7 payment-method">
                    <h4>Formas de Pago</h4>
                    <p>1. Deposito en BANCOMER cuenta: 0110222038.<br/>
                        Benficiario:FEDERICO RINCON ELIZONDO<br/>
                    Ventanilla y Cajeros Bancomer 24 HORAS</p>
                    <p>2. Transferencia electrónica: 012098001102220389 (Bancomer)</p>
                    <p>3. OXXO: PLASTICO 5204 1602 1449 3392. BANAMEX<br/>
                    <strong>REFERENCIA: {{$cliente->id}}-{{$pedido->id}}</strong>
                    <br>
                    <h3 class="inv-label itatic">Gracias por tu Compra</h3>
                </div>
                <div class="col-md-4 col-xs-5 invoice-block pull-right">
                    <ul class="unstyled amounts">
                        <!--
                        <li>Sub - Total : {{number_format((float)$pedido->sub_total,2)}}</li>
                        <li>IVA : {{number_format((float)$pedido->iva,2)}}</li>
                    -->
                        <li class="grand-total">Total : {{number_format((float)$pedido->total,2)}}</li>
                    </ul>
                </div>
            </div>


        </div>
    </section>
</div>



        <!-- page end-->
        </section>
    </section>
    <!--main content end-->

</section>



<!-- JavaScripts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js" integrity="sha384-I6F5OKECLVtK/BL+8iSLDEHowSAfUo76ZL9+kGAgTRdiByINKJaqTPH/QVNS1VDb" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
{{-- <script src="{{ elixir('js/app3.js') }}"></script> --}}



<!--Core js-->
<script class="include" type="text/javascript" src="{{ asset('') }}vendor/theme/js/jquery.dcjqaccordion.2.7.js"></script>
<script src="{{ asset('') }}vendor/theme/js/jquery.scrollTo.min.js"></script>
<script src="{{ asset('') }}vendor/theme/js/jQuery-slimScroll-1.3.0/jquery.slimscroll.js"></script>
<script src="{{ asset('') }}vendor/theme/js/jquery.nicescroll.js"></script>
<!--Easy Pie Chart-->
<script src="{{ asset('') }}vendor/theme/js/easypiechart/jquery.easypiechart.js"></script>
<!--Sparkline Chart-->
<script src="{{ asset('') }}vendor/theme/js/sparkline/jquery.sparkline.js"></script>
<!--jQuery Flot Chart-->
<script src="{{ asset('') }}vendor/theme/js/flot-chart/jquery.flot.js"></script>
<script src="{{ asset('') }}vendor/theme/js/flot-chart/jquery.flot.tooltip.min.js"></script>
<script src="{{ asset('') }}vendor/theme/js/flot-chart/jquery.flot.resize.js"></script>
<script src="{{ asset('') }}vendor/theme/js/flot-chart/jquery.flot.pie.resize.js"></script>

<!--common script init for all pages-->
<script src="{{ asset('') }}vendor/theme/js/scripts.js"></script>
<script src="{{ asset('') }}js/app3.js"></script>
<script type="text/javascript">
    window.print();
</script>

</body>
</html>
